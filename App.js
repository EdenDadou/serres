import React, { Suspense, Component, useState } from "react";
import { Provider } from "react-redux";
import store from "./src/Redux/store";
import AppLoading from "expo-app-loading";
import Navigator from "./src/app/Navigator";
import { Asset } from "expo-asset";
import {
  useFonts,
  MedievalSharp_400Regular,
} from "@expo-google-fonts/medievalsharp";
import {
  Palanquin_500Medium,
  Palanquin_700Bold,
} from "@expo-google-fonts/palanquin";
import { Platform } from "react-native";

export default function App(props) {
  let [fontsLoaded] = useFonts({
    MedievalSharp_400Regular,
    Palanquin_500Medium,
    Palanquin_700Bold,
  });
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!fontsLoaded && !isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <Provider store={store}>
        <Suspense>
          <Navigator />
        </Suspense>
      </Provider>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require("./assets/splash.png"),
      require("./src/Views/Assets/Langues/PhotoSerresVille.jpg"),
      require("./src/Views/Assets/Menu/Agenda.png"),
      require("./src/Views/Assets/Menu/Artisanat.jpg"),
      require("./src/Views/Assets/Menu/Art.jpg"),
      require("./src/Views/Assets/Menu/BlasonSerres.png"),
      require("./src/Views/Assets/Menu/ParcoursLibre.jpg"),
      require("./src/Views/Assets/Menu/CulturePat.jpg"),
      require("./src/Views/Assets/Menu/EscapeGame.jpg"),
      require("./src/Views/Assets/Menu/Reglages.png"),
      require("./src/Views/Assets/Menu/SerresPratique.png"),
      require("./src/Views/Assets/Menu/medieval.jpg"),
      require("./src/Views/Assets/Menu/NatureLoisirs.jpg"),

      require("./src/Views/Assets/Artisanat/Map.png"),
      require("./src/Views/Assets/Artisanat/Alcatraz.png"),
      require("./src/Views/Assets/Artisanat/Dupuis.png"),
      require("./src/Views/Assets/Artisanat/Gagnard.png"),
      require("./src/Views/Assets/Artisanat/HangArt.png"),
      require("./src/Views/Assets/Artisanat/Judith.png"),
      require("./src/Views/Assets/Artisanat/Siaud.jpg"),

      require("./src/Views/Assets/Artisanat/Alcatraz/Anita.jpg"),
      require("./src/Views/Assets/Artisanat/Alcatraz/luminaire_1.jpg"),
      require("./src/Views/Assets/Artisanat/Alcatraz/luminaire_2.jpg"),
      require("./src/Views/Assets/Artisanat/Alcatraz/port_guitare.jpg"),
      require("./src/Views/Assets/Artisanat/Alcatraz/Table_1.jpg"),

      require("./src/Views/Assets/Artisanat/Dupuis/BrodCerf.png"),
      require("./src/Views/Assets/Artisanat/Dupuis/Broderie.jpg"),
      require("./src/Views/Assets/Artisanat/Dupuis/BrodEtui.png"),
      require("./src/Views/Assets/Artisanat/Dupuis/BrodMitaineMarie.png"),
      require("./src/Views/Assets/Artisanat/Dupuis/BrodServiettes.png"),
      require("./src/Views/Assets/Artisanat/Dupuis/DessinBrod.jpg"),

      require("./src/Views/Assets/Artisanat/Gagnard/FauteuilExpo.jpg"),
      require("./src/Views/Assets/Artisanat/Gagnard/FauteuilFourrure.jpg"),
      require("./src/Views/Assets/Artisanat/Gagnard/FauteuilTapisserie.jpg"),

      require("./src/Views/Assets/Artisanat/HangArt/Bijoux.jpg"),
      require("./src/Views/Assets/Artisanat/HangArt/Marroquinerie.jpg"),
      require("./src/Views/Assets/Artisanat/HangArt/Salle.png"),
      require("./src/Views/Assets/Artisanat/HangArt/Vannerie.jpg"),
      require("./src/Views/Assets/Artisanat/HangArt/Vannerie2.jpg"),
      require("./src/Views/Assets/Artisanat/HangArt/Voiture.png"),

      require("./src/Views/Assets/Artisanat/Judith/Judith1.png"),
      require("./src/Views/Assets/Artisanat/Judith/Judith2.png"),

      require("./src/Views/Assets/Artisanat/Siaud/Couteau_1.jpg"),
      require("./src/Views/Assets/Artisanat/Siaud/Couteau_2.jpg"),
      require("./src/Views/Assets/Artisanat/Siaud/Couteau_3.jpg"),
      require("./src/Views/Assets/Artisanat/Siaud/Couteau_4.jpg"),
      require("./src/Views/Assets/Artisanat/Siaud/Couteaux.jpg"),
      require("./src/Views/Assets/Artisanat/Siaud/Guitard.jpg"),

      require("./src/Views/Assets/Art/Map.png"),
      require("./src/Views/Assets/Art/CEM.jpg"),
      require("./src/Views/Assets/Art/Espace72.jpg"),
      require("./src/Views/Assets/Art/SaintArey.png"),
      require("./src/Views/Assets/Art/OfficeTourisme.jpg"),
      require("./src/Views/Assets/Art/Espace72/peinture1.jpg"),
      require("./src/Views/Assets/Art/Espace72/peinture2.jpg"),
      require("./src/Views/Assets/Art/Espace72/peinture3.jpg"),
      require("./src/Views/Assets/Art/SaintArey/peinture1.png"),
      require("./src/Views/Assets/Art/SaintArey/peinture2.png"),
      require("./src/Views/Assets/Art/SaintArey/peinture3.png"),
      require("./src/Views/Assets/Art/SaintArey/peinture4.png"),
      require("./src/Views/Assets/Art/OfficeTourisme/amilcar.jpg"),
      require("./src/Views/Assets/Art/OfficeTourisme/office_expo_2.jpg"),
      require("./src/Views/Assets/Art/OfficeTourisme/office_expo.jpg"),
      require("./src/Views/Assets/Art/OfficeTourisme/OfficeTourisme.jpg"),

      require("./src/Views/Assets/NatureLoisirs/BordEau/bordEau.jpeg"),

      require("./src/Views/Assets/NatureLoisirs/Germanette/Germanette_1.jpg"),
      require("./src/Views/Assets/NatureLoisirs/Germanette/Germanette_2.jpg"),
      require("./src/Views/Assets/NatureLoisirs/Germanette/germanette_logo.jpg"),

      require("./src/Views/Assets/NatureLoisirs/SentierGineste/petite_gineste.jpg"),

      require("./src/Views/Assets/NatureLoisirs/SentierMoines/Chapelle_Saumane_Cloche.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/ChapelleSaumane.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/nbs_1.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/nbs_2.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/tombe_juive.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/tombe_juive_2.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/vierge_saumane.jpg"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/fr/Chapelle_de_saumane.mp3"),
      require("./src/Views/Assets/NatureLoisirs/SentierMoines/fr/TombeauJuif_96kbps.mp3"),

      require("./src/Views/Assets/NatureLoisirs/SentierSources/sources.jpg"),

      require("./src/Views/Assets/CulturePatrimoine/Photo/ancienne_maison_commune_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/ancienne_maison_commune_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/ancienne_maison_commune_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/beau_de_rochas.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/bourg_reynaud_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/chateau_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/chateau_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Estanco_1.png"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Estanco_2.png"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Estanco_3.png"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/fortifications_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/grenier_a_sel_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/hospitalet_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/le_portalet_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/le_portalet_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/mairie_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/mairie_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/mairie_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_arnaudon_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_arnaudon_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_arnaudon_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_commune_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_commune_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_juge_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_juge_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Maison_lesdig_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Maison_lesdig_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/Maison_lesdig_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_vieille_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/maison_vieille_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/nbs_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/nbs_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/patrimoine_archi.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/piece_buste_lesdiguières.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/place_auche_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/place_auche_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/place_auche_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/portail_guire_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/portail_guire_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/porte_saint_claude_1.png"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/porte_saint_claude_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/porte_saint_claude_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/serres_dessin.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/st_arey_1.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/st_arey_2.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/st_arey_3.jpg"),
      require("./src/Views/Assets/CulturePatrimoine/Photo/tour_de_molend_1.jpg"),

      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_01_Le_portalet_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_02_Bourg_Reynaud_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_03_Fortifications_112_ok.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_05_Le_Baillage_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_06_demeure_arnaudon_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_07_ancienne_maison_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_08_maison_lesdiguieres_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_09_eglise_starey_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_10_auche_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_11_maison_commune_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_12_tour_de_molend_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_13_nd_bonsecours_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PL_14_chateau_serres_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_01_porte_saint_claude_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_02_maison_typique_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_05_maison_juge_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_06_grenier_sel_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_07_hospitaletl_112.mp3"),
      require("./src/Views/Assets/CulturePatrimoine/Audio/fr/PP_08_portail_guire_112.mp3"),

      require("./src/Views/Assets/EscapeGame/image/texture.jpg"),
    ]),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}
