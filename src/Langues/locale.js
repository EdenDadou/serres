import fr from "./fr.json";
import en from "./en.json";
import it from "./it.json";
import nl from "./nl.json";

const data = {
  fr,
  en,
  it,
  nl,
};

export const translate = (keyWord, lang) => {
  return data[lang].hasOwnProperty(keyWord) && data[lang][keyWord];
};
