/* LOCALE */
export const CHANGE_LOCALE = "CHANGE_LOCALE";
export const UPDATE_LOCATION = "UPDATE_LOCATION";

/* PARCOURS */
export const SET_PARCOUR_NAME = "SET_PARCOUR_NAME";
export const SET_STEP = "SET_STEP";
