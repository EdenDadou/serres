import { UPDATE_LOCATION } from "../actions";

export const ChangeLocation = (latLong) => (
  {
    type: UPDATE_LOCATION,
    payload: latLong,
  }
);
