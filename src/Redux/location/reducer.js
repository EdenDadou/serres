import {
    UPDATE_LOCATION
} from '../actions';

const INIT_STATE = {
    currentUserLocation: undefined,
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_LOCATION:
            return { ...state, currentUserLocation: action.payload };
        default:
            return state
    }

}

