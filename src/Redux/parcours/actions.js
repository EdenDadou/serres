import { SET_STEP, SET_PARCOUR_NAME } from "../actions";

export const setStep = (step) => ({
  type: SET_STEP,
  payload: step,
});

export const setParcourName = (name) => ({
  type: SET_PARCOUR_NAME,
  payload: name,
});
