import { SET_STEP, SET_PARCOUR_NAME } from "../actions";

const INIT_STATE = {
  parcoursEvent: { step: 0 },
  parcoursName: undefined,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_STEP:
      return { ...state, parcoursEvent: { step: action.payload } };
    case SET_PARCOUR_NAME:
      return { ...state, parcoursName: action.payload };
    default:
      return state;
  }
};
