import { combineReducers } from "redux";
import locale from "./locale/reducer";
import location from "./location/reducer";
import parcours from "./parcours/reducer";

const reducers = combineReducers({
  locale,
  location,
  parcours,
});

export default reducers;
