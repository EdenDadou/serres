import axios from "axios";
import { URL } from "./config";

//*************************** USER CONNECTION ***********************//

export const ConnectUser = (data) => {
  const PromiseResponse = axios.post(URL + "/api/auth/login", data);
  return PromiseResponse;
};

//*************************** EVENT ***********************//

export const getEvent = () => {
  const PromiseResponse = axios.get(URL + "/api/event");
  return PromiseResponse;
};

export const postEvent = (data) => {
  const PromiseResponse = axios.post(URL + "/api/event/newevent", data);
  // console.log(PromiseResponse)
  return PromiseResponse;
};

export const postPictureToEvent = (data) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data; boundary=coucou",
    },
  };
  const PromiseResponse = axios.put(
    URL + "/api/event/addpicture",
    data,
    config
  );
  return PromiseResponse;
};
