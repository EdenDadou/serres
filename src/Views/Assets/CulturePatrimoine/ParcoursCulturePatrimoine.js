import {
  polylinePatrimoine,
  polylineLesdiguieres,
  polylineEstanco,
} from "./Polylines";

{
  uri: "http://137.74.198.215:3000/langues/Audio";
}

export const ParcoursCulturePatrimoine = (langue) =>
  Platform.OS === "ios"
    ? [
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Patrimoine.0",
              description: "CulturePatrimoine.Patrimoine.0.Desc",
              image: require("./Photo/patrimoine_archi.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_00_intro_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_00_intro_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_00_intro_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_00_intro_112_NL.mp3"),
            },
          ],
          markers: [
            {
              latitude: 44.429954,
              longitude: 5.716445,
              title: "CulturePatrimoine.Patrimoine.1",
              description: "CulturePatrimoine.Patrimoine.1.Desc",
              image: require("./Photo/porte_saint_claude_1.png"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_01_porte_saint_claude_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_01_porte_saint_claude_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_01_porte_saint_claude_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_01_porte_saint_claude_112_NL.mp3"),
            },
            {
              latitude: 44.429637,
              longitude: 5.716586,
              title: "CulturePatrimoine.Patrimoine.2",
              description: "CulturePatrimoine.Patrimoine.2.Desc",
              image: require("./Photo/maison_vieille_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_02_maison_typique_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_02_maison_typique_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_02_maison_typique_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_02_maison_typique_112_NL.mp3"),
            },
            {
              latitude: 44.42926,
              longitude: 5.716273,
              title: "CulturePatrimoine.Patrimoine.3",
              description: "CulturePatrimoine.Patrimoine.3.Desc",
              image: require("./Photo/st_arey_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_09_eglise_starey_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_09_eglise_starey_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_09_eglise_starey_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_09_eglise_starey_112_NL.mp3"),
            },
            {
              latitude: 44.42917,
              longitude: 5.71609,
              title: "CulturePatrimoine.Patrimoine.4",
              description: "CulturePatrimoine.Patrimoine.4.Desc",
              image: require("./Photo/Maison_lesdig_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_08_maison_lesdiguieres_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_08_maison_lesdiguieres_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_08_maison_lesdiguieres_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3"),
            },
            {
              latitude: 44.429073,
              longitude: 5.715922,
              title: "CulturePatrimoine.Patrimoine.5",
              description: "CulturePatrimoine.Patrimoine.5.Desc",
              image: require("./Photo/maison_juge_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_05_maison_juge_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_05_maison_juge_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_05_maison_juge_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_05_maison_juge_112_NL.mp3"),
            },
            {
              latitude: 44.428877,
              longitude: 5.715494,
              title: "CulturePatrimoine.Patrimoine.6",
              description: "CulturePatrimoine.Patrimoine.6.Desc",
              image: require("./Photo/grenier_a_sel_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_06_grenier_sel_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_06_grenier_sel_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_06_grenier_sel_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_06_grenier_sel_112_NL.mp3"),
            },
            {
              latitude: 44.428831,
              longitude: 5.715365,
              title: "CulturePatrimoine.Patrimoine.7",
              description: "CulturePatrimoine.Patrimoine.7.Desc",
              image: require("./Photo/portail_guire_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_08_portail_guire_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_08_portail_guire_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_08_portail_guire_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_08_portail_guire_112_NL.mp3"),
            },
            {
              latitude: 44.428746,
              longitude: 5.715213,
              title: "CulturePatrimoine.Patrimoine.8",
              description: "CulturePatrimoine.Patrimoine.8.Desc",
              image: require("./Photo/hospitalet_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_07_hospitaletl_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PP_07_hospitaletl_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PP_07_hospitaletl_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PP_07_hospitaletl_112_NL.mp3"),
            },
            {
              latitude: 44.42856,
              longitude: 5.71479,
              title: "CulturePatrimoine.Patrimoine.9",
              description: "CulturePatrimoine.Patrimoine.9.Desc",
              image: require("./Photo/bailliage_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_05_Le_Baillage_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_05_Le_Baillage_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_05_Le_bailliage_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_05_Le_bailliage_112_NL.mp3"),
            },
            {
              latitude: 44.42843,
              longitude: 5.71457,
              title: "CulturePatrimoine.Patrimoine.10",
              description: "CulturePatrimoine.Patrimoine.10.Desc",
              image: require("./Photo/mairie_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3"),
            },
            {
              latitude: 44.42886,
              longitude: 5.71484,
              title: "CulturePatrimoine.Patrimoine.11",
              description: "CulturePatrimoine.Patrimoine.11.Desc",
              image: require("./Photo/place_auche_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_10_auche_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_10_auche_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_10_auche_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_10_auche_112_NL.mp3"),
            },
            {
              latitude: 44.42906,
              longitude: 5.71394,
              title: "CulturePatrimoine.Patrimoine.12",
              description: "CulturePatrimoine.Patrimoine.12.Desc",
              image: require("./Photo/tour_de_molend_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_12_tour_de_molend_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_12_tour_de_molend_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_12_tour_de_molend_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_12_tour_de_molend_112_NL.mp3"),
            },
            {
              latitude: 44.4294,
              longitude: 5.714449,
              title: "CulturePatrimoine.Patrimoine.13",
              description: "CulturePatrimoine.Patrimoine.13.Desc",
              image: require("./Photo/nbs_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_13_nd_bonsecours_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_13_nd_bonsecours_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_13_nd_bonsecours_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_13_nd_bonsecours_112_NL.mp3"),
            },
            {
              latitude: 44.429457,
              longitude: 5.714449,
              title: "CulturePatrimoine.Lesdiguieres.14",
              description: "CulturePatrimoine.Lesdiguieres.14.Desc",
              image: require("./Photo/chateau_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_14_chateau_serres_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_14_chateau_serres_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_14_chateau_serres_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_14_chateau_serres_112_NL.mp3"),
            },
          ],
          title: "CulturePatrimoine.Patrimoine",
          time: "3h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Patrimoine.Desc",
          difficulty: "Easy",
          image: require("./Photo/chateau_3.jpg"),
          imageGroup: [
            require("./Photo/portail_guire_1.jpg"),
            require("./Photo/portail_guire_2.jpg"),
            require("./Photo/place_auche_1.jpg"),
            require("./Photo/place_auche_2.jpg"),
            require("./Photo/place_auche_3.jpg"),
            require("./Photo/nbs_1.jpg"),
            require("./Photo/chateau_1.jpg"),
          ],
          name: "Patrimoine",
          polyline: polylinePatrimoine,
          region: {
            latitude: 44.427,
            longitude: 5.715,
            latitudeDelta: 0.01,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Lesdiguieres.0",
              description: "CulturePatrimoine.Lesdiguieres.0.Desc",
              image: require("./Photo/piece_buste_lesdiguières.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_intro_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_intro_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_intro_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_intro_112_NL.mp3"),
            },
          ],
          markers: [
            {
              latitude: 44.42861,
              longitude: 5.71529,
              title: "CulturePatrimoine.Lesdiguieres.1",
              description: "CulturePatrimoine.Lesdiguieres.1.Desc",
              image: require("./Photo/le_portalet_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_01_Le_portalet_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_01_Le_portalet_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_01_Le_portalet_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_01_Le_portalet_112_NL.mp3"),
            },
            {
              latitude: 44.42851,
              longitude: 5.71525,
              title: "CulturePatrimoine.Lesdiguieres.2",
              description: "CulturePatrimoine.Lesdiguieres.2.Desc",
              image: require("./Photo/bourg_reynaud_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_02_Bourg_Reynaud_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_02_Bourg_Reynaud_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_02_bourg_reynaud_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_02_bourg_reynaud_112_NL.mp3"),
            },
            {
              latitude: 44.42838,
              longitude: 5.71483,
              title: "CulturePatrimoine.Lesdiguieres.3",
              description: "CulturePatrimoine.Lesdiguieres.3.Desc",
              image: require("./Photo/fortifications_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_03_Fortifications_112_ok.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_03_Fortifications_112_ok_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_03_fortifications_112_ok_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_03_fortifications_112_ok_NL.mp3"),
            },
            {
              latitude: 44.42843,
              longitude: 5.71457,
              title: "CulturePatrimoine.Lesdiguieres.4",
              description: "CulturePatrimoine.Lesdiguieres.4.Desc",
              image: require("./Photo/mairie_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3"),
            },
            {
              latitude: 44.42856,
              longitude: 5.71479,
              title: "CulturePatrimoine.Lesdiguieres.5",
              description: "CulturePatrimoine.Lesdiguieres.5.Desc",
              image: require("./Photo/bailliage_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_05_Le_Baillage_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_05_Le_Baillage_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_05_Le_bailliage_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_05_Le_bailliage_112_NL.mp3"),
            },
            {
              latitude: 44.42896,
              longitude: 5.71546,
              title: "CulturePatrimoine.Lesdiguieres.6",
              description: "CulturePatrimoine.Lesdiguieres.6.Desc",
              image: require("./Photo/maison_arnaudon_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_06_demeure_arnaudon_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_06_demeure_arnaudon_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_06_demeure_arnaudon_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_06_demeure_arnaudon_112_NL.mp3"),
            },
            {
              latitude: 44.429029,
              longitude: 5.715603,
              title: "CulturePatrimoine.Lesdiguieres.7",
              description: "CulturePatrimoine.Lesdiguieres.7.Desc",
              image: require("./Photo/ancienne_maison_commune_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_07_ancienne_maison_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_07_ancienne_maison_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_07_ancienne_maison_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_07_ancienne_maison_112_NL.mp3"),
            },
            {
              latitude: 44.42917,
              longitude: 5.71609,
              title: "CulturePatrimoine.Lesdiguieres.8",
              description: "CulturePatrimoine.Lesdiguieres.8.Desc",
              image: require("./Photo/Maison_lesdig_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_08_maison_lesdiguieres_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_08_maison_lesdiguieres_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_08_maison_lesdiguieres_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3"),
            },
            {
              latitude: 44.42926,
              longitude: 5.716273,
              title: "CulturePatrimoine.Lesdiguieres.9",
              description: "CulturePatrimoine.Lesdiguieres.9.Desc",
              image: require("./Photo/st_arey_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_09_eglise_starey_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_09_eglise_starey_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_09_eglise_starey_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_09_eglise_starey_112_NL.mp3"),
            },
            {
              latitude: 44.42886,
              longitude: 5.71484,
              title: "CulturePatrimoine.Lesdiguieres.10",
              description: "CulturePatrimoine.Lesdiguieres.10.Desc",
              image: require("./Photo/place_auche_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_10_auche_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_10_auche_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_10_auche_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_10_auche_112_NL.mp3"),
            },
            {
              latitude: 44.42864,
              longitude: 5.71413,
              title: "CulturePatrimoine.Lesdiguieres.11",
              description: "CulturePatrimoine.Lesdiguieres.11.Desc",
              image: require("./Photo/maison_commune_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_11_maison_commune_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_11_maison_commune_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_11_maison_commune_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_11_maison_commune_112_NL.mp3"),
            },
            {
              latitude: 44.42906,
              longitude: 5.71394,
              title: "CulturePatrimoine.Lesdiguieres.12",
              description: "CulturePatrimoine.Lesdiguieres.12.Desc",
              image: require("./Photo/tour_de_molend_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_12_tour_de_molend_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_12_tour_de_molend_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_12_tour_de_molend_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_12_tour_de_molend_112_NL.mp3"),
            },
            {
              latitude: 44.429409,
              longitude: 5.714473,
              title: "CulturePatrimoine.Lesdiguieres.13",
              description: "CulturePatrimoine.Lesdiguieres.13.Desc",
              image: require("./Photo/nbs_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_13_nd_bonsecours_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_13_nd_bonsecours_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_13_nd_bonsecours_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_13_nd_bonsecours_112_NL.mp3"),
            },
            {
              latitude: 44.429457,
              longitude: 5.714449,
              title: "CulturePatrimoine.Lesdiguieres.14",
              description: "CulturePatrimoine.Lesdiguieres.14.Desc",
              image: require("./Photo/chateau_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_14_chateau_serres_112.mp3")
                  : langue === "en"
                  ? require("./Audio/en/PL_14_chateau_serres_112_EN.mp3")
                  : langue === "it"
                  ? require("./Audio/it/PL_14_chateau_serres_112_ITA.mp3")
                  : langue === "nl" &&
                    require("./Audio/nl/PL_14_chateau_serres_112_NL.mp3"),
            },
          ],
          title: "CulturePatrimoine.Lesdiguieres",
          time: "3h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Lesdiguieres.Desc",
          difficulty: "Easy",
          image: require("./Photo/Maison_lesdig_2.jpg"),
          imageGroup: [
            require("./Photo/bourg_reynaud_1.jpg"),
            require("./Photo/place_auche_1.jpg"),
            require("./Photo/maison_commune_1.jpg"),
            require("./Photo/tour_de_molend_1.jpg"),
            require("./Photo/nbs_1.jpg"),
            require("./Photo/maison_arnaudon_1.jpg"),
            require("./Photo/Maison_lesdig_1.jpg"),
            require("./Photo/chateau_1.jpg"),
          ],
          name: "Lesdiguieres",
          polyline: polylineLesdiguieres,
          region: {
            latitude: 44.427,
            longitude: 5.715,
            latitudeDelta: 0.01,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Estanco.0",
            },
            {
              latitude: 44.42483,
              longitude: 5.715168,
              title: "CulturePatrimoine.Estanco.1",
            },
          ],
          title: "CulturePatrimoine.Estanco",
          time: "1h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Estanco.Desc",
          difficulty: "Easy",
          image: require("./Photo/Estanco_1.png"),
          imageGroup: [
            require("./Photo/Estanco_1.png"),
            require("./Photo/Estanco_2.png"),
            require("./Photo/Estanco_3.png"),
          ],
          name: "Patrimoine",
          polyline: polylineEstanco,
          region: {
            latitude: 44.422,
            longitude: 5.715,
            latitudeDelta: 0.02,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
            },
          ],
          title: "CulturePatrimoine.Illustre",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Illustre.Desc",
          image: require("./Photo/Illustres/marie_vignon_illustre.jpg"),
          name: "Patrimoine",
          illustres: [
            {
              title: "CulturePatrimoine.Illustre.Lesdiguere",
              description: "CulturePatrimoine.Illustre.Lesdiguere.Desc",
              image: require("./Photo/Illustres/lesdiguieres_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Vignon",
              description: "CulturePatrimoine.Illustre.Vignon.Desc",
              image: require("./Photo/Illustres/marie_vignon_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Allier",
              description: "CulturePatrimoine.Illustre.Allier.Desc",
              image: require("./Photo/Illustres/adelaide_allier_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Itier",
              description: "CulturePatrimoine.Illustre.Itier.Desc",
              image: require("./Photo/Illustres/jules_itier_1.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Barrillon",
              description: "CulturePatrimoine.Illustre.Barrillon.Desc",
              image: require("./Photo/Illustres/alexandre_barillon_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Pons",
              description: "CulturePatrimoine.Illustre.Pons.Desc",
              image: require("./Photo/Illustres/jean_louis_pons_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Correard",
              description: "CulturePatrimoine.Illustre.Correard.Desc",
              image: require("./Photo/Illustres/alexandre_correard_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Ayme",
              description: "CulturePatrimoine.Illustre.Ayme.Desc",
              image: require("./Photo/Illustres/dubois_ayme_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Rochas",
              description: "CulturePatrimoine.Illustre.Rochas.Desc",
              image: require("./Photo/Illustres/beau_de_rochas.jpg"),
            },
          ],
          region: {
            latitude: 44.422,
            longitude: 5.715,
            latitudeDelta: 0.02,
            longitudeDelta: 0.004,
          },
        },
      ]
    : [
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Patrimoine.0",
              description: "CulturePatrimoine.Patrimoine.0.Desc",
              image: require("./Photo/patrimoine_archi.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_00_intro_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_00_intro_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_00_intro_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_00_intro_112_NL.mp3",
                    },
            },
          ],
          markers: [
            {
              latitude: 44.429954,
              longitude: 5.716445,
              title: "CulturePatrimoine.Patrimoine.1",
              description: "CulturePatrimoine.Patrimoine.1.Desc",
              image: require("./Photo/porte_saint_claude_1.png"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_01_porte_saint_claude_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "/langues/Audio/en/PP_01_porte_saint_claude_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_01_porte_saint_claude_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_01_porte_saint_claude_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429637,
              longitude: 5.716586,
              title: "CulturePatrimoine.Patrimoine.2",
              description: "CulturePatrimoine.Patrimoine.2.Desc",
              image: require("./Photo/maison_vieille_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_02_maison_typique_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_02_maison_typique_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_02_maison_typique_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_02_maison_typique_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42926,
              longitude: 5.716273,
              title: "CulturePatrimoine.Patrimoine.3",
              description: "CulturePatrimoine.Patrimoine.3.Desc",
              image: require("./Photo/st_arey_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_09_eglise_starey_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_09_eglise_starey_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_09_eglise_starey_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_09_eglise_starey_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42917,
              longitude: 5.71609,
              title: "CulturePatrimoine.Patrimoine.4",
              description: "CulturePatrimoine.Patrimoine.4.Desc",
              image: require("./Photo/Maison_lesdig_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_08_maison_lesdiguieres_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_08_maison_lesdiguieres_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_08_maison_lesdiguieres_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429073,
              longitude: 5.715922,
              title: "CulturePatrimoine.Patrimoine.5",
              description: "CulturePatrimoine.Patrimoine.5.Desc",
              image: require("./Photo/maison_juge_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_05_maison_juge_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_05_maison_juge_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_05_maison_juge_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_05_maison_juge_112_NL.mp3",
                    },
            },
            {
              latitude: 44.428877,
              longitude: 5.715494,
              title: "CulturePatrimoine.Patrimoine.6",
              description: "CulturePatrimoine.Patrimoine.6.Desc",
              image: require("./Photo/grenier_a_sel_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_06_grenier_sel_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_06_grenier_sel_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_06_grenier_sel_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_06_grenier_sel_112_NL.mp3",
                    },
            },
            {
              latitude: 44.428831,
              longitude: 5.715365,
              title: "CulturePatrimoine.Patrimoine.7",
              description: "CulturePatrimoine.Patrimoine.7.Desc",
              image: require("./Photo/portail_guire_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_08_portail_guire_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_08_portail_guire_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_08_portail_guire_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_08_portail_guire_112_NL.mp3",
                    },
            },
            {
              latitude: 44.428746,
              longitude: 5.715213,
              title: "CulturePatrimoine.Patrimoine.8",
              description: "CulturePatrimoine.Patrimoine.8.Desc",
              image: require("./Photo/hospitalet_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PP_07_hospitaletl_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PP_07_hospitaletl_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PP_07_hospitaletl_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_07_hospitaletl_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42856,
              longitude: 5.71479,
              title: "CulturePatrimoine.Patrimoine.9",
              description: "CulturePatrimoine.Patrimoine.9.Desc",
              image: require("./Photo/bailliage_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_05_Le_Baillage_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_05_Le_Baillage_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_05_Le_bailliage_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_05_Le_bailliage_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42843,
              longitude: 5.71457,
              title: "CulturePatrimoine.Patrimoine.10",
              description: "CulturePatrimoine.Patrimoine.10.Desc",
              image: require("./Photo/mairie_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42886,
              longitude: 5.71484,
              title: "CulturePatrimoine.Patrimoine.11",
              description: "CulturePatrimoine.Patrimoine.11.Desc",
              image: require("./Photo/place_auche_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_10_auche_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_10_auche_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_10_auche_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_10_auche_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42906,
              longitude: 5.71394,
              title: "CulturePatrimoine.Patrimoine.12",
              description: "CulturePatrimoine.Patrimoine.12.Desc",
              image: require("./Photo/tour_de_molend_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_12_tour_de_molend_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_12_tour_de_molend_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_12_tour_de_molend_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_12_tour_de_molend_112_NL.mp3",
                    },
            },
            {
              latitude: 44.4294,
              longitude: 5.714449,
              title: "CulturePatrimoine.Patrimoine.13",
              description: "CulturePatrimoine.Patrimoine.13.Desc",
              image: require("./Photo/nbs_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_13_nd_bonsecours_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_13_nd_bonsecours_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429457,
              longitude: 5.714449,
              title: "CulturePatrimoine.Lesdiguieres.14",
              description: "CulturePatrimoine.Lesdiguieres.14.Desc",
              image: require("./Photo/chateau_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_14_chateau_serres_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_14_chateau_serres_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_14_chateau_serres_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_14_chateau_serres_112_NL.mp3",
                    },
            },
          ],
          title: "CulturePatrimoine.Patrimoine",
          time: "3h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Patrimoine.Desc",
          difficulty: "Easy",
          image: require("./Photo/chateau_3.jpg"),
          imageGroup: [
            require("./Photo/portail_guire_1.jpg"),
            require("./Photo/portail_guire_2.jpg"),
            require("./Photo/place_auche_1.jpg"),
            require("./Photo/place_auche_2.jpg"),
            require("./Photo/place_auche_3.jpg"),
            require("./Photo/nbs_1.jpg"),
            require("./Photo/chateau_1.jpg"),
          ],
          name: "Patrimoine",
          polyline: polylinePatrimoine,
          region: {
            latitude: 44.427,
            longitude: 5.715,
            latitudeDelta: 0.01,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Lesdiguieres.0",
              description: "CulturePatrimoine.Lesdiguieres.0.Desc",
              image: require("./Photo/piece_buste_lesdiguières.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_intro_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_intro_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_intro_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_intro_112_NL.mp3",
                    },
            },
          ],
          markers: [
            {
              latitude: 44.42861,
              longitude: 5.71529,
              title: "CulturePatrimoine.Lesdiguieres.1",
              description: "CulturePatrimoine.Lesdiguieres.1.Desc",
              image: require("./Photo/le_portalet_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_01_Le_portalet_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_01_Le_portalet_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_01_Le_portalet_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_01_Le_portalet_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42851,
              longitude: 5.71525,
              title: "CulturePatrimoine.Lesdiguieres.2",
              description: "CulturePatrimoine.Lesdiguieres.2.Desc",
              image: require("./Photo/bourg_reynaud_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_02_Bourg_Reynaud_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_02_Bourg_Reynaud_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_02_bourg_reynaud_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_02_bourg_reynaud_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42838,
              longitude: 5.71483,
              title: "CulturePatrimoine.Lesdiguieres.3",
              description: "CulturePatrimoine.Lesdiguieres.3.Desc",
              image: require("./Photo/fortifications_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_03_Fortifications_112_ok.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_03_Fortifications_112_ok_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_03_fortifications_112_ok_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_03_fortifications_112_ok_NL.mp3",
                    },
            },
            {
              latitude: 44.42843,
              longitude: 5.71457,
              title: "CulturePatrimoine.Lesdiguieres.4",
              description: "CulturePatrimoine.Lesdiguieres.4.Desc",
              image: require("./Photo/mairie_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42856,
              longitude: 5.71479,
              title: "CulturePatrimoine.Lesdiguieres.5",
              description: "CulturePatrimoine.Lesdiguieres.5.Desc",
              image: require("./Photo/bailliage_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_05_Le_Baillage_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_05_Le_Baillage_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_05_Le_bailliage_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_05_Le_bailliage_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42896,
              longitude: 5.71546,
              title: "CulturePatrimoine.Lesdiguieres.6",
              description: "CulturePatrimoine.Lesdiguieres.6.Desc",
              image: require("./Photo/maison_arnaudon_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_06_demeure_arnaudon_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_06_demeure_arnaudon_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_06_demeure_arnaudon_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_06_demeure_arnaudon_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429029,
              longitude: 5.715603,
              title: "CulturePatrimoine.Lesdiguieres.7",
              description: "CulturePatrimoine.Lesdiguieres.7.Desc",
              image: require("./Photo/ancienne_maison_commune_2.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_07_ancienne_maison_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_07_ancienne_maison_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_07_ancienne_maison_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_07_ancienne_maison_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42917,
              longitude: 5.71609,
              title: "CulturePatrimoine.Lesdiguieres.8",
              description: "CulturePatrimoine.Lesdiguieres.8.Desc",
              image: require("./Photo/Maison_lesdig_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_08_maison_lesdiguieres_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_08_maison_lesdiguieres_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_08_maison_lesdiguieres_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42926,
              longitude: 5.716273,
              title: "CulturePatrimoine.Lesdiguieres.9",
              description: "CulturePatrimoine.Lesdiguieres.9.Desc",
              image: require("./Photo/st_arey_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_09_eglise_starey_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_09_eglise_starey_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_09_eglise_starey_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_09_eglise_starey_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42886,
              longitude: 5.71484,
              title: "CulturePatrimoine.Lesdiguieres.10",
              description: "CulturePatrimoine.Lesdiguieres.10.Desc",
              image: require("./Photo/place_auche_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_10_auche_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_10_auche_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_10_auche_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_10_auche_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42864,
              longitude: 5.71413,
              title: "CulturePatrimoine.Lesdiguieres.11",
              description: "CulturePatrimoine.Lesdiguieres.11.Desc",
              image: require("./Photo/maison_commune_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_11_maison_commune_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_11_maison_commune_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_11_maison_commune_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_11_maison_commune_112_NL.mp3",
                    },
            },
            {
              latitude: 44.42906,
              longitude: 5.71394,
              title: "CulturePatrimoine.Lesdiguieres.12",
              description: "CulturePatrimoine.Lesdiguieres.12.Desc",
              image: require("./Photo/tour_de_molend_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_12_tour_de_molend_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_12_tour_de_molend_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_12_tour_de_molend_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_12_tour_de_molend_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429409,
              longitude: 5.714473,
              title: "CulturePatrimoine.Lesdiguieres.13",
              description: "CulturePatrimoine.Lesdiguieres.13.Desc",
              image: require("./Photo/nbs_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_13_nd_bonsecours_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_13_nd_bonsecours_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3",
                    },
            },
            {
              latitude: 44.429457,
              longitude: 5.714449,
              title: "CulturePatrimoine.Lesdiguieres.14",
              description: "CulturePatrimoine.Lesdiguieres.14.Desc",
              image: require("./Photo/chateau_1.jpg"),
              audio:
                langue === "fr"
                  ? require("./Audio/fr/PL_14_chateau_serres_112.mp3")
                  : langue === "en"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/en/PL_14_chateau_serres_112_EN.mp3",
                    }
                  : langue === "it"
                  ? {
                      uri: "http://137.74.198.215:3000/langues/Audio/it/PL_14_chateau_serres_112_ITA.mp3",
                    }
                  : langue === "nl" && {
                      uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_14_chateau_serres_112_NL.mp3",
                    },
            },
          ],
          title: "CulturePatrimoine.Lesdiguieres",
          time: "3h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Lesdiguieres.Desc",
          difficulty: "Easy",
          image: require("./Photo/Maison_lesdig_2.jpg"),
          imageGroup: [
            require("./Photo/bourg_reynaud_1.jpg"),
            require("./Photo/place_auche_1.jpg"),
            require("./Photo/maison_commune_1.jpg"),
            require("./Photo/tour_de_molend_1.jpg"),
            require("./Photo/nbs_1.jpg"),
            require("./Photo/maison_arnaudon_1.jpg"),
            require("./Photo/Maison_lesdig_1.jpg"),
            require("./Photo/chateau_1.jpg"),
          ],
          name: "Lesdiguieres",
          polyline: polylineLesdiguieres,
          region: {
            latitude: 44.427,
            longitude: 5.715,
            latitudeDelta: 0.01,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
              title: "CulturePatrimoine.Estanco.0",
            },
            {
              latitude: 44.42483,
              longitude: 5.715168,
              title: "CulturePatrimoine.Estanco.1",
            },
          ],
          title: "CulturePatrimoine.Estanco",
          time: "1h",
          distance: "2km",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Estanco.Desc",
          difficulty: "Easy",
          image: require("./Photo/Estanco_1.png"),
          imageGroup: [
            require("./Photo/Estanco_1.png"),
            require("./Photo/Estanco_2.png"),
            require("./Photo/Estanco_3.png"),
          ],
          name: "Patrimoine",
          polyline: polylineEstanco,
          region: {
            latitude: 44.422,
            longitude: 5.715,
            latitudeDelta: 0.02,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
            },
          ],
          title: "CulturePatrimoine.Illustre",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Illustre.Desc",
          image: require("./Photo/Illustres/marie_vignon_illustre.jpg"),
          name: "Patrimoine",
          illustres: [
            {
              title: "CulturePatrimoine.Illustre.Lesdiguere",
              description: "CulturePatrimoine.Illustre.Lesdiguere.Desc",
              image: require("./Photo/Illustres/lesdiguieres_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Vignon",
              description: "CulturePatrimoine.Illustre.Vignon.Desc",
              image: require("./Photo/Illustres/marie_vignon_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Allier",
              description: "CulturePatrimoine.Illustre.Allier.Desc",
              image: require("./Photo/Illustres/adelaide_allier_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Itier",
              description: "CulturePatrimoine.Illustre.Itier.Desc",
              image: require("./Photo/Illustres/jules_itier_1.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Barrillon",
              description: "CulturePatrimoine.Illustre.Barrillon.Desc",
              image: require("./Photo/Illustres/alexandre_barillon_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Pons",
              description: "CulturePatrimoine.Illustre.Pons.Desc",
              image: require("./Photo/Illustres/jean_louis_pons_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Correard",
              description: "CulturePatrimoine.Illustre.Correard.Desc",
              image: require("./Photo/Illustres/alexandre_correard_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Ayme",
              description: "CulturePatrimoine.Illustre.Ayme.Desc",
              image: require("./Photo/Illustres/dubois_ayme_illustre.jpg"),
            },
            {
              title: "CulturePatrimoine.Illustre.Rochas",
              description: "CulturePatrimoine.Illustre.Rochas.Desc",
              image: require("./Photo/Illustres/beau_de_rochas.jpg"),
            },
          ],
          region: {
            latitude: 44.422,
            longitude: 5.715,
            latitudeDelta: 0.02,
            longitudeDelta: 0.004,
          },
        },
        {
          startEndMarker: [
            {
              latitude: 44.429274,
              longitude: 5.716975,
            },
          ],
          title: "CulturePatrimoine.Artistique",
          type: require("./Family.png"),
          description: "CulturePatrimoine.Artistique.Desc",
          image: require("./Photo/coming-soon.jpg"),
          name: "Patrimoine",
          region: {
            latitude: 44.422,
            longitude: 5.715,
            latitudeDelta: 0.02,
            longitudeDelta: 0.004,
          },
        },
      ];
