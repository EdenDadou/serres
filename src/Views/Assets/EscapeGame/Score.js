export const Score = [
  {
    score: "EscapeGame.score.03",
    title: "EscapeGame.score.03.title",
    promo: "EscapeGame.score.03.promo",
    desc: "EscapeGame.score.03.desc",
    image: require("./image/soldat_0a3.jpg"),
  },
  {
    score: "EscapeGame.score.46",
    title: "EscapeGame.score.46.title",
    promo: "EscapeGame.score.46.promo",
    desc: "EscapeGame.score.46.desc",
    image: require("./image/coligny_4a6.jpg"),
  },
  {
    score: "EscapeGame.score.78",
    title: "EscapeGame.score.78.title",
    promo: "EscapeGame.score.78.promo",
    desc: "EscapeGame.score.78.desc",
    image: require("./image/medicis_7a8.jpg"),
  },
  {
    score: "EscapeGame.score.910",
    title: "EscapeGame.score.910.title",
    promo: "EscapeGame.score.910.promo",
    desc: "EscapeGame.score.910.desc",
    image: require("./image/bayard_9a10.jpg"),
  },
];
