import {
  polylineMoine,
  polylineSources,
  polylineGineste,
  polylineGermanette,
  polylineBordEau,
  polylineBaumont,
  polylineArambe,
  polylineRaphyTest,
} from "./Polylines";
import { Platform } from "react-native";

export const ParcoursNatureLoisir = (langue) => [
  //***** Sentier des Moines  ******/
  {
    startEndMarker: [
      {
        latitude: 44.429257,
        longitude: 5.716931,
      },
    ],
    markers: [
      {
        latitude: 44.4293,
        longitude: 5.7153,
        title: "NatureLoisirs.SentierMoines.0",
        description: "NatureLoisirs.SentierMoines.0.Desc",
        image: require("./SentierMoines/nbs_1.jpg"),
        audio:
          langue === "fr"
            ? require("../CulturePatrimoine/Audio/fr/PL_13_nd_bonsecours_112.mp3")
            : langue === "en"
            ? Platform === "android"
              ? {
                  uri: "http://137.74.198.215:3000/langues/Audio/en/PL_13_nd_bonsecours_112_EN.mp3",
                }
              : require("../CulturePatrimoine/Audio/en/PL_13_nd_bonsecours_112_EN.mp3")
            : langue === "it"
            ? Platform === "android"
              ? {
                  uri: "http://137.74.198.215:3000/langues/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3",
                }
              : require("../CulturePatrimoine/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3")
            : langue === "nl" && Platform === "android"
            ? {
                uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3",
              }
            : require("../CulturePatrimoine/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3"),
      },
      {
        latitude: 44.42952,
        longitude: 5.71099,
        title: "NatureLoisirs.SentierMoines.1",
        description: "NatureLoisirs.SentierMoines.1.Desc",
        image: require("./SentierMoines/tombe_juive_2.jpg"),
        audio:
          langue === "fr"
            ? require("./SentierMoines/fr/TombeauJuif_96kbps.mp3")
            : langue === "en"
            ? require("./SentierMoines/en/tombeau_juif_112_EN.mp3")
            : langue === "it"
            ? require("./SentierMoines/it/tombeau_juif_112_ITA.mp3")
            : langue === "nl" &&
              require("./SentierMoines/nl/tombeau_juif_112_NL.mp3"),
      },
      {
        latitude: 44.431834,
        longitude: 5.697435,
        title: "NatureLoisirs.SentierMoines.2",
        description: "NatureLoisirs.SentierMoines.2.Desc",
        image: require("./SentierMoines/Chapelle_Saumane_Cloche.jpg"),
        audio:
          langue === "fr"
            ? require("./SentierMoines/fr/Chapelle_de_saumane.mp3")
            : langue === "en"
            ? require("./SentierMoines/en/chapelle_saumane_112_EN.mp3")
            : langue === "it"
            ? require("./SentierMoines/it/chapelle_saumane_112_ITA.mp3")
            : langue === "nl" &&
              require("./SentierMoines/nl/chapelle_saumane_112_NL.mp3"),
      },
      {
        latitude: 44.428038,
        longitude: 5.712805,
        title: "NatureLoisirs.SentierMoines.3",
        description: "NatureLoisirs.SentierMoines.3.Desc",
        image: require("../CulturePatrimoine/Photo/meridienne_1.jpg"),
        audio:
          langue === "fr"
            ? require("./SentierMoines/fr/meridienne_serres_112.mp3")
            : langue === "en"
            ? require("./SentierMoines/en/meridienne_serres_112_EN.mp3")
            : langue === "it"
            ? require("./SentierMoines/it/meridienne_serres_112_ITA.mp3")
            : langue === "nl" &&
              require("./SentierMoines/nl/meridienne_serres_112_NL.mp3"),
      },
    ],
    startEndMarker: [
      {
        latitude: 44.429114,
        longitude: 5.717449,
      },
    ],
    title: "NatureLoisirs.SentierMoines",
    time: "3h",
    distance: "6km",
    type: require("./Family.png"),
    description: "NatureLoisirs.SentierMoines.Desc",
    difficulty: "Medium",
    image: require("./SentierMoines/ChapelleSaumane.jpg"),
    imageGroup: [
      require("./SentierMoines/Chapelle_Saumane_Cloche.jpg"),
      require("./SentierMoines/ChapelleSaumane.jpg"),
      require("./SentierMoines/nbs_1.jpg"),
      require("./SentierMoines/nbs_2.jpg"),
      require("./SentierMoines/vierge_saumane.jpg"),
      require("./SentierMoines/tombe_juive.jpg"),
      require("./SentierMoines/tombe_juive_2.jpg"),
    ],
    name: "SentierMoines",
    polyline: polylineMoine,
    region: {
      latitude: 44.423,
      longitude: 5.705,
      latitudeDelta: 0.04,
      longitudeDelta: 0.004,
    },
  },
  //***** Bord du buech  ******/
  {
    startEndMarker: [
      {
        latitude: 44.429114,
        longitude: 5.717449,
      },
      {
        latitude: 44.43278,
        longitude: 5.712576,
      },
    ],
    title: "NatureLoisirs.BordEau",
    time: "10min",
    distance: "1km",
    type: require("./Family.png"),
    description: "NatureLoisirs.BordEau.Desc",
    difficulty: "Easy",
    image: require("./BordEau/plage_galets.jpg"),
    logo: require("./BordEau/Natura_2000.png"),
    name: "Bord du Buech",
    polyline: polylineBordEau,
    region: {
      latitude: 44.425,
      longitude: 5.715,
      latitudeDelta: 0.005,
      longitudeDelta: 0.02,
    },
  },
  //***** Germanette ******/
  {
    startEndMarker: [
      {
        latitude: 44.429114,
        longitude: 5.717449,
      },
      {
        latitude: 44.406949,
        longitude: 5.726059,
      },
    ],
    title: "NatureLoisirs.BaseGermanette",
    time: "0h45h",
    distance: "3km",
    type: require("./Family.png"),
    description: "NatureLoisirs.BaseGermanette.Desc",
    difficulty: "Easy",
    image: require("./Germanette/Germanette_2.jpg"),
    imageGroup: [
      require("./Germanette/Germanette_2.jpg"),
      require("./Germanette/Germanette_1.jpg"),
      require("./Germanette/germanette_logo.jpg"),
    ],
    name: "Bord du Buech",
    polyline: polylineGermanette,
    region: {
      latitude: 44.4,
      longitude: 5.72,
      latitudeDelta: 0.006,
      longitudeDelta: 0.06,
    },
  },
  //***** Sentier Gineste  ******/
  {
    startEndMarker: [
      {
        latitude: 44.429159,
        longitude: 5.71732,
      },
    ],
    markers: [
      {
        latitude: 44.423853,
        longitude: 5.719106,
        title: "NatureLoisirs.SentierGineste.1",
      },
      {
        latitude: 44.421136,
        longitude: 5.721466,
        title: "NatureLoisirs.SentierGineste.2",
      },
      {
        latitude: 44.415803,
        longitude: 5.732973,
        title: "NatureLoisirs.SentierGineste.3",
      },
      {
        latitude: 44.408136,
        longitude: 5.726439,
        title: "NatureLoisirs.SentierGineste.4",
      },
    ],
    title: "NatureLoisirs.SentierGineste",
    time: "2h30h",
    distance: "7,5km",
    type: require("./Medium.png"),
    description: "NatureLoisirs.SentierGineste.Desc",
    difficulty: "Easy",
    image: require("./SentierGineste/petite_gineste.jpg"),
    name: "Sentier de la Gineste",
    polyline: polylineGineste,
    region: {
      latitude: 44.405,
      longitude: 5.72,
      latitudeDelta: 0.06,
      longitudeDelta: 0.004,
    },
  },
  //***** Balcon des Sources ******/
  {
    startEndMarker: [
      {
        latitude: 44.514165,
        longitude: 5.637888,
      },
    ],
    markers: [
      {
        latitude: 44.51447,
        longitude: 5.633312,
        title: "NatureLoisirs.SentierSources.1",
      },
      {
        latitude: 44.523727,
        longitude: 5.62372,
        title: "NatureLoisirs.SentierSources.2",
      },
      {
        latitude: 44.535177,
        longitude: 5.625984,
        title: "NatureLoisirs.SentierSources.3",
      },
      {
        latitude: 44.519451,
        longitude: 5.647613,
        title: "NatureLoisirs.SentierSources.4",
      },
      {
        latitude: 44.515488,
        longitude: 5.652174,
        title: "NatureLoisirs.SentierSources.5",
      },
    ],
    title: "NatureLoisirs.SentierSources",
    time: "5h",
    distance: "12km",
    type: require("./Hard.png"),
    description: "NatureLoisirs.SentierSources.Desc",
    difficulty: "Medium",
    image: require("./SentierSources/sources.jpg"),
    name: "SentierSources",
    polyline: polylineSources,
    region: {
      latitude: 44.503,
      longitude: 5.63,
      latitudeDelta: 0.09,
      longitudeDelta: 0.004,
    },
  },
  //***** Rocher de Baumont  ******/
  {
    startEndMarker: [
      {
        latitude: 44.414575,
        longitude: 5.681037,
      },
    ],
    markers: [
      {
        latitude: 44.406963,
        longitude: 5.682225,
        title: "NatureLoisirs.Baumont.1",
      },
      {
        latitude: 44.392076,
        longitude: 5.685395,
        title: "NatureLoisirs.Baumont.2",
      },
      {
        latitude: 44.385601,
        longitude: 5.685202,
        title: "NatureLoisirs.Baumont.3",
      },
      {
        latitude: 44.382239,
        longitude: 5.685336,
        title: "NatureLoisirs.Baumont.4",
      },
    ],
    title: "NatureLoisirs.Baumont",
    time: "4h",
    distance: "13km",
    type: require("./Hard.png"),
    description: "NatureLoisirs.Baumont.Desc",
    difficulty: "Medium",
    image: require("./Baumont/Beaumont_clean.png"),
    name: "Baumont",
    polyline: polylineBaumont,
    region: {
      latitude: 44.375,
      longitude: 5.68,
      latitudeDelta: 0.1,
      longitudeDelta: 0.001,
    },
  },
  //***** Montagne d'Arambe  ******/
  {
    startEndMarker: [
      {
        latitude: 44.429159,
        longitude: 5.71732,
      },
    ],
    markers: [
      {
        latitude: 44.423853,
        longitude: 5.719106,
        title: "NatureLoisirs.Arambe.1",
      },
      {
        latitude: 44.417704,
        longitude: 5.731181,
        title: "NatureLoisirs.Arambe.2",
      },
      {
        latitude: 44.411948,
        longitude: 5.750053,
        title: "NatureLoisirs.Arambe.3",
      },
      {
        latitude: 44.414017,
        longitude: 5.756062,
        title: "NatureLoisirs.Arambe.4",
      },
      {
        latitude: 44.430162,
        longitude: 5.745172,
        title: "NatureLoisirs.Arambe.5",
      },
      {
        latitude: 44.427692,
        longitude: 5.723998,
        title: "NatureLoisirs.Arambe.6",
      },
    ],
    title: "NatureLoisirs.Arambe",
    time: "3h30",
    distance: "7km",
    type: require("./Hard.png"),
    description: "NatureLoisirs.Arambe.Desc",
    difficulty: "Medium",
    image: require("./Arambe/arambre_clean.jpg"),
    name: "Arambe",
    polyline: polylineArambe,
    region: {
      latitude: 44.4,
      longitude: 5.74,
      latitudeDelta: 0.1,
      longitudeDelta: 0.001,
    },
  },
];
