export const MarkerParcourLibre = (langue) => [
  {
    name: "ParcourLibre.Lesdiguiere",
    desc: "ParcourLibre.Lesdiguiere.Desc",
    latitude: 44.42917,
    longitude: 5.71609,
    logo: require("./chevalier.png"),
    image: require("../CulturePatrimoine/Photo/Maison_lesdig_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_08_maison_lesdiguieres_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_08_maison_lesdiguieres_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_08_maison_lesdiguieres_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_08_maison_lesdiguieres_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_08_maison_lesdiguieres_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_08_maison_lesdiguieres_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Arnaudon",
    desc: "ParcourLibre.Arnaudon.Desc",
    latitude: 44.42896,
    longitude: 5.71546,
    logo: require("./maison_remarquable.png"),
    image: require("../CulturePatrimoine/Photo/maison_arnaudon_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_06_demeure_arnaudon_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_06_demeure_arnaudon_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_06_demeure_arnaudon_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_06_demeure_arnaudon_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_06_demeure_arnaudon_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_06_demeure_arnaudon_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_06_demeure_arnaudon_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Baillage",
    desc: "ParcourLibre.Baillage.Desc",
    longitude: 5.71479,
    latitude: 4442856,
    logo: require("./batiment_important.png"),
    image: require("../CulturePatrimoine/Photo/bailliage_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_05_Le_Baillage_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_05_Le_Baillage_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_05_Le_Baillage_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_05_Le_bailliage_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_05_Le_bailliage_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_05_Le_bailliage_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_05_Le_bailliage_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.MaisonCommune",
    desc: "ParcourLibre.MaisonCommune.Desc",
    longitude: 5.714116,
    latitude: 44.428588,
    logo: require("./batiment_important.png"),
    image: require("../CulturePatrimoine/Photo/maison_commune_2.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_11_maison_commune_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_11_maison_commune_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_11_maison_commune_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_11_maison_commune_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_11_maison_commune_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_11_maison_commune_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_11_maison_commune_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Mairie",
    desc: "ParcourLibre.Mairie.Desc",
    longitude: 5.71457,
    latitude: 44.42843,
    logo: require("./monument_1.png"),
    image: require("../CulturePatrimoine/Photo/mairie_2.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_04_Mairie_Demeure_perrinet_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_04_Mairie_Demeure_perrinet_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_04_Mairie_Demeure_perrinet_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_04_Mairie_Demeure_perrinet_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Fortification",
    desc: "ParcourLibre.Fortification.Desc",
    longitude: 5.71483,
    latitude: 44.42838,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/fortifications_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_03_Fortifications_112_ok.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_03_Fortifications_112_ok_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_03_Fortifications_112_ok_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_03_fortifications_112_ok_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_03_fortifications_112_ok_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_03_fortifications_112_ok_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_03_fortifications_112_ok_NL.mp3"),
  },
  {
    name: "ParcourLibre.Auche",
    desc: "ParcourLibre.Auche.Desc",
    longitude: 5.71484,
    latitude: 44.42886,
    logo: require("./infos.png"),
    image: require("../CulturePatrimoine/Photo/place_auche_2.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_10_auche_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_10_auche_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_10_auche_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_10_auche_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_10_auche_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_10_auche_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_10_auche_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.SaintClaude",
    desc: "ParcourLibre.SaintClaude.Desc",
    longitude: 5.716445,
    latitude: 44.429954,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/porte_saint_claude_1.png"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_01_porte_saint_claude_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_01_porte_saint_claude_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_01_porte_saint_claude_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_01_porte_saint_claude_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_01_porte_saint_claude_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_01_porte_saint_claude_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_01_porte_saint_claude_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Rochas",
    desc: "ParcourLibre.Rochas.Desc",
    longitude: 5.716586,
    latitude: 44.429637,
    logo: require("./maison_remarquable.png"),
    image: require("../CulturePatrimoine/Photo/maison_vieille_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_02_maison_typique_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_02_maison_typique_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_02_maison_typique_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_02_maison_typique_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_02_maison_typique_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_02_maison_typique_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_02_maison_typique_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.SaintAray",
    desc: "ParcourLibre.SaintAray.Desc",
    latitude: 44.42926,
    longitude: 5.716273,
    logo: require("./eglise.png"),
    image: require("../CulturePatrimoine/Photo/st_arey_2.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_09_eglise_starey_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_09_eglise_starey_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_09_eglise_starey_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_09_eglise_starey_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_09_eglise_starey_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_09_eglise_starey_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_09_eglise_starey_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.JugeDePaix",
    desc: "ParcourLibre.JugeDePaix.Desc",
    latitude: 44.429073,
    longitude: 5.715922,
    logo: require("./batiment_important.png"),
    image: require("../CulturePatrimoine/Photo/maison_juge_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_05_maison_juge_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_05_maison_juge_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_05_maison_juge_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_05_maison_juge_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_05_maison_juge_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_05_maison_juge_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_05_maison_juge_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Grenier",
    desc: "ParcourLibre.Grenier.Desc",
    latitude: 44.428877,
    longitude: 5.715494,
    logo: require("./batiment_important.png"),
    image: require("../CulturePatrimoine/Photo/grenier_a_sel_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_06_grenier_sel_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_06_grenier_sel_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_06_grenier_sel_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_06_grenier_sel_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_06_grenier_sel_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_06_grenier_sel_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_06_grenier_sel_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Guire",
    desc: "ParcourLibre.Guire.Desc",
    latitude: 44.428831,
    longitude: 5.715365,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/portail_guire_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_08_portail_guire_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_08_portail_guire_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_08_portail_guire_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_08_portail_guire_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_08_portail_guire_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_08_portail_guire_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_08_portail_guire_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Madeleine",
    desc: "ParcourLibre.Madeleine.Desc",
    latitude: 44.428746,
    longitude: 5.715213,
    logo: require("./batiment_important.png"),
    image: require("../CulturePatrimoine/Photo/hospitalet_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PP_07_hospitaletl_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PP_07_hospitaletl_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PP_07_hospitaletl_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PP_07_hospitaletl_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PP_07_hospitaletl_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PP_07_hospitaletl_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PP_07_hospitaletl_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Portalet",
    desc: "ParcourLibre.Portalet.Desc",
    longitude: 5.71529,
    latitude: 44.42861,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/le_portalet_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_01_Le_portalet_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_01_Le_portalet_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_01_Le_portalet_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_01_Le_portalet_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_01_Le_portalet_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_01_Le_portalet_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_01_Le_portalet_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Reynaud",
    desc: "ParcourLibre.Reynaud.Desc",
    longitude: 5.71525,
    latitude: 44.42851,
    logo: require("./etoile_de_david.png"),
    image: require("../CulturePatrimoine/Photo/bourg_reynaud_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_02_Bourg_Reynaud_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_02_Bourg_Reynaud_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_02_Bourg_Reynaud_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_02_bourg_reynaud_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_02_bourg_reynaud_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_02_bourg_reynaud_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_02_bourg_reynaud_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Bonsecours",
    desc: "ParcourLibre.Bonsecours.Desc",
    longitude: 5.71438,
    latitude: 44.42942,
    logo: require("./eglise.png"),
    image: require("../NatureLoisirs/SentierMoines/nbs_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_13_nd_bonsecours_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_13_nd_bonsecours_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_13_nd_bonsecours_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_13_nd_bonsecours_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_13_nd_bonsecours_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Tombeau",
    desc: "ParcourLibre.Tombeau.Desc",
    longitude: 5.71099,
    latitude: 44.42952,
    logo: require("./etoile_de_david.png"),
    image: require("../NatureLoisirs/SentierMoines/tombe_juive_2.jpg"),
    audio:
      langue === "fr"
        ? require("../NatureLoisirs/SentierMoines/fr/TombeauJuif_96kbps.mp3")
        : langue === "en"
        ? require("../NatureLoisirs/SentierMoines/en/tombeau_juif_112_EN.mp3")
        : langue === "it"
        ? require("../NatureLoisirs/SentierMoines/it/tombeau_juif_112_ITA.mp3")
        : langue === "nl" &&
          require("../NatureLoisirs/SentierMoines/nl/tombeau_juif_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Saumane",
    desc: "ParcourLibre.Saumane.Desc",
    longitude: 5.697435,
    latitude: 44.431834,
    logo: require("./eglise.png"),
    image: require("../NatureLoisirs/SentierMoines/ChapelleSaumane.jpg"),
    audio:
      langue === "fr"
        ? require("../NatureLoisirs/SentierMoines/fr/Chapelle_de_saumane.mp3")
        : langue === "en"
        ? require("../NatureLoisirs/SentierMoines/en/chapelle_saumane_112_EN.mp3")
        : langue === "it"
        ? require("../NatureLoisirs/SentierMoines/it/chapelle_saumane_112_ITA.mp3")
        : langue === "nl" &&
          require("../NatureLoisirs/SentierMoines/nl/chapelle_saumane_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Méridienne",
    desc: "ParcourLibre.Méridienne.Desc",
    longitude: 5.712805,
    latitude: 44.428038,
    logo: require("./monument_1.png"),
    image: require("./meridienne.jpeg"),
    audio:
      langue === "fr"
        ? require("../NatureLoisirs/SentierMoines/fr/meridienne_serres_112.mp3")
        : langue === "en"
        ? require("../NatureLoisirs/SentierMoines/en/meridienne_serres_112_EN.mp3")
        : langue === "it"
        ? require("../NatureLoisirs/SentierMoines/it/meridienne_serres_112_ITA.mp3")
        : langue === "nl" &&
          require("../NatureLoisirs/SentierMoines/nl/meridienne_serres_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Molend",
    desc: "ParcourLibre.Molend.Desc",
    longitude: 5.71394,
    latitude: 44.42906,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/tour_de_molend_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_12_tour_de_molend_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_12_tour_de_molend_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_12_tour_de_molend_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_12_tour_de_molend_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_12_tour_de_molend_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_12_tour_de_molend_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_12_tour_de_molend_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.AncienneMaisonCommune",
    desc: "ParcourLibre.AncienneMaisonCommune.Desc",
    latitude: 44.429034,
    longitude: 5.715614,
    logo: require("./monument_1.png"),
    image: require("../CulturePatrimoine/Photo/ancienne_maison_commune_2.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_07_ancienne_maison_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_07_ancienne_maison_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_07_ancienne_maison_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_07_ancienne_maison_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_07_ancienne_maison_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_07_ancienne_maison_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_07_ancienne_maison_112_NL.mp3"),
  },
  {
    name: "ParcourLibre.Chateau",
    desc: "ParcourLibre.Chateau.Desc",
    latitude: 44.429457,
    longitude: 5.714449,
    logo: require("./fort.png"),
    image: require("../CulturePatrimoine/Photo/chateau_1.jpg"),
    audio:
      langue === "fr"
        ? require("../CulturePatrimoine/Audio/fr/PL_14_chateau_serres_112.mp3")
        : langue === "en"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/en/PL_14_chateau_serres_112_EN.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/en/PL_14_chateau_serres_112_EN.mp3",
            }
        : langue === "it"
        ? Platform.OS === "ios"
          ? require("../CulturePatrimoine/Audio/it/PL_14_chateau_serres_112_ITA.mp3")
          : {
              uri: "http://137.74.198.215:3000/langues/Audio/it/PL_14_chateau_serres_112_ITA.mp3",
            }
        : langue === "nl" && Platform.OS === "android"
        ? {
            uri: "http://137.74.198.215:3000/langues/Audio/nl/PL_14_chateau_serres_112_NL.mp3",
          }
        : require("../CulturePatrimoine/Audio/nl/PL_14_chateau_serres_112_NL.mp3"),
  },
];
