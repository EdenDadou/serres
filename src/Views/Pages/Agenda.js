import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
	StyleSheet,
	Text,
	View,
	Dimensions,
	TouchableOpacity,
	ScrollView,
	Image,
	BackHandler,
} from "react-native";
import { ListItem, Icon, Divider } from "react-native-elements";
import { Feather, Entypo } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import { getEvent } from "../../Services/services";
import { URL } from "../../Services/config";
import moment from "moment";

var urlList = [
	require("../Assets/Agenda/CalendarNumber/1.png"),
	require("../Assets/Agenda/CalendarNumber/2.png"),
	require("../Assets/Agenda/CalendarNumber/3.png"),
	require("../Assets/Agenda/CalendarNumber/4.png"),
	require("../Assets/Agenda/CalendarNumber/5.png"),
	require("../Assets/Agenda/CalendarNumber/6.png"),
	require("../Assets/Agenda/CalendarNumber/7.png"),
	require("../Assets/Agenda/CalendarNumber/8.png"),
	require("../Assets/Agenda/CalendarNumber/9.png"),
	require("../Assets/Agenda/CalendarNumber/10.png"),
	require("../Assets/Agenda/CalendarNumber/12.png"),
	require("../Assets/Agenda/CalendarNumber/13.png"),
	require("../Assets/Agenda/CalendarNumber/14.png"),
	require("../Assets/Agenda/CalendarNumber/15.png"),
	require("../Assets/Agenda/CalendarNumber/16.png"),
	require("../Assets/Agenda/CalendarNumber/17.png"),
	require("../Assets/Agenda/CalendarNumber/18.png"),
	require("../Assets/Agenda/CalendarNumber/19.png"),
	require("../Assets/Agenda/CalendarNumber/10.png"),
	require("../Assets/Agenda/CalendarNumber/21.png"),
	require("../Assets/Agenda/CalendarNumber/22.png"),
	require("../Assets/Agenda/CalendarNumber/23.png"),
	require("../Assets/Agenda/CalendarNumber/24.png"),
	require("../Assets/Agenda/CalendarNumber/25.png"),
	require("../Assets/Agenda/CalendarNumber/26.png"),
	require("../Assets/Agenda/CalendarNumber/27.png"),
	require("../Assets/Agenda/CalendarNumber/28.png"),
	require("../Assets/Agenda/CalendarNumber/29.png"),
	require("../Assets/Agenda/CalendarNumber/30.png"),
	require("../Assets/Agenda/CalendarNumber/31.png"),
];

var monthAbbr = [
	"Mois.Jan",
	"Mois.Feb",
	"Mois.Mar",
	"Mois.Apr",
	"Mois.May",
	"Mois.Jun",
	"Mois.Jul",
	"Mois.Aug",
	"Mois.Sept",
	"Mois.Oct",
	"Mois.Nov",
	"Mois.Dec",
];

class Agenda extends Component {
	constructor(props) {
		super(props);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.state = {
			currentMonth: undefined,
			events: undefined,
			toggleEvent: false,
			presenceEvent: false,
		};
	}

	componentDidMount() {
		const month = new Date().toDateString().split(" ")[1];
		this.setState({ currentMonth: `Mois.${month}` });
		getEvent()
			.then((response) => {
				this.setState({ events: response.data });
			})
			.catch((err) => console.log(err));

		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	handleBackButtonClick() {
		this.props.navigation.navigate("Menu");
		return true;
	}

	nextMonth = () => {
		let { currentMonth } = this.state;
		if (currentMonth) {
			const equal = (item) => item === currentMonth;
			var monthIndex = monthAbbr.findIndex(equal);
			var nextMonth =
				monthIndex !== 11 ? monthAbbr[monthIndex + 1] : "Mois.Jan";
			this.setState({ currentMonth: nextMonth });
		}
	};

	previousMonth = () => {
		let { currentMonth } = this.state;
		if (currentMonth) {
			const equal = (item) => item === currentMonth;
			var monthIndex = monthAbbr.findIndex(equal);
			var previousMonth =
				monthIndex !== 0 ? monthAbbr[monthIndex - 1] : "Mois.Dec";
			this.setState({ currentMonth: previousMonth });
		}
	};

	toggleEvent = (idItem) => {
		if (!this.state.toggleEvent || this.state.toggleEvent !== idItem) {
			this.setState({ toggleEvent: idItem });
		} else {
			this.setState({ toggleEvent: false });
		}
	};

	render() {
		const sortedEventByCurrentMonth = () => {
			let { events, currentMonth } = this.state;
			const equal = (item) => item === currentMonth;
			var monthIndex = monthAbbr.findIndex(equal);
			let arrayOfEvent = events?.filter((item) => {
				let eventMonth = item.date_debut.split("/")[1];
				let monthSelected = monthIndex + 1;
				if (Number(eventMonth) === Number(monthSelected)) {
					return true;
				}
			});
			return arrayOfEvent;
		};

		let eventItem = (item) => {
			var date = item.date_debut;
			var date_fin = item.date_fin;
			let indexDay = date.split("/")[0] - 1;
			var urlImage = urlList[indexDay];
			return (
				<ListItem
					key={item._id}
					bottomDivider
					onPress={() => this.toggleEvent(item._id)}
					containerStyle={
						this.state.toggleEvent === date
							? {
									backgroundColor: "#E2E7F0",
									shadowColor: "black",
									shadowOffset: {
										width: 0,
										height: 20,
									},
									shadowOpacity: 0.52,
									shadowRadius: 5.46,
									elevation: 40,
									width: width,
							  }
							: {
									width: width,
							  }
					}
				>
					<ListItem.Content
						style={[
							{
								flexDirection: "colum",
								justifyContent: "flex-start",
							},
						]}
					>
						<View
							style={[{ flexDirection: "row", justifyContent: "flex-start" }]}
						>
							<Image source={urlImage} resizeMode={"cover"} />

							<ListItem.Title
								style={[
									styles.textEventTitle,
									this.state.toggleEvent === date && {
										fontWeight: "500",
									},
								]}
								bottomDivider
							>
								{item.titre}
							</ListItem.Title>
						</View>
						{this.state.toggleEvent === item._id && (
							<View>
								<Image
									style={styles.imgEvent}
									source={{ uri: URL + "/" + item.photo }}
									resizeMode={"cover"}
								/>
								<Text style={{ marginBottom: 10 }}>
									{date_fin ? `du : ${date} au ${date_fin}` : date}
								</Text>

								<Text
									adjustsFontSizeToFit
									style={[
										styles.textDescEvent,
										this.state.toggleEvent === item._id && {
											fontWeight: "300",
										},
									]}
								>
									{item.desc}
								</Text>
							</View>
						)}
					</ListItem.Content>
					{this.state.toggleEvent !== date ? (
						<ListItem.Chevron size={25} color={"black"} />
					) : (
						<Entypo name="chevron-down" size={24} color="black" />
					)}
				</ListItem>
			);
		};
		return (
			<View style={styles.container}>
				<View style={styles.containerHeader}>
					<TouchableOpacity
						onPress={() => this.props.navigation.navigate("Menu")}
						style={{ marginHorizontal: 10 }}
					>
						<Feather name="menu" size={40} color="white" />
					</TouchableOpacity>

					<Text adjustsFontSizeToFit style={styles.textHeader}>
						{translate("Menu.Agenda", this.props.Locale)}
					</Text>
				</View>
				<Divider style={styles.divider} />
				<View style={styles.containerHeaderMenu}>
					<View style={styles.containerAgenda}>
						<TouchableOpacity onPress={() => this.previousMonth(monthAbbr)}>
							<Feather name="arrow-left-circle" size={32} color="black" />
						</TouchableOpacity>
						<Text adjustsFontSizeToFit style={styles.textMonth}>
							{translate(this.state.currentMonth, this.props.Locale)}
						</Text>

						<TouchableOpacity onPress={() => this.nextMonth(monthAbbr)}>
							<Feather name="arrow-right-circle" size={32} color="black" />
						</TouchableOpacity>
					</View>
				</View>
				<Divider style={styles.divider} />
				<ScrollView
					style={[styles.containerScrollView]}
					contentContainerStyle={{
						flexDirection: "row",
					}}
				>
					{sortedEventByCurrentMonth()?.length ? (
						<View>
							{sortedEventByCurrentMonth()?.map((item) => {
								return eventItem(item);
							})}
						</View>
					) : (
						<View style={styles.containerNoEvents}>
							<Text
								adjustsFontSizeToFit
								style={{ fontSize: 18, fontWeight: "600" }}
							>
								{translate("No.Event", this.props.Locale)}
							</Text>
						</View>
					)}
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		alignItems: "center",
		justifyContent: "flex-start",
	},
	containerHeader: {
		height: height * 0.15,
		width: width,
		flexDirection: "row",
		alignItems: "center",
		shadowColor: "black",
		shadowOffset: {
			width: 0,
			height: 10,
		},
		shadowOpacity: 0.22,
		shadowRadius: 5.46,
		elevation: 20,
		paddingTop: 20,
		backgroundColor: "#7ABFF1",
	},
	containerScrollView: {
		height: height * 0.9,
	},
	containerHeaderMenu: {
		paddingHorizontal: 20,
		alignItems: "center",
		justifyContent: "center",
		height: height * 0.1,
		width: width,
		backgroundColor: "#B1D8F1",
	},
	textEventTitle: {
		fontSize: 20,
		marginLeft: 10,
		marginVertical: 13,
	},
	textDescEvent: {
		fontSize: 16,
		marginHorizontal: 10,
		marginBottom: 10,
	},
	divider: {
		backgroundColor: "black",
		height: 1,
		width: width,
		shadowColor: "black",
		shadowOffset: {
			width: 0,
			height: 4,
		},
		shadowOpacity: 2,
		shadowRadius: 10,
		elevation: 20,
	},
	containerNoEvents: {
		width: width,
		height: height * 0.77,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "white",
	},
	textHeader: {
		fontSize: 34,
		fontWeight: "800",
		textShadowColor: "black",
		textShadowOffset: { width: 0.1, height: 0.1 },
		textShadowRadius: 0.1,
		shadowOpacity: 0.1,
		color: "white",
	},
	textMonth: {
		fontSize: 30,
		fontWeight: "700",
		color: "black",
		marginBottom: 10,
	},
	imgHeader: {
		width: "100%",
		height: 150,
	},
	imgEvent: {
		height: 150,
		width: width * 0.7,
		marginVertical: 20,
		marginHorizontal: width * 0.08,
	},
	containerAgenda: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: 10,
	},
});

const mapStateToProps = ({ locale }) => {
	const { Locale } = locale;
	return { Locale };
};

export default connect(mapStateToProps)(Agenda);
