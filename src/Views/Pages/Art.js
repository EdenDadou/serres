import React, { Component, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ImageBackground,
  Animated,
  Easing,
  Linking,
  BackHandler,
} from "react-native";
import { ListItem, Icon, Divider, Button } from "react-native-elements";
import { Feather, Entypo } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");

import PictureCarousel from "./Components/PictureCarousel";
import MapCardCarousel from "./Components/MapCardCarousel";

class Art extends Component {
  constructor(props) {
    super(props);
    this.refs = undefined;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      currentMonth: undefined,
      events: undefined,
      toggleEvent: false,
      presenceEvent: false,
      animatedZoom: new Animated.Value(1),
      animatedOpacity: new Animated.Value(1),
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    if (this.state.toggleEvent === false) {
      this.props.navigation.navigate("Menu");
    } else {
      this.setState({ toggleEvent: false });
    }
    return true;
  }

  toggleEvent = (item) => {
    this.refs._scrollView.scrollTo({
      y: 0,
      animated: false,
    });
    if (this.state.toggleEvent === false || this.state.toggleEvent !== item) {
      Animated.parallel([
        Animated.timing(this.state.animatedZoom, {
          toValue: 1.2,
          duration: 30,
          useNativeDriver: true,
        }),
        Animated.timing(this.state.animatedOpacity, {
          toValue: 0.2,
          duration: 30,
          useNativeDriver: true,
        }),
      ]).start(() =>
        this.setState({
          toggleEvent: item,
          animatedZoom: new Animated.Value(1),
          animatedOpacity: new Animated.Value(1),
        })
      );
    } else {
      Animated.parallel([
        Animated.timing(this.state.animatedZoom, {
          toValue: 0.97,
          duration: 30,
          easing: Easing.easeInExpo,
          useNativeDriver: true,
        }),
        Animated.timing(this.state.animatedOpacity, {
          toValue: 0.4,
          duration: 30,
          easing: Easing.easeInExpo,
          useNativeDriver: true,
        }),
      ]).start(() => {
        this.setState({
          toggleEvent: false,
          animatedZoom: new Animated.Value(1),
          animatedOpacity: new Animated.Value(1),
        });
      });
    }
  };

  ArtisantPageItems(arrayPicture, itemName, urlTopPicture, SiteURL) {
    if (itemName === this.state.toggleEvent) {
      return (
        <Animated.View
          style={{
            backgroundColor: "rgba(214, 157, 157,0.5)",
            paddingBottom: 30,
            transform: [{ scale: this.state.animatedZoom }],
            flexGrow: 1,
            justifyContent: "center",
          }}
        >
          <Image
            style={styles.imgPres}
            source={urlTopPicture}
            resizeMode={"cover"}
          />

          <View
            style={{
              width: width,
              padding: width * 0.08,
            }}
          >
            {itemName === "Espace72" && (
              <Image
                style={{
                  height: 69,
                  width: 64,
                  marginHorizontal: width / 2 - 64,
                }}
                source={require("../Assets/Art/Espace72/Espace72Logo.jpg")}
                resizeMode={"cover"}
              />
            )}
            <Text
              adjustsFontSizeToFit
              style={{
                textAlign: "center",
                fontSize: 25,
                fontWeight: "500",
                fontFamily: "Palanquin_700Bold",
                marginBottom: 20,
              }}
            >
              {translate("Art." + itemName, this.props.Locale)}
            </Text>

            <Text
              adjustsFontSizeToFit
              style={{
                fontFamily: "Palanquin_500Medium",
              }}
            >
              {translate("Art." + itemName + ".Pres", this.props.Locale)}
            </Text>
          </View>
          {itemName !== "CEM" && <PictureCarousel data={arrayPicture} />}

          <View
            style={{
              flexDirection: "row",
              marginHorizontal: width * 0.075,
            }}
          >
            <Button
              title={translate("SiteWeb", this.props.Locale)}
              type="solid"
              onPress={() =>
                Linking.openURL(SiteURL).catch((err) =>
                  console.error("Couldn't load page", err)
                )
              }
              buttonStyle={{
                backgroundColor: "rgba(255, 255, 255, 0.75)",
                width: width * 0.4,
                borderRadius: 20,
                marginBottom: 20,
                marginRight: width * 0.05,
              }}
              titleStyle={{ color: "black", fontWeight: "600", fontSize: 16 }}
            />
          </View>
        </Animated.View>
      );
    }
  }

  updateChildState(data) {
    this.setState(data);
  }
  render() {
    var items = ["Map", "CEM", "Espace72", "SaintArey", "OfficeTourisme"];
    var picture = [
      require("../Assets/Art/Map.png"),
      require("../Assets/Art/CEM.jpg"),
      require("../Assets/Art/Espace72.jpg"),
      require("../Assets/Art/SaintArey.png"),
      require("../Assets/Art/OfficeTourisme.jpg"),
    ];

    var PictureEsapce72 = [
      require("../Assets/Art/Espace72/peinture1.jpg"),
      require("../Assets/Art/Espace72/peinture2.jpg"),
      require("../Assets/Art/Espace72/peinture3.jpg"),
    ];
    var PictureSaintArey = [
      require("../Assets/Art/SaintArey/peinture1.png"),
      require("../Assets/Art/SaintArey/peinture2.png"),
      require("../Assets/Art/SaintArey/peinture3.png"),
      require("../Assets/Art/SaintArey/peinture4.png"),
    ];
    var PictureOfficeTourisme = [
      require("../Assets/Art/OfficeTourisme/amilcar.jpg"),
      require("../Assets/Art/OfficeTourisme/office_expo_2.jpg"),
      require("../Assets/Art/OfficeTourisme/office_expo.jpg"),
      require("../Assets/Art/OfficeTourisme/OfficeTourisme.jpg"),
    ];
    var MenuItems = items.map((item, index) => {
      var pictureUrl = picture[index];
      return (
        <ListItem
          underlayColor="transparent"
          onPress={() => this.toggleEvent(item)}
          containerStyle={[styles.containerListItem]}
          key={item}
        >
          <ImageBackground
            source={pictureUrl}
            style={[styles.image, { borderRadius: 40 }]}
            resizeMode={"cover"}
          >
            <Divider style={styles.divider} />

            <ListItem.Content style={{ display: "flex" }}>
              <View style={styles.columnCenter}>
                <View
                  style={{
                    backgroundColor: "#D69D9D",
                    paddingTop: 0,
                    paddingLeft: 10,
                    paddingRight: 10,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomRightRadius: 20,
                    // borderRadius: 20,
                  }}
                >
                  <ListItem.Title style={styles.textTitleListItem}>
                    {translate("Art." + item, this.props.Locale)}
                  </ListItem.Title>
                </View>
                <View style={styles.rowCenter}>
                  <Text adjustsFontSizeToFit style={styles.textDescListItem}>
                    {translate("Art." + item + ".Desc", this.props.Locale)}
                  </Text>
                  {this.state.toggleEvent !== item ? (
                    <Entypo name="chevron-right" size={25} color="black" />
                  ) : (
                    <Entypo name="chevron-up" size={25} color="black" />
                  )}
                </View>
              </View>
            </ListItem.Content>
            <Divider style={styles.divider} />
          </ImageBackground>
        </ListItem>
      );
    });

    var Markers = [
      {
        coordinate: {
          latitude: 44.429,
          longitude: 5.70993,
        },
        title: translate("Art.CEM", this.props.Locale),
        description: translate("Art.CEM.Desc", this.props.Locale),
        image: require("../Assets/Art/CEM.jpg"),
        name: "CEM",
      },
      {
        coordinate: {
          latitude: 44.42862,
          longitude: 5.71491,
        },
        title: translate("Art.Espace72", this.props.Locale),
        description: translate("Art.Espace72.Desc", this.props.Locale),
        image: require("../Assets/Art/Espace72.jpg"),
        name: "Espace72",
      },
      {
        coordinate: {
          latitude: 44.42918,
          longitude: 5.71747,
        },
        title: translate("Art.OfficeTourisme", this.props.Locale),
        description: translate("Art.OfficeTourisme.Desc", this.props.Locale),
        image: require("../Assets/Art/OfficeTourisme.jpg"),
        name: "OfficeTourisme",
      },
    ];

    var Region = {
      latitude: 44.4285,
      longitude: 5.70993,
      latitudeDelta: 0.0008,
      longitudeDelta: 0.004,
    };

    return (
      <Animated.View style={[styles.container]}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Menu")}
            style={{ marginHorizontal: 10 }}
          >
            <Feather
              name="menu"
              size={40}
              color="white"
              style={{
                textShadowColor: "black",
                textShadowOffset: { width: 0.1, height: 0.1 },
                textShadowRadius: 0.1,
              }}
            />
          </TouchableOpacity>

          <Text adjustsFontSizeToFit style={styles.textHeader}>
            {translate("Menu.Arts", this.props.Locale)}
          </Text>

          {this.state.toggleEvent !== false && (
            <TouchableOpacity
              style={styles.iconSidePosition}
              onPress={() => this.toggleEvent(this.state.toggleEvent)}
            >
              <Entypo name="chevron-up" size={40} color="white" />
            </TouchableOpacity>
          )}
        </View>
        <Divider style={styles.dividerHeader} />

        <Animated.ScrollView
          ref="_scrollView"
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "flex-start",
            backgroundColor: "transparent",
          }}
          style={[
            styles.containerScrollView,
            {
              transform: [{ scale: this.state.animatedZoom }],
              opacity: this.state.animatedOpacity,
            },
          ]}
        >
          {this.state.toggleEvent === false && MenuItems}
          {this.state.toggleEvent === "Map" && (
            <View style={styles.container}>
              <MapCardCarousel
                updateParentState={this.updateChildState.bind(this)}
                markers={Markers}
                region={Region}
                color={"#D69D9D"}
              />
            </View>
          )}

          {this.state.toggleEvent === "CEM" &&
            this.ArtisantPageItems(
              undefined,
              "CEM",
              require("../Assets/Art/CEM.jpg"),
              "https://serreslezarts.wixsite.com/accueil/le-cem"
            )}
          {this.state.toggleEvent === "Espace72" &&
            this.ArtisantPageItems(
              PictureEsapce72,
              "Espace72",
              require("../Assets/Art/Espace72.jpg"),
              "https://espace72serres.blogspot.com/"
            )}
          {this.state.toggleEvent === "SaintArey" &&
            this.ArtisantPageItems(
              PictureSaintArey,
              "SaintArey",
              require("../Assets/Art/SaintArey.png")
            )}
          {this.state.toggleEvent === "OfficeTourisme" &&
            this.ArtisantPageItems(
              PictureOfficeTourisme,
              "OfficeTourisme",
              require("../Assets/Art/OfficeTourisme.jpg"),
              "https://www.sisteron-buech.fr/fr/commerce- service/office-tourisme-sisteron-buech-bureau-serres"
            )}
        </Animated.ScrollView>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "rgba(214, 157, 157,0.5)",
  },
  image: {
    resizeMode: "contain",
    width: width,
    height: height * 0.3,
  },
  map: {
    width: width,
    height: height * 0.88,
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    paddingTop: 20,

    backgroundColor: "#D69D9D",
  },
  containerScrollView: {
    flex: 1,
  },
  dividerHeader: {
    backgroundColor: "black",
    height: 0.3,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  divider: {
    backgroundColor: "black",
    height: 0.9,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  iconSidePosition: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 1,
    paddingTop: 20,
  },
  textHeader: {
    fontSize: 34,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: 0.1,
    shadowOpacity: 0.1,
    color: "white",
  },
  imgPres: {
    width: width,
    height: 250,
    marginTop: 0,
    paddingTop: 0,
  },

  containerListItem: {
    backgroundColor: "#D69D9D",
    shadowColor: "black",
    width: width,
    height: height * 0.3,
    // marginTop: 10,
    shadowColor: "black",
    shadowOffset: {
      width: 4,
      height: 10,
    },
    shadowOpacity: 0.12,
    shadowRadius: 5.46,
    elevation: 20,
    padding: 0,
    marginBottom: 8,
  },
  textTitleListItem: {
    fontSize: 20,
    fontFamily: "Palanquin_700Bold",
  },
  textDescListItem: {
    fontSize: 13,
    marginTop: -5,
    // marginBottom:10,
    fontFamily: "Palanquin_500Medium",
  },
  rowCenter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#D69D9D",
    paddingLeft: 10,
    paddingRight: 10,
    // marginBottom: 1,
    width: width,
    opacity: 0.8,
  },
  columnCenter: {
    flexDirection: "column",
    alignItems: "flex-start",
    position: "absolute",
    bottom: 0,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(Art);
