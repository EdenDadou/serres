import { StatusBar } from "expo-status-bar";
import React, { Component, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Animated,
  Easing,
  Linking,
  BackHandler,
} from "react-native";
import { ListItem, Icon, Divider, Button } from "react-native-elements";
import { Feather } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import { Entypo } from "@expo/vector-icons";

import PictureCarousel from "./Components/PictureCarousel";
import MapCardCarousel from "./Components/MapCardCarousel";

class Artisanat extends Component {
  constructor(props) {
    super(props);
    this.refs = undefined;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      currentMonth: undefined,
      events: undefined,
      toggleEvent: false,
      presenceEvent: false,
      animatedZoom: new Animated.Value(1),
      animatedOpacity: new Animated.Value(1),
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    if (this.state.toggleEvent === false) {
      this.props.navigation.navigate("Menu");
      return true;
    } else {
      this.setState({ toggleEvent: false });
      return true;
    }
  }

  toggleEvent = (item) => {
    this.refs._scrollView.scrollTo({
      y: 0,
      animated: false,
    });
    if (this.state.toggleEvent === false || this.state.toggleEvent !== item) {
      Animated.parallel([
        Animated.timing(this.state.animatedZoom, {
          toValue: 1.2,
          duration: 30,
          useNativeDriver: true,
        }),
        Animated.timing(this.state.animatedOpacity, {
          toValue: 0.2,
          duration: 30,
          useNativeDriver: true,
        }),
      ]).start(() =>
        this.setState({
          toggleEvent: item,
          animatedZoom: new Animated.Value(1),
          animatedOpacity: new Animated.Value(1),
        })
      );
    } else {
      Animated.parallel([
        Animated.timing(this.state.animatedZoom, {
          toValue: 0.97,
          duration: 30,
          easing: Easing.easeInExpo,
          useNativeDriver: true,
        }),
        Animated.timing(this.state.animatedOpacity, {
          toValue: 0.4,
          duration: 30,
          easing: Easing.easeInExpo,
          useNativeDriver: true,
        }),
      ]).start(() => {
        this.setState({
          toggleEvent: false,
          animatedZoom: new Animated.Value(1),
          animatedOpacity: new Animated.Value(1),
        });
      });
    }
  };

  ArtisantPageItems(arrayPicture, itemName, urlTopPicture, SiteURL) {
    if (itemName === this.state.toggleEvent) {
      return (
        <Animated.View
          style={{
            backgroundColor: "transparent",
            paddingBottom: 30,
            transform: [{ scale: this.state.animatedZoom }],
            flexGrow: 1,
            justifyContent: "center",
          }}
        >
          <Image
            style={styles.imgPres}
            source={urlTopPicture}
            resizeMode={"cover"}
          />

          <View
            style={{
              width: width,
              padding: width * 0.08,
            }}
          >
            <Text
              adjustsFontSizeToFit
              style={{
                textAlign: "center",
                fontSize: 25,
                fontWeight: "500",
                fontFamily: "Palanquin_700Bold",
                marginBottom: 20,
              }}
              adjustsFontSizeToFit
            >
              {translate("Artisanat." + itemName, this.props.Locale)}
            </Text>

            <Text
              adjustsFontSizeToFit
              style={{
                fontFamily: "Palanquin_500Medium",
              }}
              adjustsFontSizeToFit
            >
              {translate("Artisanat." + itemName + ".Pres", this.props.Locale)}
            </Text>
          </View>
          <PictureCarousel data={arrayPicture} />

          {itemName === "Siaud" && (
            <View
              style={{
                flexDirection: "row",
                marginHorizontal: width * 0.075,
              }}
            >
              <Button
                title="La Tallandier"
                type="solid"
                onPress={() =>
                  Linking.openURL(
                    "https://www.facebook.com/Le- Taillandier-112029320385140"
                  ).catch((err) => console.error("Couldn't load page", err))
                }
                icon={<Entypo name="facebook" size={20} color="black" />}
                buttonStyle={{
                  backgroundColor: "rgba(255, 255, 255, 0.75)",
                  width: width * 0.35,
                  borderRadius: 20,
                  marginBottom: 20,
                  marginRight: width * 0.02,
                }}
                titleStyle={{
                  color: "black",
                  fontWeight: "600",
                  fontSize: 14,
                  marginLeft: 5,
                }}
              />
              <Button
                title="Guitares Papecaster"
                type="solid"
                onPress={() =>
                  Linking.openURL("https://www.facebook.com/Papecaster").catch(
                    (err) => console.error("Couldn't load page", err)
                  )
                }
                icon={<Entypo name="facebook" size={20} color="black" />}
                buttonStyle={{
                  backgroundColor: "rgba(255, 255, 255, 0.75)",
                  width: width * 0.45,
                  borderRadius: 20,
                  marginBottom: 20,
                  marginRight: width * 0.02,
                }}
                titleStyle={{
                  color: "black",
                  fontWeight: "600",
                  fontSize: 14,
                  marginLeft: 5,
                }}
              />
            </View>
          )}
          <View
            style={{
              flexDirection: "row",
              marginHorizontal: width * 0.075,
            }}
          >
            {itemName !== "Judith" && itemName !== "Siaud" && (
              <Button
                title={translate("SiteWeb", this.props.Locale)}
                type="solid"
                onPress={() =>
                  Linking.openURL(SiteURL).catch((err) =>
                    console.error("Couldn't load page", err)
                  )
                }
                buttonStyle={{
                  backgroundColor: "rgba(255, 255, 255, 0.75)",
                  width: width * 0.4,
                  borderRadius: 20,
                  marginBottom: 20,
                  marginRight: width * 0.05,
                }}
                titleStyle={{ color: "black", fontWeight: "600", fontSize: 16 }}
              />
            )}

            {itemName === "Judith" && (
              <Button
                title="La Sauterelle"
                type="solid"
                onPress={() =>
                  Linking.openURL(
                    "https://www.facebook.com/Boutique-La-Sauterelle-%C3%A0-Serres-05700-104705944344529"
                  ).catch((err) => console.error("Couldn't load page", err))
                }
                icon={<Entypo name="facebook" size={20} color="black" />}
                buttonStyle={{
                  backgroundColor: "rgba(255, 255, 255, 0.75)",
                  width: width * 0.4,
                  borderRadius: 20,
                  marginBottom: 20,
                  marginRight: width * 0.02,
                }}
                titleStyle={{
                  color: "black",
                  fontWeight: "600",
                  fontSize: 15,
                  marginLeft: 5,
                }}
              />
            )}

            {itemName !== "Dupuis" && (
              <Button
                title={translate("SeeOnMap", this.props.Locale)}
                type="solid"
                onPress={() => this.setState({ toggleEvent: "Map" })}
                buttonStyle={{
                  backgroundColor: "rgba(255, 255, 255, 0.75)",
                  width: width * 0.4,
                  borderRadius: 20,
                }}
                titleStyle={{ color: "black", fontWeight: "600", fontSize: 15 }}
              />
            )}
          </View>
        </Animated.View>
      );
    }
  }

  updateChildState(data) {
    this.setState(data);
  }
  render() {
    var items = [
      "Map",
      "HangArt",
      "Gagnard",
      "Judith",
      "Siaud",
      "Alcatraz",
      "Dupuis",
    ];
    var picture = [
      require("../Assets/Artisanat/Map.png"),
      require("../Assets/Artisanat/HangArt.png"),
      require("../Assets/Artisanat/Gagnard.png"),
      require("../Assets/Artisanat/Judith.png"),
      require("../Assets/Artisanat/Siaud.jpg"),
      require("../Assets/Artisanat/Alcatraz.png"),
      require("../Assets/Artisanat/Dupuis.png"),
    ];

    var PictureHangart = [
      require("../Assets/Artisanat/HangArt/Bijoux.jpg"),
      require("../Assets/Artisanat/HangArt/Marroquinerie.jpg"),
      require("../Assets/Artisanat/HangArt/Vannerie.jpg"),
      require("../Assets/Artisanat/HangArt/Vannerie2.jpg"),
      require("../Assets/Artisanat/HangArt/Voiture.png"),
      require("../Assets/Artisanat/HangArt/Salle.png"),
    ];
    var PictureGagnard = [
      require("../Assets/Artisanat/Gagnard/FauteuilExpo.jpg"),
      require("../Assets/Artisanat/Gagnard/FauteuilFourrure.jpg"),
      require("../Assets/Artisanat/Gagnard/FauteuilTapisserie.jpg"),
    ];
    var PictureJudith = [
      require("../Assets/Artisanat/Judith/Judith2.png"),
      require("../Assets/Artisanat/Judith/Judith1.png"),
    ];
    var PictureSiaud = [
      require("../Assets/Artisanat/Siaud/Couteau_1.jpg"),
      require("../Assets/Artisanat/Siaud/Couteau_2.jpg"),
      require("../Assets/Artisanat/Siaud/Couteau_3.jpg"),
      require("../Assets/Artisanat/Siaud/Couteau_4.jpg"),
      require("../Assets/Artisanat/Siaud/Couteaux.jpg"),
      require("../Assets/Artisanat/Siaud/Guitard.jpg"),
    ];
    var PictureAlcatraz = [
      require("../Assets/Artisanat/Alcatraz/Anita.jpg"),
      require("../Assets/Artisanat/Alcatraz/luminaire_1.jpg"),
      require("../Assets/Artisanat/Alcatraz/luminaire_2.jpg"),
      require("../Assets/Artisanat/Alcatraz/port_guitare.jpg"),
      require("../Assets/Artisanat/Alcatraz/Table_1.jpg"),
    ];
    var PictureDupuis = [
      require("../Assets/Artisanat/Dupuis/BrodCerf.png"),
      require("../Assets/Artisanat/Dupuis/Broderie.jpg"),
      require("../Assets/Artisanat/Dupuis/BrodEtui.png"),
      require("../Assets/Artisanat/Dupuis/BrodMitaineMarie.png"),
      require("../Assets/Artisanat/Dupuis/BrodServiettes.png"),
      require("../Assets/Artisanat/Dupuis/DessinBrod.jpg"),
    ];

    var MenuItems = items.map((item, index) => {
      var pictureUrl = picture[index];
      return (
        <ListItem
          underlayColor="transparent"
          onPress={() => this.toggleEvent(item)}
          containerStyle={[styles.containerListItem]}
          key={item}
        >
          <ImageBackground
            source={pictureUrl}
            style={[styles.image, { borderRadius: 40 }]}
            resizeMode={"cover"}
          >
            <ListItem.Content style={{ display: "flex" }}>
              <View style={styles.columnCenter}>
                <View
                  style={{
                    backgroundColor: "#ECDC4A",
                    paddingTop: 3,
                    paddingLeft: 10,
                    paddingRight: 12,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomRightRadius: 20,
                  }}
                >
                  <ListItem.Title style={styles.textTitleListItem}>
                    {translate("Artisanat." + item, this.props.Locale)}
                  </ListItem.Title>
                </View>
                <View style={styles.rowCenter}>
                  <Text adjustsFontSizeToFit style={styles.textDescListItem}>
                    {translate(
                      "Artisanat." + item + ".Desc",
                      this.props.Locale
                    )}
                  </Text>
                  {this.state.toggleEvent !== item ? (
                    <Entypo name="chevron-right" size={25} color="black" />
                  ) : (
                    <Entypo name="chevron-up" size={25} color="black" />
                  )}
                </View>
              </View>
            </ListItem.Content>
            <Divider style={styles.divider} />
          </ImageBackground>
        </ListItem>
      );
    });

    var Markers = [
      {
        coordinate: {
          latitude: 44.431313,
          longitude: 5.713425,
        },
        title: translate("Artisanat.HangArt", this.props.Locale),
        description: translate("Artisanat.HangArt.Desc", this.props.Locale),
        image: require("../Assets/Artisanat/HangArt.png"),
        name: "HangArt",
      },
      {
        coordinate: {
          latitude: 44.42862,
          longitude: 5.71491,
        },
        title: translate("Artisanat.Gagnard", this.props.Locale),
        description: translate("Artisanat.Gagnard.Desc", this.props.Locale),
        image: require("../Assets/Artisanat/Gagnard.png"),
        name: "Gagnard",
      },
      {
        coordinate: {
          latitude: 44.429275,
          longitude: 5.716794,
        },
        title: translate("Artisanat.Judith", this.props.Locale),
        description: translate("Artisanat.Judith.Desc", this.props.Locale),
        image: require("../Assets/Artisanat/Judith.png"),
        name: "Judith",
      },
      {
        coordinate: {
          latitude: 44.42847,
          longitude: 5.71536,
        },
        title: translate("Artisanat.Siaud", this.props.Locale),
        description: translate("Artisanat.Siaud.Desc", this.props.Locale),
        image: require("../Assets/Artisanat/Siaud.jpg"),
        name: "Siaud",
      },
      {
        coordinate: {
          latitude: 44.431313,
          longitude: 5.713425,
        },
        title: translate("Artisanat.Alcatraz", this.props.Locale),
        description: translate("Artisanat.Alcatraz.Desc", this.props.Locale),
        image: require("../Assets/Artisanat/Alcatraz.png"),
        name: "Alcatraz",
      },
    ];

    var Region = {
      latitude: 44.4305,
      longitude: 5.7135,
      latitudeDelta: 0.0008,
      longitudeDelta: 0.004,
    };

    return (
      <Animated.View style={[styles.container]}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Menu")}
            style={{ marginHorizontal: 10 }}
          >
            <Feather
              name="menu"
              size={40}
              color="white"
              style={{
                textShadowColor: "black",
                textShadowOffset: { width: 0.3, height: 0.3 },
                textShadowRadius: 2,
              }}
            />
          </TouchableOpacity>

          <Text adjustsFontSizeToFit style={styles.textHeader}>
            {translate("Menu.Artisanat", this.props.Locale)}
          </Text>

          {this.state.toggleEvent !== false && (
            <TouchableOpacity
              style={styles.iconSidePosition}
              onPress={() => this.toggleEvent(this.state.toggleEvent)}
            >
              <Entypo name="chevron-up" size={40} color="white" />
            </TouchableOpacity>
          )}
        </View>
        <Divider style={styles.dividerHeader} />

        <Animated.ScrollView
          ref="_scrollView"
          contentContainerStyle={{
            flexGrow: 1,
            alignItems: "flex-start",
            backgroundColor: "transparent",
          }}
          style={[
            styles.containerScrollView,
            {
              transform: [{ scale: this.state.animatedZoom }],
              opacity: this.state.animatedOpacity,
            },
          ]}
        >
          {this.state.toggleEvent === false && MenuItems}
          {this.state.toggleEvent === "Map" && (
            <View style={styles.container}>
              <MapCardCarousel
                updateParentState={this.updateChildState.bind(this)}
                markers={Markers}
                region={Region}
                color={"#ECDC4A"}
              />
            </View>
          )}
          {this.state.toggleEvent === "HangArt" &&
            this.ArtisantPageItems(
              PictureHangart,
              "HangArt",
              require("../Assets/Artisanat/HangArt/Voiture.png"),
              "https://lehangartdeserres.fr"
            )}
          {this.state.toggleEvent === "Gagnard" &&
            this.ArtisantPageItems(
              PictureGagnard,
              "Gagnard",
              require("../Assets/Artisanat/Gagnard/FauteuilExpo.jpg"),
              "https://espace72serres.blogspot.com/"
            )}
          {this.state.toggleEvent === "Judith" &&
            this.ArtisantPageItems(
              PictureJudith,
              "Judith",
              require("../Assets/Artisanat/Judith.png")
            )}
          {this.state.toggleEvent === "Siaud" &&
            this.ArtisantPageItems(
              PictureSiaud,
              "Siaud",
              require("../Assets/Artisanat/Siaud.jpg")
            )}
          {this.state.toggleEvent === "Alcatraz" &&
            this.ArtisantPageItems(
              PictureAlcatraz,
              "Alcatraz",
              require("../Assets/Artisanat/Alcatraz/luminaire_1.jpg"),
              "https://lemeubleautrement.com/"
            )}
          {this.state.toggleEvent === "Dupuis" &&
            this.ArtisantPageItems(
              PictureDupuis,
              "Dupuis",
              require("../Assets/Artisanat/Dupuis/BrodMitaineMarie.png"),
              "https://www.amdacoudre.com/"
            )}
        </Animated.ScrollView>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(236, 220, 74,0.5)",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  image: {
    resizeMode: "cover",
    width: width,
    height: height * 0.3,
  },
  map: {
    width: width,
    height: height * 0.88,
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    backgroundColor: "#ECDC4A",
    paddingTop: 20,
  },
  containerScrollView: {
    flex: 1,
  },
  dividerHeader: {
    backgroundColor: "black",
    height: 0.6,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  divider: {
    backgroundColor: "black",
    height: 1,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  iconSidePosition: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 1,
    marginTop: 20,
  },
  textHeader: {
    fontSize: 34,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.3, height: 0.3 },
    textShadowRadius: 2,
    shadowOpacity: 0.1,
    color: "white",
  },
  imgPres: {
    width: width,
    height: 250,
    marginTop: 0,
    paddingTop: 0,
  },

  containerListItem: {
    backgroundColor: "#ECDC4A",
    shadowColor: "black",
    width: width,
    height: height * 0.3,
    // marginTop: 10,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.12,
    shadowRadius: 5.46,
    elevation: 20,
    padding: 0,
    marginBottom: 8,
  },
  textTitleListItem: {
    fontSize: 20,
    fontFamily: "Palanquin_700Bold",
  },
  textDescListItem: {
    fontSize: width * 0.035,
    marginTop: -5,
    fontFamily: "Palanquin_500Medium",
  },
  rowCenter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#ECDC4A",
    paddingLeft: 10,
    paddingRight: 10,
    width: width,
    opacity: 0.8,
  },
  columnCenter: {
    flexDirection: "column",
    alignItems: "flex-start",
    position: "absolute",
    bottom: 0,
    // marginTop: height * 0.205,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(Artisanat);
