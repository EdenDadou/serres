import * as React from "react";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { Divider } from "react-native-elements";
const { width, height } = Dimensions.get("screen");
import { translate } from "../../../Langues/locale";
import { Entypo, AntDesign } from "@expo/vector-icons";
import { ChangeLocation } from "../../../Redux/location/actions";
import { ScrollView } from "react-native";
import AudioSlider from "./utils/AudioSlider";

class EventParcour extends React.Component {
  constructor(props) {
    super(props);
    this.watchID = null;
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  updateChildState(data) {
    this.setState(data);
  }

  render() {
    var {
      eventTitle,
      eventDescription,
      eventImage,
      eventSound,
      event,
      showAudioDescription,
      color,
    } = this.props;
    var colorToOpacity = "rgba(" + color.split("(")[1].split(")")[0] + ",0.8)";
    return (
      <View
        style={[
          eventImage !== undefined
            ? {
                height:
                  Platform.OS === "ios"
                    ? height * 0.85
                    : height * 0.85 - StatusBar.currentHeight,
                justifyContent: "flex-start",
              }
            : { height: height * 0.12, width: width * 0.8 },
          {
            zIndex: 10,
            backgroundColor: colorToOpacity,
            borderRadius: eventImage !== undefined ? null : 20,
            opacity: eventImage !== undefined ? null : 0.9,
          },
        ]}
      >
        {/* //**** ENTETE ******/}
        <View
          style={{
            height:
              eventImage !== undefined
                ? Platform.OS === "ios"
                  ? height * 0.15
                  : height * 0.1
                : height * 0.12,
            borderRadius: 20,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: this.props.color,
            paddingHorizontal: 40,
          }}
        >
          {eventTitle.split(".")[2] !== "0" && (
            <Text
              adjustsFontSizeToFit
              style={{
                color: "white",
                fontSize: 17,
                fontWeight: "700",
                textAlign: "center",
              }}
            >
              {translate("Parcour.Step", this.props.Locale)}{" "}
              {translate(eventTitle, this.props.Locale).split(".")[0]}/
              {this.props.parcourLength - 1}
            </Text>
          )}
          <Text
            adjustsFontSizeToFit
            style={{
              color: "white",
              fontSize: 15,
              fontWeight: "500",
              textAlign: "center",
            }}
          >
            {translate(eventTitle, this.props.Locale)}
          </Text>
          <Entypo
            name="cross"
            size={19}
            style={{ position: "absolute", right: 20 }}
            color="white"
            onPress={() =>
              this.updateParentState({
                event: false,
                blockEvent: false,
                eventTitle: undefined,
                eventDescription: undefined,
                eventImage: undefined,
                eventSound: undefined,
              })
            }
          />
        </View>

        {/* //***** IMAGE ***** */}
        {eventImage !== undefined && (
          <Image
            source={eventImage}
            style={[
              showAudioDescription === true
                ? { width: width, height: height * 0.25 }
                : { width: width, height: height * 0.51 },
              { marginVertical: 15 },
            ]}
            resizeMode="contain"
          />
        )}
        {eventSound !== undefined && (
          <View
            style={
              showAudioDescription === true
                ? { height: height * 0.4 }
                : { height: height * 0.2 }
            }
          >
            <AudioSlider
              backgroundColor="#4C9C4B"
              soundObject={eventSound}
              event={event}
              height={height * 0.07}
              backgroundColor={color}
            />
            <Divider style={styles.divider} />

            {/* // ********** AUDIO DESCRIPTION ********* */}
            {showAudioDescription === true && (
              <ScrollView
                style={{
                  height: height * 0.23,
                  paddingHorizontal: 16,
                  paddingVertical: 20,
                  backgroundColor: this.props.color,
                }}
              >
                <Text
                  adjustsFontSizeToFit
                  style={{
                    color: "white",
                    fontSize: 14,
                    fontWeight: "600",
                    textAlign: "justify",
                    marginBottom: 40,
                  }}
                >
                  {translate(eventDescription, this.props.Locale)}
                </Text>
              </ScrollView>
            )}
            <Divider style={styles.divider} />

            <TouchableOpacity
              style={[
                {
                  height: height * 0.07,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: this.props.color,
                },
              ]}
              onPress={() =>
                this.updateParentState({
                  showAudioDescription: !showAudioDescription,
                })
              }
              activeOpacity={0.8}
            >
              <Text
                adjustsFontSizeToFit
                style={{ color: "white", fontSize: 15, fontWeight: "600" }}
              >
                {showAudioDescription === false
                  ? translate("Show.AudioDesciption", this.props.Locale)
                  : translate("Hide.AudioDesciption", this.props.Locale)}
              </Text>
              <AntDesign
                name={showAudioDescription === true ? "down" : "up"}
                size={20}
                style={{ position: "absolute", right: 20 }}
                color="white"
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  divider: {
    backgroundColor: "black",
    height: 0.5,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowRadius: 3,
    elevation: 30,
  },
});

const mapStateToProps = ({ locale, location }) => {
  const { Locale } = locale;
  const { currentUserLocation } = location;
  return { Locale, currentUserLocation };
};

export default connect(mapStateToProps, { ChangeLocation })(EventParcour);
