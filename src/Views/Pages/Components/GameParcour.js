import * as React from "react";
import { Audio } from "expo-av";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Vibration,
  Image,
  KeyboardAvoidingView,
} from "react-native";
import { TouchableOpacity, TextInput } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { Divider } from "react-native-elements";
const { width, height } = Dimensions.get("window");
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from "react-native-maps";
import { translate } from "../../../Langues/locale";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import { ChangeLocation } from "../../../Redux/location/actions";
import { Entypo } from "@expo/vector-icons";
import GameResult from "./GameResult";
import { Platform } from "react-native";
import { setStep, setParcourName } from "../../../Redux/parcours/actions";

class GameParcour extends React.Component {
  constructor(props) {
    super(props);
    this.watchID = null;
    this._mapView = null;
    this.location = null;
    this.state = {
      event: false,
      response: undefined,
      gameResult: false,
      checkResponse: false,
      textResponse: undefined,
      iconResponse: undefined,
      colorResponse: undefined,
      tryRestant: undefined,
      essai: 1,
    };
    if (this.props.difficulty !== undefined) {
      this.state = { ...this.state, parcoursName: this.props.difficulty };
    }
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);

    if (status === "granted") {
      this._getLocationAsync();
    } else {
      this.setState({ error: "Locations services needed" });
    }

    console.log("props parcourname", this.props.parcoursName);
    if (this.state.parcoursName === this.props.parcoursName) {
      this.props.setStep(this.props.parcoursEvent.step);
      this.updateParentState({ step: this.props.parcoursEvent.step });
    } else {
      this.props.setStep(0);
      this.props.setParcourName(this.state.parcoursName);
    }
  }

  componentWillUnmount() {
    if (this.location !== null) {
      this.location.remove();
    }
  }

  async _getLocationAsync() {
    //******* Location Precision *******/
    var options = {
      accuracy: 6,
      timeInterval: 3000,
      distanceInterval: 5,
    };

    var onNewLocation = (newLocation) => {
      let currentPosition = {
        latitude: newLocation.coords.latitude,
        longitude: newLocation.coords.longitude,
      };
      this.checkUserDistanceToMarker(currentPosition);
    };

    this.location = await Location.watchPositionAsync(
      options,
      onNewLocation,
      (error) => console.log(error)
    );
    return this.location;
  }

  async checkUserDistanceToMarker(currentPosition) {
    var { event } = this.state;
    var { enigme, step } = this.props;
    var marker = {
      latitude: enigme[step - 1].latitude,
      longitude: enigme[step - 1].longitude,
    };
    var distanceToMarker = this.measure(
      currentPosition.latitude,
      currentPosition.longitude,
      marker.latitude,
      marker.longitude
    );
    //**** on Approch ****/
    if (event === false && distanceToMarker < 15) {
      this.playSound(require("../../Assets/EscapeGame/audio/event_ting.mp3"));
      Vibration.vibrate();
      this.setState({ event: true });
    }

    //**** on Leave ****/
    if (event === true && distanceToMarker > 15) {
      Vibration.vibrate();
      this.setState({
        event: false,
      });
    }
  }
  //********** MATH FUNCTION *********/
  measure(lat1, lon1, lat2, lon2) {
    var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 * Math.PI) / 180 - (lat1 * Math.PI) / 180;
    var dLon = (lon2 * Math.PI) / 180 - (lon1 * Math.PI) / 180;
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000; // meters
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  updateChildState(data) {
    this.setState(data);
  }

  passQuestion = (step) => {
    if (step + 1 <= 10) {
      this.props.setStep(step + 1);
      this.updateParentState({ step: step + 1 });
      this.setState({ response: undefined, event: false, essai: 1 });
    } else {
      this.setState({ gameResult: true });
    }
  };

  async playSound(audio) {
    try {
      const { sound: soundObject, status } = await Audio.Sound.createAsync(
        audio,
        { shouldPlay: true }
      );
      // Your sound is playing!
    } catch (error) {
      // An error occurred!
    }
  }

  valideResponse = (step) => {
    var { enigme, step, goodAnswer, difficulty } = this.props;
    var { response, essai } = this.state;
    var answer = translate(enigme[step - 1].resp, this.props.Locale).split("/");
    this.setState({ essai: essai + 1 });

    if (step + 1 <= 10) {
      if (
        response !== undefined &&
        (response.toUpperCase() === answer[0] ||
          response.toUpperCase() === answer[1])
      ) {
        this.playSound(
          require("../../Assets/EscapeGame/audio/bonne_reponse.mp3")
        );
        this.props.setStep(step + 1);
        this.updateParentState({ step: step + 1, goodAnswer: goodAnswer + 1 });
        this.setState({
          checkResponse: true,
          textResponse: "EscapeGame.Resp.Good",
          iconResponse: "check",
          colorResponse: "green",
          essai: 1,
        });
        setTimeout(() => {
          this.setState({
            checkResponse: false,
            response: undefined,
            event: false,
          });
        }, 4000);
      } else if (essai < 3) {
        if (
          (difficulty === 0 && step === 9) ||
          (difficulty === 1 && step === 8)
        ) {
          this.props.setStep(step + 1);
          this.updateParentState({ step: step + 1 });
          this.setState({
            checkResponse: true,
            textResponse: "EscapeGame.Resp.Bad",
            iconResponse: "cross",
            colorResponse: "red",
            essai: 1,
          });
          setTimeout(() => {
            this.setState({
              checkResponse: false,
              response: undefined,
              event: false,
            });
          }, 4000);
        }
        this.playSound(
          require("../../Assets/EscapeGame/audio/mauvaise_reponse.mp3")
        );
        this.setState({
          checkResponse: true,
          textResponse: "EscapeGame.Resp.Bad",
          tryRestant: "EscapeGame.Resp.Try",
          iconResponse: "cross",
          colorResponse: "red",
        });
        setTimeout(() => {
          this.setState({
            checkResponse: false,
            response: undefined,
          });
        }, 4000);
      } else {
        this.props.setStep(step + 1);
        this.updateParentState({ step: step + 1 });
        this.setState({
          checkResponse: true,
          textResponse: "EscapeGame.Resp.Bad",
          iconResponse: "cross",
          colorResponse: "red",
        });
        setTimeout(() => {
          this.setState({
            checkResponse: false,
            response: undefined,
            event: false,
            essai: 1,
          });
        }, 4000);
      }
    } else {
      this.setState({ gameResult: true });
    }
  };

  render() {
    console.log("parcoursEvent.step", this.props.parcoursEvent);
    var { enigme, step, difficulty } = this.props;
    var { event, gameResult, checkResponse } = this.state;

    var MarkerEnigme = (
      <MapView.Marker
        coordinate={{
          latitude: enigme[step - 1].latitude,
          longitude: enigme[step - 1].longitude,
        }}
      />
    );

    return (
      <View style={styles.container}>
        {gameResult === false && (
          <MapView
            ref={(map) => (this._mapView = map)}
            initialRegion={this.props.region}
            style={[
              styles.map,
              event === false
                ? {
                    height: height - height * 0.12 - 50,
                  }
                : checkResponse === false &&
                  gameResult === false &&
                  event === true
                ? {
                    height: height * 0.5,
                  }
                : event === true && {
                    height: height - height * 0.12,
                  },
            ]}
            showsUserLocation={true}
            showsMyLocationButton={true}
            followsUserLocation={true}
          >
            {MarkerEnigme}
          </MapView>
        )}
        {event === false ? (
          <View
            style={{
              backgroundColor: "#D83B3B",
              height: 50,
              width: width,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              bottom: 0,
            }}
          >
            <Text adjustsFontSizeToFit style={{ color: "white", fontSize: 14 }}>
              {translate("EscapeGame.Instruction", this.props.Locale)}
              {translate(
                "EscapeGame.Level" + difficulty + "." + step,
                this.props.Locale
              )}
            </Text>
          </View>
        ) : checkResponse === true ? (
          <View
            style={{
              flex: 1,
              backgroundColor: "rgba(255,255,255,0.8)",
              justifyContent: "space-between",
              alignItems: "center",
              position: "absolute",
              bottom: 0,
              paddingVertical: 20,
              padding: 24,
              width: width,
              height: 150,
            }}
          >
            <Entypo
              name={this.state.iconResponse}
              size={60}
              color={this.state.colorResponse}
            />
            <Text adjustsFontSizeToFit>
              {translate(this.state.textResponse, this.props.Locale)}
            </Text>
            {this.state.textResponse === "EscapeGame.Resp.Bad" && (
              <Text style={{ flexDirection: "row" }} adjustsFontSizeToFit>
                {4 -
                  this.state.essai +
                  translate(this.state.tryRestant, this.props.Locale)}
              </Text>
            )}
          </View>
        ) : checkResponse === false &&
          gameResult === false &&
          event === true ? (
          <KeyboardAvoidingView
            behavior={"position"}
            style={styles.container}
            keyboardVerticalOffset={Platform.OS === "ios" ? 60 : -height * 0.5}
            contentContainerStyle={[
              {
                flex: 1,
                backgroundColor: "rgba(255,255,255,0.8)",
                justifyContent: "space-between",
                alignItems: "center",
                position: "absolute",
                bottom: 0,
                paddingVertical: 40,
                paddingHorizontal: 20,
              },
            ]}
          >
            <View
              style={{
                flex: 1,
                marginBottom: 20,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                width: "80%",
              }}
            >
              <Image
                style={{ width: 40, height: 45 }}
                source={require("../../Assets/EscapeGame/image/818px_Blason_Serres.png")}
                resizeMode={"stretch"}
              />
              <Text
                adjustsFontSizeToFit
                style={{ fontSize: 20, fontWeight: "700" }}
              >
                {translate(
                  "EscapeGame.Level" + difficulty + "." + step,
                  this.props.Locale
                )}
              </Text>
              <Image
                style={{ width: 40, height: 45 }}
                source={require("../../Assets/EscapeGame/image/Blason_dpt_Hautes_Alpes_.png")}
                resizeMode={"stretch"}
              />
            </View>
            {enigme[step - 1].image !== undefined &&
              enigme[step - 1].image.length === undefined && (
                <Image
                  style={{
                    width: width,
                    height: step !== 4 ? 70 : 140,
                    marginHorizontal: -20,
                    marginBottom: 15,
                  }}
                  source={enigme[step - 1].image}
                  resizeMode={step !== 4 ? "cover" : "contain"}
                />
              )}

            {enigme[step - 1].image !== undefined &&
              enigme[step - 1].image.length !== undefined && (
                <View
                  style={{
                    flexDirection: "row",
                    width: "80%",
                    justifyContent: "space-between",
                    marginBottom: 25,
                  }}
                >
                  {enigme[step - 1].image.map((item, index) => {
                    return (
                      <View key={index}>
                        {step === 9 && (
                          <Text adjustsFontSizeToFit>{index + 1}.</Text>
                        )}
                        <Image
                          style={{
                            width:
                              (width * 0.5) / enigme[step - 1].image.length,
                            height: 70,
                          }}
                          source={item}
                          resizeMode={"cover"}
                        />
                      </View>
                    );
                  })}
                </View>
              )}
            <Text
              adjustsFontSizeToFit
              style={{ fontSize: 17, marginBottom: 10, textAlign: "center" }}
            >
              {translate(enigme[step - 1].desc, this.props.Locale)}
            </Text>
            {enigme[step - 1].desc2 !== undefined && (
              <Text adjustsFontSizeToFit style={{ fontSize: 15 }}>
                {translate(enigme[step - 1].desc2, this.props.Locale)}
              </Text>
            )}
            <View style={{ flexDirection: "row", marginTop: 30 }}>
              <TextInput
                style={{
                  fontSize: 15,
                  paddingLeft: 15,
                  backgroundColor: "rgba(0,0,0,0.22)",
                  height: 40,
                  width: width * 0.7,
                  borderRadius: 20,
                }}
                onChangeText={(response) =>
                  this.setState({ response: response })
                }
                value={this.state.response}
                autoCompleteType={"off"}
                autoCorrect={false}
              />
              <TouchableOpacity
                style={{
                  marginLeft: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "green",
                  width: width * 0.15,
                  height: 40,
                  borderRadius: 20,
                }}
                onPressIn={() => this.valideResponse()}
              >
                <Entypo name="check" size={24} color="white" />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                marginLeft: 10,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(0,0,0,0.22)",
                width: width * 0.2,
                height: 40,
                borderRadius: 20,
                marginTop: 10,
              }}
              onPressIn={() => this.passQuestion(step)}
            >
              <Text adjustsFontSizeToFit style={{ fontWeight: "600" }}>
                {translate("EscapeGame.Pass", this.props.Locale)}
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        ) : (
          <GameResult result={this.props.goodAnswer} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: width,
    marginTop: 0,
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
});

const mapStateToProps = ({ locale, location, parcours }) => {
  const { Locale } = locale;
  const { parcoursEvent, parcoursName } = parcours;
  const { currentUserLocation } = location;
  return { Locale, currentUserLocation, parcoursEvent, parcoursName };
};

export default connect(mapStateToProps, {
  ChangeLocation,
  setParcourName,
  setStep,
})(GameParcour);
