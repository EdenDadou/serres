import * as React from "react";
import { Audio } from "expo-av";
import { Text, View, Dimensions, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import { translate } from "../../../Langues/locale";
import { ChangeLocation } from "../../../Redux/location/actions";
import { Score } from "../../Assets/EscapeGame/Score";

class GameResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = { score: undefined };
    if (this.props.result < 4) {
      this.state = { score: Score[0] };
    } else if (this.props.result < 7) {
      this.state = { score: Score[1] };
    } else if (this.props.result < 9) {
      this.state = { score: Score[2] };
    } else {
      this.state = { score: Score[3] };
    }
  }

  componentDidMount() {
    this.playSound(
      require("../../Assets/EscapeGame/audio/applaudissements.mp3")
    );
  }

  async playSound(audio) {
    try {
      const { sound: soundObject, status } = await Audio.Sound.createAsync(
        audio,
        { shouldPlay: true }
      );
      // Your sound is playing!
    } catch (error) {
      // An error occurred!
    }
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  updateChildState(data) {
    this.setState(data);
  }

  render() {
    var { score } = this.state;
    // console.log(score);
    // var score;
    // if (result < 4) {
    //   score = Score[0];
    // } else if (result < 7) {
    //   score = Score[1];
    // } else if (result < 9) {
    //   score = Score[2];
    // } else {
    //   score = Score[3];
    // }
    // console.log(score.title);
    // console.log(score.image);

    return (
      <View style={styles.container}>
        <Image
          style={{
            width: 110,
            height: 165,
            borderRadius: 50,
            borderWidth: 2,
            borderColor: "white",
          }}
          source={score.image}
          // resizeMode={"cover"}
        />
        <Text
          adjustsFontSizeToFit
          style={{ fontSize: 30, fontWeight: "700", color: "white" }}
        >
          {translate(score.score, this.props.Locale)}
        </Text>
        <Text
          adjustsFontSizeToFit
          style={{ fontSize: 20, fontWeight: "800", color: "white" }}
        >
          {translate(score.title, this.props.Locale)}
        </Text>
        <Text
          adjustsFontSizeToFit
          style={{ fontSize: 24, fontWeight: "700", color: "white" }}
        >
          {translate(score.promo, this.props.Locale)}
        </Text>
        <Text
          adjustsFontSizeToFit
          style={{
            fontSize: 14,
            fontWeight: "400",
            color: "white",
            textAlign: "justify",
          }}
        >
          {translate(score.desc, this.props.Locale)}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#D83B3B",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = ({ locale, location }) => {
  const { Locale } = locale;
  const { currentUserLocation } = location;
  return { Locale, currentUserLocation };
};

export default connect(mapStateToProps, { ChangeLocation })(GameResult);
