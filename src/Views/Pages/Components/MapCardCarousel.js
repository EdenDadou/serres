import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Image,
  Animated,
  ScrollView,
  TouchableOpacity,
  Platform,
} from "react-native";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from "react-native-maps";

const CARD_HEIGHT = Math.round(height / 3);
const CARD_WIDTH = Math.round(width - 50);

class MapCardCarousel extends React.Component {
  constructor(props) {
    super(props);
    this._mapView = null;
    this.state = {
      markers: undefined,
      region: undefined,
      color: "white",
      index: 0,
    };
    if (this.props.markers !== undefined && this.props.region !== undefined) {
      this.state = {
        ...this.state,
        markers: this.props.markers,
        region: this.props.region,
        color: this.props.color,
      };
    }
  }

  UNSAFE_componentWillMount() {
    this.index = 0;
    this.animation = new Animated.Value(0);
  }

  componentDidMount() {
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here
    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          const { coordinate } = this.state.markers[index];
          this.setState({ index: index });
          this.map.animateToRegion(
            {
              latitude: coordinate.latitude - 0.0005,
              longitude: coordinate.longitude,
              latitudeDelta: this.state.region.latitudeDelta,
              longitudeDelta: this.state.region.longitudeDelta,
            },
            350
          );
        }
      }, 10);
    });
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  render() {
    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH + 50,
        index * CARD_WIDTH + 50,
        (index + 1) * CARD_WIDTH + 50,
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: "clamp",
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: "clamp",
      });
      return { scale, opacity };
    });

    return (
      <View style={styles.container}>
        <MapView
          ref={(map) => (this.map = map)}
          initialRegion={this.state.region}
          style={[styles.container, styles.map]}
          showsUserLocation={true}
        >
          {this.state.markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            };
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            };
            return (
              <MapView.Marker
                key={index}
                coordinate={marker.coordinate}
                pinColor={this.props.color}
                opacity={this.state.index === index ? 1 : 0.2}
              >
                {Platform.OS === "ios" && (
                  <Animated.View style={[styles.markerWrap, opacityStyle]}>
                    <Animated.View
                      style={[
                        styles.ring,
                        scaleStyle,
                        {
                          borderColor: this.state.color,
                          backgroundColor: this.state.color,
                          opacity: 0.6,
                        },
                      ]}
                    />
                    <View
                      style={[
                        styles.marker,
                        { backgroundColor: this.state.color },
                      ]}
                    />
                  </Animated.View>
                )}
              </MapView.Marker>
            );
          })}
        </MapView>
        <Animated.ScrollView
          horizontal
          scrollEventThrottle={20}
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          snapToInterval={width}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
        >
          {this.state.markers.map((marker, index) => (
            <TouchableOpacity
              style={[
                styles.card,
                {
                  backgroundColor: this.state.color,
                },
              ]}
              key={index}
              onPress={() => {
                console.log(marker.name),
                  this.updateParentState({ toggleEvent: marker.name });
              }}
            >
              <Image
                source={marker.image}
                style={styles.cardImage}
                resizeMode="cover"
              />
              <View style={styles.textContent}>
                <Text
                  adjustsFontSizeToFit
                  numberOfLines={1}
                  style={styles.cardtitle}
                >
                  {marker.title}
                </Text>
                <Text
                  adjustsFontSizeToFit
                  numberOfLines={1}
                  style={styles.cardDescription}
                >
                  {marker.description}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </Animated.ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: width,
    height: height * 0.88,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 2,
    elevation: 4,
    // backgroundColor: this.state.color,
    marginHorizontal: 25,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  textContent: {
    flex: 1,
    paddingHorizontal: 10,
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    overflow: "hidden",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    position: "absolute",
    borderWidth: 1,
    overflow: "hidden",
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(MapCardCarousel);
