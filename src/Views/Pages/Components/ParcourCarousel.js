import React from "react";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Image,
  Animated,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { Feather, FontAwesome5, Entypo } from "@expo/vector-icons";
import { Button } from "react-native-elements";

const { width, height } = Dimensions.get("window");
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import { translate } from "../../../Langues/locale";

const CARD_HEIGHT = Math.round(height / 2.5);
const CARD_WIDTH = Math.round(width - 50);

import PictureCarousel from "./PictureCarousel";
const FIXED_BAR_WIDTH = width;
const BAR_SPACE = 10;

class ParcourCarousel extends React.Component {
  constructor(props) {
    super(props);
    this._mapView = null;
    this.index = 0;
    this.numItems = this.props.parcours.length;
    this.itemWidth = FIXED_BAR_WIDTH / this.numItems - 10;
    this.animation = new Animated.Value(0);
    this.state = {
      parcours: undefined,
      region: undefined,
      color: "white",
      scaleAnimation: new Animated.ValueXY({ x: 0, y: 0 }),
      animatedSettingHeight: new Animated.Value(CARD_HEIGHT),
      parcourIndex: 0,
      zoomed: false,
      disableZoom: false,
    };
    if (this.props.parcours !== undefined && this.props.region !== undefined) {
      this.state = {
        ...this.state,
        parcours: this.props.parcours,
        region: this.props.region,
        color: this.props.color,
      };
    }
  }

  componentDidMount() {
    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH);
      if (index >= this.state.parcours.length) {
        index = this.state.parcours.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }
      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          const region = this.state.parcours[index].region;
          this._mapView.animateToRegion(region, 350);
          this.setState({
            parcourIndex: index,
          });
        }
      }, 10);
    });
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  showMoreParcourInfo = () => {
    if (this.state.disableZoom === false) {
      if (this.state.zoomed === false) {
        this.setState({ zoomed: true });
        Animated.parallel([
          Animated.spring(this.state.scaleAnimation, {
            useNativeDriver: true,
            toValue: { x: 0, y: -10 },
            velocity: 7,
            friction: 8,
          }),
          Animated.timing(this.state.animatedSettingHeight, {
            useNativeDriver: false,
            toValue: height * 0.75,
            duration: 700,
          }),
        ]).start();
      } else {
        this.setState({ zoomed: false });
        Animated.parallel([
          Animated.spring(this.state.scaleAnimation, {
            useNativeDriver: true,
            toValue: { x: 0, y: 0 },
            velocity: 7,
            friction: 8,
          }),
          Animated.timing(this.state.animatedSettingHeight, {
            useNativeDriver: false,
            toValue: CARD_HEIGHT,
            duration: 500,
          }),
        ]).start();
      }
    }
  };

  startRando = (index) => {
    this.updateParentState({ SelectedParcour: index });
  };

  convertPolyline(arrayOfPoint) {
    let latLngArray = [];
    for (let i = 0; i < arrayOfPoint.length; i++) {
      const gData = {
        latitude: arrayOfPoint[i][1],
        longitude: arrayOfPoint[i][0],
      };
      latLngArray.push(gData);
    }
    return latLngArray;
  }

  render() {
    let barArray = [];
    var ParcourMarker =
      this.state.parcours[this.state.parcourIndex].markers !== undefined &&
      this.state.parcours[this.state.parcourIndex].markers.map(
        (item, index) => {
          return (
            <Marker key={index} coordinate={item}>
              <View
                style={[
                  styles.ring,
                  styles.markerWrap,
                  {
                    borderColor: this.state.color,
                    backgroundColor: this.state.color,
                  },
                ]}
              >
                <Text
                  adjustsFontSizeToFit
                  adjustsFontSizeToFit
                  style={{ fontSize: 12, fontWeight: "500", color: "white" }}
                >
                  {index + 1}
                </Text>
              </View>
            </Marker>
          );
        }
      );

    var ParcourMarkerStartEnd =
      this.state.parcours[this.state.parcourIndex].startEndMarker !==
        undefined &&
      this.state.parcours[this.state.parcourIndex].startEndMarker.map(
        (markers, index) => {
          return (
            <MapView.Marker key={index} coordinate={markers}>
              <View
                style={[
                  styles.ring,
                  styles.markerWrap,
                  this.props.mode === "NatureLoisir"
                    ? {
                        borderColor: index === 0 ? "#5F7EFA" : "#58B557",
                        backgroundColor: index === 0 ? "#5F7EFA" : "#58B557",
                      }
                    : {
                        borderColor: "#5F7EFA",
                        backgroundColor: "#5F7EFA",
                      },
                ]}
              />
            </MapView.Marker>
          );
        }
      );

    var ParcourPolyline = this.state.parcours.map((parcour, index) => {
      if (parcour.polyline !== undefined) {
        var convertedPolyline = this.convertPolyline(parcour.polyline);

        return (
          <MapView.Polyline
            key={index}
            coordinates={convertedPolyline}
            strokeWidth={5}
            strokeColor={
              index === this.state.parcourIndex
                ? this.props.mode === "NatureLoisir"
                  ? "rgba(88, 181, 87, 0.8)"
                  : "rgba(128, 148, 251, 0.8)"
                : this.props.mode === "NatureLoisir"
                ? "rgba(88, 181, 87, 0.2)"
                : "rgba(128, 148, 251, 0.2)"
            }
          />
        );
      }
    });

    return (
      <View style={styles.container}>
        <MapView
          ref={(map) => (this._mapView = map)}
          initialRegion={this.state.region}
          style={[styles.container, styles.map]}
          provider={"google"}
        >
          {ParcourMarkerStartEnd}
          {ParcourMarker}
          {ParcourPolyline}
        </MapView>

        <Animated.ScrollView
          horizontal
          scrollEventThrottle={20}
          decelerationRate="fast"
          snapToInterval={width + 5}
          scrollEnabled={!this.state.zoomed}
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={[styles.scrollView]}
          contentContainerStyle={{
            alignItems: "flex-end",
          }}
          onMomentumScrollBegin={() => this.setState({ disableZoom: true })}
          onMomentumScrollEnd={() => this.setState({ disableZoom: false })}
        >
          {this.state.parcours.map((parcour, index) => {
            const scrollBarVal = this.animation.interpolate({
              inputRange: [width * (index - 1), width * (index + 1)],
              outputRange: [-this.itemWidth, this.itemWidth],
              extrapolate: "clamp",
            });

            const thisBar = (
              <View
                key={`bar${index}`}
                style={[
                  styles.track,
                  {
                    width: this.itemWidth,
                    marginHorizontal: 5,
                  },
                ]}
              >
                <Animated.View
                  style={[
                    styles.bar,
                    {
                      width: this.itemWidth,
                      transform: [{ translateX: scrollBarVal }],
                      backgroundColor: this.props.color,
                    },
                  ]}
                />
              </View>
            );
            barArray.push(thisBar);

            return (
              <TouchableOpacity
                key={index}
                onPress={this.showMoreParcourInfo}
                disabled={this.state.zoomed}
              >
                <Animated.View
                  style={[
                    styles.card,
                    {
                      height: this.state.animatedSettingHeight,
                      borderColor: this.state.color,
                      backgroundColor: this.state.color,
                    },
                  ]}
                >
                  {this.state.zoomed === false && (
                    <Image
                      source={parcour.image}
                      style={styles.cardImage}
                      resizeMode="cover"
                    />
                  )}
                  {this.state.zoomed === false ? (
                    <View style={styles.textContent}>
                      <Text
                        adjustsFontSizeToFit
                        numberOfLines={1}
                        style={styles.cardtitle}
                        adjustsFontSizeToFit
                      >
                        {translate(parcour.title, this.props.Locale)}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          marginBottom: 2,
                          width: CARD_WIDTH,
                          paddingHorizontal: 20,
                        }}
                      >
                        {parcour.time !== undefined && (
                          <View style={{ flexDirection: "row" }}>
                            <Feather name="clock" size={16} color="black" />
                            <Text
                              adjustsFontSizeToFit
                              numberOfLines={1}
                              style={styles.cardTimeDifficulte}
                            >
                              {parcour.time}
                            </Text>
                          </View>
                        )}
                        {parcour.distance !== undefined && (
                          <View style={{ flexDirection: "row" }}>
                            <FontAwesome5
                              name="walking"
                              size={16}
                              color="black"
                            />

                            <Text
                              adjustsFontSizeToFit
                              numberOfLines={1}
                              style={styles.cardTimeDifficulte}
                            >
                              {parcour.distance}
                            </Text>
                          </View>
                        )}
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                        }}
                      >
                        {parcour.difficulty !== undefined && (
                          <Text
                            adjustsFontSizeToFit
                            adjustsFontSizeToFit
                            numberOfLines={1}
                            style={styles.cardTimeDifficulte}
                          >
                            {translate("Difficulty", this.props.Locale)} :{" "}
                            {translate(parcour.difficulty, this.props.Locale)}
                          </Text>
                        )}
                      </View>
                    </View>
                  ) : parcour.title === "CulturePatrimoine.Illustre" ? (
                    <Animated.View
                      onPress={this.showMoreParcourInfo}
                      key={index}
                      style={[
                        styles.textContent,
                        {
                          borderColor: this.state.color,
                          backgroundColor: this.state.color,
                        },
                      ]}
                    >
                      {/* <ScrollView style={styles.textContent}> */}
                      <TouchableOpacity
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                        }}
                        onPress={this.showMoreParcourInfo}
                      >
                        <Text
                          adjustsFontSizeToFit
                          numberOfLines={1}
                          style={styles.cardtitleZoomed}
                          adjustsFontSizeToFit
                        >
                          {translate(parcour.title, this.props.Locale)}
                        </Text>
                        <Entypo
                          name="chevron-down"
                          size={25}
                          color="black"
                          style={{ marginTop: 10 }}
                        />
                      </TouchableOpacity>

                      <ScrollView
                        ref={(node) => (this.scroll = node)}
                        horizontal
                        pagingEnabled
                        snapToInterval={CARD_WIDTH}
                        decelerationRate="fast"
                        showsHorizontalScrollIndicator={false}
                      >
                        {parcour.illustres.map((illustre, index) => {
                          var IllustreLength = parcour.illustres.length;

                          return (
                            <View
                              key={index}
                              style={[
                                styles.illustreContent,
                                // { backgroundColor: "red", height: 200 },
                              ]}
                            >
                              <Text
                                adjustsFontSizeToFit
                                adjustsFontSizeToFit
                                style={[
                                  styles.cardIllustreTitle,
                                  { marginBottom: 30 },
                                ]}
                              >
                                {translate(illustre.title, this.props.Locale)}
                              </Text>
                              <View
                                style={{
                                  flexDirection: "row",
                                  justifyContent: "space-around",
                                  alignItems: "center",
                                }}
                              >
                                <TouchableOpacity
                                  style={{ marginRight: 30 }}
                                  onPress={() =>
                                    index === 0
                                      ? this.scroll.scrollTo({
                                          x: IllustreLength * CARD_WIDTH,
                                        })
                                      : this.scroll.scrollTo({
                                          x: index * CARD_WIDTH - CARD_WIDTH,
                                        })
                                  }
                                >
                                  <Feather
                                    name="arrow-left-circle"
                                    size={32}
                                    color="black"
                                  />
                                </TouchableOpacity>

                                <Image
                                  source={illustre.image}
                                  style={[
                                    styles.imageIllustre,
                                    {
                                      borderRadius: 100,
                                      borderColor: "white",
                                      borderWidth: 5,
                                    },
                                  ]}
                                />
                                <TouchableOpacity
                                  style={{ marginLeft: 30 }}
                                  onPress={() => {
                                    index === IllustreLength - 1
                                      ? this.scroll.scrollTo({
                                          x: 0,
                                        })
                                      : this.scroll.scrollTo({
                                          x: index * CARD_WIDTH + CARD_WIDTH,
                                        });
                                  }}
                                >
                                  <Feather
                                    name="arrow-right-circle"
                                    size={32}
                                    color="black"
                                  />
                                </TouchableOpacity>
                              </View>
                              <ScrollView
                                style={{ paddingHorizontal: 15, marginTop: 20 }}
                              >
                                <Text
                                  adjustsFontSizeToFit
                                  style={{ fontSize: 14 }}
                                >
                                  {translate(
                                    illustre.description,
                                    this.props.Locale
                                  )}
                                </Text>
                              </ScrollView>
                            </View>
                          );
                        })}
                      </ScrollView>
                    </Animated.View>
                  ) : (
                    <View>
                      <TouchableOpacity
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                        }}
                        onPress={this.showMoreParcourInfo}
                      >
                        <Text
                          adjustsFontSizeToFit
                          numberOfLines={1}
                          style={styles.cardtitleZoomed}
                        >
                          {translate(parcour.title, this.props.Locale)}
                        </Text>
                        <Entypo
                          name="chevron-down"
                          size={25}
                          color="black"
                          style={{ marginTop: 10 }}
                        />
                      </TouchableOpacity>
                      <ScrollView style={styles.textContent}>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginBottom: 2,
                            width: CARD_WIDTH,
                            paddingHorizontal: 20,
                          }}
                        >
                          {parcour.time !== undefined && (
                            <View style={{ flexDirection: "row" }}>
                              <Feather name="clock" size={16} color="black" />
                              <Text
                                adjustsFontSizeToFit
                                numberOfLines={1}
                                style={styles.cardTimeDifficulteZoomed}
                              >
                                {parcour.time}
                              </Text>
                            </View>
                          )}
                          {parcour.distance !== undefined && (
                            <View style={{ flexDirection: "row" }}>
                              <FontAwesome5
                                name="walking"
                                size={16}
                                color="black"
                              />
                              <Text
                                adjustsFontSizeToFit
                                numberOfLines={1}
                                style={styles.cardTimeDifficulteZoomed}
                              >
                                {parcour.distance}
                              </Text>
                            </View>
                          )}
                        </View>
                        {parcour.difficulty !== undefined && (
                          <View
                            style={{
                              flexDirection: "row",
                              width: CARD_WIDTH,
                              paddingHorizontal: 20,
                              marginBottom: 20,
                            }}
                          >
                            <Image
                              source={parcour.type}
                              style={styles.cardIcon}
                              resizeMode="cover"
                            />
                            <Text
                              adjustsFontSizeToFit
                              numberOfLines={1}
                              style={styles.cardTimeDifficulteZoomed}
                            >
                              {translate("Difficulty", this.props.Locale)} :{" "}
                              {translate(parcour.difficulty, this.props.Locale)}
                            </Text>
                          </View>
                        )}
                        {parcour.imageGroup === undefined ? (
                          <View style={{ width: CARD_WIDTH }}>
                            <Image
                              source={parcour.image}
                              style={styles.cardImageZoomed}
                              resizeMode="cover"
                            />
                          </View>
                        ) : (
                          <View style={{ width: CARD_WIDTH - 100 }}>
                            <PictureCarousel
                              data={parcour.imageGroup}
                              sliderwidth={CARD_WIDTH}
                            />
                          </View>
                        )}
                        <Text
                          adjustsFontSizeToFit
                          style={styles.cardDescription}
                        >
                          {translate(parcour.description, this.props.Locale)}
                        </Text>

                        <Button
                          title={translate("Lets.Go", this.props.Locale)}
                          type="solid"
                          onPress={() => this.startRando(index)}
                          icon={
                            <Entypo
                              name="chevron-right"
                              size={20}
                              color="black"
                            />
                          }
                          buttonStyle={{
                            backgroundColor: "rgba(255, 255, 255, 0.75)",
                            width: width * 0.35,
                            borderRadius: 20,
                            marginVertical: 20,
                            marginHorizontal: (CARD_WIDTH - width * 0.35) / 2,
                          }}
                          titleStyle={{
                            color: "black",
                            fontWeight: "600",
                            fontSize: 14,
                            marginLeft: 5,
                          }}
                        />
                      </ScrollView>
                    </View>
                  )}
                </Animated.View>
              </TouchableOpacity>
            );
          })}
        </Animated.ScrollView>
        <View style={styles.barContainer}>{barArray}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 8,
  },
  map: {
    flex: 1,
    width: width,
  },
  scrollView: {
    position: "absolute",
    bottom: 20,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  card: {
    padding: 0,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: (width - CARD_WIDTH) / 2,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    overflow: "hidden",
    borderRadius: 20,
    borderWidth: 2,
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  cardImageZoomed: {
    width: "100%",
    height: 0.25 * CARD_HEIGHT,
    alignSelf: "center",
    marginBottom: 10,
  },
  cardIcon: {
    width: 16,
    height: 16,
  },
  textContent: {
    flex: 1,
    width: CARD_WIDTH,
  },
  cardtitle: {
    fontSize: 15,
    marginTop: 5,
    fontWeight: "bold",
    textAlign: "center",
  },
  cardtitleZoomed: {
    fontSize: 20,
    marginVertical: 10,
    fontWeight: "bold",
    textAlign: "center",
  },
  cardDescription: {
    fontSize: 13,
    fontWeight: "500",
    width: CARD_WIDTH,
    paddingHorizontal: 20,
    textAlign: "justify",
  },
  cardTimeDifficulte: {
    fontSize: 13,
    color: "#444",
    marginLeft: 5,
  },
  cardTimeDifficulteZoomed: {
    fontSize: 16,
    color: "#444",
    marginLeft: 5,
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  ring: {
    width: 15,
    height: 15,
    borderRadius: 12,
    borderWidth: 1,
  },
  cardIllustreTitle: {
    fontSize: 16,
    fontWeight: "600",
    textAlign: "center",
  },
  imageIllustre: {
    width: 130,
    height: 160,
  },
  illustreContent: {
    flex: 1,
    width: CARD_WIDTH,
    alignItems: "center",
  },
  barContainer: {
    position: "absolute",
    bottom: 5,
    flexDirection: "row",
  },
  track: {
    backgroundColor: "#ccc",
    overflow: "hidden",
    height: 5,
  },
  bar: {
    height: 5,
    position: "absolute",
    left: 0,
    top: 0,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(ParcourCarousel);
