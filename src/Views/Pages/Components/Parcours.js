import * as React from "react";
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Vibration,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import SideDrawer from "./SideDrawer";
import { Divider } from "react-native-elements";
const { width, height } = Dimensions.get("screen");
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from "react-native-maps";
import PolyLine from "@mapbox/polyline";
import { translate } from "../../../Langues/locale";
import axios from "axios";
import * as Location from "expo-location";
import MenuDrawer from "react-native-side-drawer";
import { FontAwesome5, AntDesign } from "@expo/vector-icons";
import { setStep, setParcourName } from "../../../Redux/parcours/actions";

const GOOGLEAPIKEY = require("../../../Services/config").GOOGLEAPIKEY;
import EventParcour from "./EventParcour";
import { Platform } from "react-native";

class Parcours extends React.Component {
  constructor(props) {
    super(props);
    this.watchID = null;
    this._mapView = null;
    this.location = null;
    this.state = {
      pointsCoords: undefined,
      parcours: undefined,
      userParcourMessage: "NatureLoisirs.GoToStartPoint",

      step: 0,
      event: false,
      eventTitle: undefined,
      nextEventTitle: undefined,
      eventDescription: undefined,
      eventImage: undefined,
      eventSound: undefined,
      blockEvent: false,

      sideMenu: false,
      showAudioDescription: false,
    };
    //****** Get Parcour ******/
    if (this.props.parcours !== undefined) {
      this.state = {
        ...this.state,
        parcours: this.props.parcours,
      };
    }
  }

  async componentDidMount() {
    this._getLocationAsync();

    var { markers, startEndMarker } = this.state.parcours;
    var { step, event, blockEvent } = this.state;

    var stepArray = undefined;

    if (markers) {
      stepArray = startEndMarker.concat(markers);
      if (startEndMarker.length === 2) {
        var elm = stepArray.splice(1, 1)[0];
        stepArray.splice(stepArray.length, 0, elm);
      }
    } else {
      stepArray = startEndMarker;
    }

    if (this.state.parcours.name === this.props.parcoursName) {
      this.props.setStep(this.props.parcoursEvent.step);
      this.setState({
        step: this.props.parcoursEvent.step,
      });
      if (this.props.parcoursEvent.step !== 0) {
        this.setState({
          userParcourMessage: "Next.Stop",
          nextEventTitle: stepArray[this.props.parcoursEvent.step].title,
        });
      }
    } else {
      this.props.setStep(0);
      this.props.setParcourName(this.state.parcours.name);
    }
  }

  componentWillUnmount() {
    if (this.location !== null) {
      this.location.remove();
    }
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  async _getLocationAsync() {
    //******* Location Precision *******/
    var options = {
      accuracy: 6,
      timeInterval: 3000,
      distanceInterval: 5,
    };

    var onNewLocation = (newLocation) => {
      let currentPosition = {
        latitude: newLocation.coords.latitude,
        longitude: newLocation.coords.longitude,
      };
      if (this.state.step === 0) {
        this._getRouteDirections(currentPosition);
      }

      this.checkUserDistanceToMarker(currentPosition);
    };

    this.location = await Location.watchPositionAsync(
      options,
      onNewLocation,
      (error) => console.log(error)
    );
    return this.location;
  }

  async _getRouteDirections() {
    var options = {
      accuracy: 4,
    };
    var currentPositionAsync = await Location.getLastKnownPositionAsync(
      options
    );
    var currentPosition = {
      latitude: currentPositionAsync.coords.latitude,
      longitude: currentPositionAsync.coords.longitude,
    };
    if (this._mapView !== null && this.state.pointsCoords === undefined) {
      let getDirectionUrl = `https://maps.googleapis.com/maps/api/directions/json?origin=${currentPosition.latitude},${currentPosition.longitude}&destination=${this.state.parcours.startEndMarker[0].latitude},${this.state.parcours.startEndMarker[0].longitude}&mode=walking&key=${GOOGLEAPIKEY}`;
      try {
        const resp = await axios.get(getDirectionUrl);
        if (resp.data.routes[0] !== undefined) {
          const points = PolyLine.decode(
            resp.data.routes[0].overview_polyline.points
          );
          const pointsCoords = points.map((point) => {
            return { latitude: point[0], longitude: point[1] };
          });
          this.setState({ pointsCoords: pointsCoords });
        }
        if (currentPosition !== undefined && this._mapView !== null) {
          this._mapView.fitToCoordinates(
            [currentPosition, this.state.parcours.startEndMarker[0]],
            {
              edgePadding: { top: 50, right: 50, bottom: 50, left: 50 },
              animated: true,
            }
          );
        }
      } catch (error) {
        console.error(error);
      }
    }
  }

  async checkUserDistanceToMarker(currentPosition) {
    var { markers, startEndMarker } = this.state.parcours;
    var { step, event, blockEvent } = this.state;
    var stepArray = undefined;

    //******* Make array of marker ********/
    if (markers) {
      stepArray = startEndMarker.concat(markers);
      if (startEndMarker.length === 2) {
        var elm = stepArray.splice(1, 1)[0];
        stepArray.splice(stepArray.length, 0, elm);
      }
    } else {
      stepArray = startEndMarker;
    }

    var distanceTomarker = this.measure(
      currentPosition.latitude,
      currentPosition.longitude,
      stepArray[step].latitude,
      stepArray[step].longitude
    );
    //**** on Approch ****/
    if (event === false && distanceTomarker < 12) {
      // if (Platform.OS === "ios") {
      Vibration.vibrate();
      // }

      if (stepArray[step].title !== undefined) {
        if (stepArray[step].audio !== undefined) {
          this.setState({
            eventSound: stepArray[step].audio,
          });
        }
        this.setState({
          event: true,
          eventTitle: stepArray[step].title,
          eventDescription: stepArray[step].description,
          eventImage: stepArray[step].image,
        });
      }
      if (step + 1 < stepArray.length) {
        this.setState({
          userParcourMessage: "Next.Stop",
          nextEventTitle: stepArray[step + 1].title,
        });
      } else if (step + 1 === stepArray.length) {
        this.setState({
          userParcourMessage: "Finish",
        });
      }

      this.props.setStep(this.state.step + 1);

      this.setState({ step: this.state.step + 1 });
    }

    var distanceToOldmarker =
      step !== 0 &&
      this.measure(
        currentPosition.latitude,
        currentPosition.longitude,
        stepArray[step - 1].latitude,
        stepArray[step - 1].longitude
      );
    //**** on Leave ****/
    if (event === true && distanceToOldmarker > 15) {
      this.setState({
        event: false,
        blockEvent: false,
        eventTitle: undefined,
        eventDescription: undefined,
        eventImage: undefined,
        eventSound: undefined,
      });
    }
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  updateChildState(data) {
    this.setState(data);
  }

  handleInfoPress = () => {
    this.setState({ sideMenu: !this.props.sideMenu, event: false });
    this.updateParentState({ sideMenu: !this.props.sideMenu });
  };

  //********** MATH FUNCTION *********/
  measure(lat1, lon1, lat2, lon2) {
    var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 * Math.PI) / 180 - (lat1 * Math.PI) / 180;
    var dLon = (lon2 * Math.PI) / 180 - (lon1 * Math.PI) / 180;
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000; // meters
  }

  convertPolyline(arrayOfPoint) {
    let latLngArray = [];
    for (let i = 0; i < arrayOfPoint.length; i++) {
      const gData = {
        latitude: arrayOfPoint[i][1],
        longitude: arrayOfPoint[i][0],
      };
      latLngArray.push(gData);
    }
    return latLngArray;
  }

  render() {
    var { startEndMarker, markers, polyline } = this.state.parcours;
    var {
      eventTitle,
      eventDescription,
      eventImage,
      step,
      eventSound,
      event,
      showAudioDescription,
      parcours,
    } = this.state;

    var startEndMarkers = startEndMarker.map((markers, index) => {
      return (
        <Marker key={index} identifier={"mk" + index} coordinate={markers}>
          <View
            style={[
              styles.ring,
              this.props.mode === "NatureLoisir"
                ? {
                    borderColor: index === 0 ? "#5F7EFA" : "#58B557",
                    backgroundColor: index === 0 ? "#5F7EFA" : "#58B557",
                  }
                : {
                    borderColor: "#5F7EFA",
                    backgroundColor: "#5F7EFA",
                  },
            ]}
          />
        </Marker>
      );
    });

    var MarkerEvent =
      markers !== undefined &&
      markers.map((markers, index) => {
        var id = index + 1;
        return (
          <Marker key={index} identifier={"mk" + id} coordinate={markers}>
            <View
              style={[
                styles.ring,
                styles.markerWrap,
                this.props.mode === "NatureLoisir"
                  ? {
                      borderColor: index + 1 === 0 ? "#5F7EFA" : "#58B557",
                      backgroundColor: index + 1 === 0 ? "#5F7EFA" : "#58B557",
                    }
                  : {
                      borderColor: "#5F7EFA",
                      backgroundColor: "#5F7EFA",
                    },
              ]}
            >
              <Text
                adjustsFontSizeToFit
                style={{ fontSize: 12, fontWeight: "500", color: "white" }}
              >
                {index + 1}
              </Text>
            </View>
          </Marker>
        );
      });
    var convertedPolyline = this.convertPolyline(polyline);
    var parcourLength = markers && markers.length + startEndMarker.length;
    var sideMenu = this.props.sideMenu;
    return (
      <View style={styles.container}>
        {/* //*********************** ON EVENT *************/}
        {event === true && (
          <EventParcour
            eventTitle={eventTitle}
            eventDescription={eventDescription}
            eventImage={eventImage}
            eventSound={eventSound}
            event={event}
            step={step}
            parcourLength={parcourLength}
            showAudioDescription={showAudioDescription}
            color={this.props.color}
            updateParentState={this.updateChildState.bind(this)}
          />
        )}
        <Divider style={styles.divider} />
        <MenuDrawer
          open={this.props.sideMenu}
          drawerContent={
            <SideDrawer
              startEnd={startEndMarker}
              markers={markers}
              currentStep={step}
              updateParentState={this.updateChildState.bind(this)}
              updateParentofParentState={this.updateParentState.bind(this)}
              mode={this.props.mode}
              color={this.props.color2}
            />
          }
          drawerPercentage={85}
          overlay={true}
          captureGestures={false}
          animationTime={100}
        >
          <MapView
            ref={(map) => (this._mapView = map)}
            initialRegion={{
              latitude: this.state.parcours.region.latitude,
              longitude: this.state.parcours.region.longitude,
              latitudeDelta: this.state.parcours.region.latitudeDelta,
              longitudeDelta: this.state.parcours.region.longitudeDelta,
            }}
            style={styles.map}
            showsUserLocation={true}
            showsMyLocationButton={true}
            onPress={() => sideMenu && this.handleInfoPress()}
            // maxZoomLevel={16}
          >
            {/* ************ CONTENU MAP ************** */}

            {startEndMarkers}
            {MarkerEvent}
            <Polyline
              coordinates={convertedPolyline}
              strokeWidth={5}
              strokeColor={
                this.props.mode === "NatureLoisir" ? "#58B557" : "#5F7EFA"
              }
            />

            {this.state.pointsCoords !== undefined &&
              this.props.parcoursEvent.step === 0 && (
                <MapView.Polyline
                  coordinates={this.state.pointsCoords}
                  strokeWidth={2}
                  strokeColor={"#23A1F5"}
                />
              )}
          </MapView>

          {/* ************ EN TETE PARCOUR ************** */}
          <TouchableOpacity
            style={{
              height: 40,
              backgroundColor: this.props.color,
              opacity: 0.7,
              padding: 8,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              flexDirection: "row",
              justifyContent: "space-around",
              position: "absolute",
              width: width * 0.8,
              marginHorizontal: width * 0.1,
              // flex:1
            }}
            onPress={() => {
              parcours.markers !== undefined && this.handleInfoPress();
            }}
          >
            <Text
              adjustsFontSizeToFit
              numberOfLines={1}
              style={[styles.cardtitle, { color: "white" }]}
            >
              {translate(parcours.title, this.props.Locale)}
            </Text>
            <FontAwesome5
              name="info-circle"
              size={24}
              color={"black"}
              // style={{ position: "absolute", right: 20, top: 10 }}
            />
          </TouchableOpacity>

          {/* ************ FOOTER ************** */}

          <View
            style={{
              height: height * 0.1,
              backgroundColor: this.props.color,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              adjustsFontSizeToFit
              style={{ color: "white", fontWeight: "bold" }}
            >
              {translate(this.state.userParcourMessage, this.props.Locale)}
            </Text>
            {this.state.userParcourMessage === "Next.Stop" && (
              <Text
                adjustsFontSizeToFit
                style={{ color: "white", fontWeight: "600" }}
              >
                {translate(this.state.nextEventTitle, this.props.Locale)}
              </Text>
            )}
          </View>
        </MenuDrawer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 1,
  },
  map: {
    height:
      Platform.OS === "ios"
        ? height * 0.75
        : height * 0.73 - StatusBar.currentHeight,
  },
  cardtitle: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  cardinfo: {
    fontSize: 15,
    fontWeight: "bold",
    textAlign: "center",
  },

  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
    zIndex: 0,
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
  ring: {
    width: 15,
    height: 15,
    borderRadius: 7,
    // position: "absolute",
    borderWidth: 1,
  },
  divider: {
    backgroundColor: "black",
    height: 0.5,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 20,
    },
    shadowRadius: 3,
    elevation: 30,
  },
});

const mapStateToProps = ({ locale, parcours }) => {
  const { Locale } = locale;
  const { parcoursEvent, parcoursName } = parcours;
  return { Locale, parcoursEvent, parcoursName };
};

export default connect(mapStateToProps, { setParcourName, setStep })(Parcours);
