import React, { Component } from "react";
import { Text, View, Dimensions, StyleSheet, Image } from "react-native";
import Carousel from "react-native-snap-carousel"; // Version can be specified in package.json

import {
  scrollInterpolator2,
  animatedStyles2,
} from "./utils/carouselAnimation";
const { width, height } = Dimensions.get("window");

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 5);

export default class PictureCarousel extends Component {
  state = {
    index: 0,
  };

  constructor(props) {
    super(props);
    this._renderItem = this._renderItem.bind(this);
  }

  _renderItem({ item, index }) {
    return (
      <Image
        style={styles.itemContainer}
        source={item}
        resizeMode={"contain"}
      />
    );
  }

  render() {
    return (
      <View style={{ paddingBottom: 10 }}>
        <Carousel
          ref={(c) => (this.carousel = c)}
          data={this.props.data}
          renderItem={this._renderItem}
          containerCustomStyle={{ flexGrow: 0 }}
          sliderWidth={
            this.props.sliderwidth === undefined
              ? SLIDER_WIDTH
              : this.props.sliderwidth
          }
          itemWidth={ITEM_WIDTH}
          inactiveSlideShift={0}
          onSnapToItem={(index) => this.setState({ index })}
          scrollInterpolator={scrollInterpolator2}
          slideInterpolatedStyle={animatedStyles2}
          useScrollView={true}
          loop={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 50,
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent",
  },
  itemLabel: {
    color: "white",
    fontSize: 24,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
});
