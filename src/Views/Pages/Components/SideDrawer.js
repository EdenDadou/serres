import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, StatusBar } from "react-native";
import { Feather, FontAwesome5 } from "@expo/vector-icons";
import { translate } from "../../../Langues/locale";
import { connect } from "react-redux";
import { TouchableOpacity } from "react-native";
import { setStep, setParcourName } from "../../../Redux/parcours/actions";
import { Platform } from "react-native";

const { width, height } = Dimensions.get("screen");

class SideDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: undefined,
    };
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }
  updateParentofParentState(data) {
    this.props.updateParentofParentState(data);
  }

  goNextStep() {
    var markers = this.props.markers;
    var startEndMarker = this.props.startEnd;
    var stepArray = undefined;

    //******* Make array of marker ********/
    if (markers) {
      stepArray = startEndMarker.concat(markers);
      if (startEndMarker.length === 2) {
        var elm = stepArray.splice(1, 1)[0];
        stepArray.splice(stepArray.length, 0, elm);
      }
    } else {
      stepArray = startEndMarker;
    }

    var etapesLength = stepArray.length;

    var nextStep = this.props.currentStep + 1;
    if (nextStep < etapesLength) {
      this.props.setStep(nextStep);
      this.updateParentState({
        step: nextStep,
        userParcourMessage: "Next.Stop",
      });

      if (
        stepArray[nextStep] !== undefined &&
        stepArray[nextStep].title !== undefined
      ) {
        this.updateParentState({
          nextEventTitle: stepArray[nextStep].title,
        });
      }
    } else if (parseInt(nextStep) === parseInt(etapesLength)) {
      this.updateParentState({
        step: nextStep,
        userParcourMessage: "Finish",
      });
    }
  }
  goPreviousStep() {
    var markers = this.props.markers;
    var startEndMarker = this.props.startEnd;
    var stepArray = undefined;

    //******* Make array of marker ********/
    if (markers) {
      stepArray = startEndMarker.concat(markers);
      if (startEndMarker.length === 2) {
        var elm = stepArray.splice(1, 1)[0];
        stepArray.splice(stepArray.length, 0, elm);
      }
    } else {
      stepArray = startEndMarker;
    }

    if (this.props.currentStep > 0) {
      this.props.setStep(this.props.currentStep - 1);

      this.updateParentState({
        step: this.props.currentStep - 1,
        userParcourMessage: "Next.Stop",
        nextEventTitle: stepArray[this.props.currentStep - 1].title,
      });
    }
  }

  viewEvent(title, desc, image, audio, index) {
    this.updateParentState({
      event: true,
      eventTitle: title,
      eventDescription: desc,
      eventImage: image,
      eventSound: audio,
      blockEvent: true,
    });
  }

  render() {
    var markers = this.props.markers;
    var startEndMarker = this.props.startEnd;
    var stepArray = undefined;

    //******* Make array of marker ********/
    if (markers) {
      stepArray = startEndMarker.concat(markers);
      if (startEndMarker.length === 2) {
        var elm = stepArray.splice(1, 1)[0];
        stepArray.splice(stepArray.length, 0, elm);
      }
    } else {
      stepArray = startEndMarker;
    }

    var etapesLength = stepArray.length + 1;
    var etapes = stepArray;
    var spaceBetweenStep =
      Platform.OS === "ios"
        ? (height * 0.85 - 100 - etapesLength * 10) / etapesLength
        : (height * 0.84 - StatusBar.currentHeight - 100 - etapesLength * 10) /
          etapesLength;

    const parcourStep =
      markers !== undefined &&
      markers.map((item, index) => {
        return (
          <View key={index}>
            <TouchableOpacity
              onPress={() =>
                this.viewEvent(
                  item.title,
                  item.description,
                  item.image,
                  item.audio,
                  index
                )
              }
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    height: 20,
                    width: 20,
                    borderRadius: 10,
                    backgroundColor:
                      index <= this.props.currentStep - 2
                        ? "#5F7EFA"
                        : this.props.color,
                    marginHorizontal: 10,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text
                    adjustsFontSizeToFit
                    style={{ color: "white", fontWeight: "600" }}
                  >
                    {index + 1}
                  </Text>
                </View>

                <Text
                  adjustsFontSizeToFit
                  style={{ fontSize: 12, fontWeight: "700" }}
                >
                  {translate(item.title, this.props.Locale)}
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={{
                height: spaceBetweenStep,
                backgroundColor:
                  index <= this.props.currentStep - 2
                    ? "#5F7EFA"
                    : this.props.color,
                width: 10,
                marginLeft: 15,
                marginVertical: -5,
                zIndex: -2,
              }}
            />
          </View>
        );
      });

    return (
      <View
        style={[
          styles.body,
          {
            backgroundColor:
              this.props.mode === "NatureLoisir"
                ? "rgba(88, 181, 87, 0.6)"
                : "rgba(128, 148, 251, 0.6)",
          },
        ]}
      >
        <TouchableOpacity
          onPress={() =>
            this.updateParentofParentState({
              sideMenu: false,
            })
          }
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: 10,
            backgroundColor: this.props.color,
            opacity: 0.8,
            alignItems: "center",
            height: 50,
          }}
        >
          <Text
            adjustsFontSizeToFit
            style={{ color: "white", fontWeight: "600" }}
            adjustsFontSizeToFit
          >
            {translate("Close.Info", this.props.Locale)}
          </Text>
          <FontAwesome5 name="info-circle" size={24} color={"black"} />
        </TouchableOpacity>
        <View style={{ justifyContent: "center" }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
            }}
          >
            <View
              style={{
                height: 20,
                width: 20,
                borderRadius: 10,
                backgroundColor:
                  this.props.currentStep > 0 ? "#5F7EFA" : this.props.color,
                marginHorizontal: 10,
              }}
            />
            <Text adjustsFontSizeToFit>
              {translate("Parcours.Start", this.props.Locale)}
            </Text>
          </View>

          <View
            style={{
              height: spaceBetweenStep,
              backgroundColor:
                this.props.currentStep > 0 ? "#5F7EFA" : this.props.color,
              width: 10,
              marginLeft: 15,
              marginVertical: -5,
              zIndex: -2,
            }}
          />
          {parcourStep}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
            }}
          >
            <View
              style={{
                height: 20,
                width: 20,
                borderRadius: 10,
                backgroundColor:
                  this.props.currentStep >= etapesLength + 1
                    ? "#5F7EFA"
                    : this.props.color,
                marginHorizontal: 10,
              }}
            />
            <Text adjustsFontSizeToFit>
              {translate("Parcours.End", this.props.Locale)}
            </Text>
          </View>
        </View>
        {/* //********* Barre navogation between step ********/}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 10,
            backgroundColor: this.props.color,
            opacity: 0.8,
            alignItems: "center",
            height: 50,
          }}
        >
          <Feather
            name="arrow-left-circle"
            size={24}
            color={"white"}
            // style={{ position: "absolute", right: 30, top: 10 }}
            onPress={() => this.goPreviousStep()}
          />
          <Text
            adjustsFontSizeToFit
            style={{ color: "white", fontWeight: "600" }}
          >
            {this.props.currentStep === 0
              ? translate("Parcours.Start", this.props.Locale)
              : this.props.currentStep === etapesLength + 1
              ? translate("Parcours.End", this.props.Locale)
              : translate("Parcours.Step", this.props.Locale) +
                " " +
                parseInt(this.props.currentStep - 1)}
          </Text>
          <Feather
            name="arrow-right-circle"
            size={24}
            color={"white"}
            // style={{ position: "absolute", right: 30, top: 10 }}
            onPress={() => this.goNextStep()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    height:
      Platform.OS === "ios"
        ? height * 0.85
        : height * 0.8 - StatusBar.currentHeight,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    justifyContent: "space-between",
  },
  inputSearch: {
    marginLeft: 0,
  },
  headerName: {
    fontSize: 18,
    marginRight: 3,
    marginLeft: 25,
    marginTop: 25,
  },
  headerName2: {
    fontSize: 18,
    fontWeight: "600",
    marginTop: 25,
  },
  contentSideMenu: {
    height: height * 0.8,
    marginHorizontal: "10%",
    marginTop: 15,
  },
  buttonSideMenu: {
    marginVertical: 15,
  },
  textButtonSideMenu: {
    fontSize: 24,
    color: "white",
    fontWeight: "700",
    // marginHorizontal: width * 0.08
  },
  dividerLabelStyle: {
    backgroundColor: "white",
    height: 3,
    // marginVertical: 10
  },
  driverSpace: {
    backgroundColor: "black",
    height: height * 0.3,
    width: width * 0.8,
    flexDirection: "row",
  },
  textDriverSpace: {
    width: "65%",
    fontSize: 30,
    fontWeight: "700",
    color: "white",
    marginHorizontal: "5%",
    marginVertical: "2%",
  },
});

const mapStateToProps = ({ locale, parcours }) => {
  const { Locale } = locale;
  const { parcoursEvent, parcoursName } = parcours;
  return { Locale };
};

export default connect(mapStateToProps, { setStep })(SideDrawer);
