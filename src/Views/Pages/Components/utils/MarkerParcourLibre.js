import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, Image } from "react-native";
import { translate } from "../../../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import MapView, { Callout } from "react-native-maps";

class MarkerPL extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: false,
      playing: false,
    };
  }

  updateParentState(data) {
    this.props.updateParentState(data);
  }

  render() {
    var { markers, index, coordinate, region, markerImg } = this.props;
    // console.log("img root", markerImg);

    return (
      <MapView.Marker
        key={index}
        coordinate={coordinate}
        onPress={() => {
          this.props.handleMarkerPress(index, region, markers);
        }}
      >
        <Image source={markers.logo} style={{ height: 20, width: 20 }} />
      </MapView.Marker>
    );
  }
}

const styles = StyleSheet.create({
  markerCallout: {
    width: height * 0.45,
    // height: height * 0.6,
    backgroundColor: "rgba(252, 193, 79,0.8)",
    borderRadius: 20,
    borderColor: "black",
    borderWidth: 0.5,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(MarkerPL);
