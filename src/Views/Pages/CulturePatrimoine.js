import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import { Divider } from "react-native-elements";
import { Feather } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("screen");
import ParcourCarousel from "./Components/ParcourCarousel";
import Parcours from "./Components/Parcours";
import { ParcoursCulturePatrimoine } from "../Assets/CulturePatrimoine/ParcoursCulturePatrimoine";

class CulturePatrimoine extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      SelectedParcour: undefined,
      sideMenu: false,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    if (this.state.sideMenu === false) {
      this.props.navigation.navigate("Menu");
      return true;
    } else {
      this.setState({ sideMenu: false });
      return true;
    }
  }

  updateChildState(data) {
    this.setState(data);
  }
  render() {
    var Region = {
      latitude: 44.427,
      longitude: 5.715,
      latitudeDelta: 0.01,
      longitudeDelta: 0.004,
    };

    return (
      <View style={styles.container}>
        {/* **************** HEADER **************** */}
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Menu")}
            style={{ marginHorizontal: 10 }}
          >
            <Feather
              name="menu"
              size={40}
              color="white"
              style={{
                textShadowColor: "black",
                textShadowOffset: { width: 0.1, height: 0.1 },
                textShadowRadius: 0.1,
              }}
            />
          </TouchableOpacity>

          <Text adjustsFontSizeToFit style={styles.textHeader}>
            {translate("Menu.Culture.Patrimoine", this.props.Locale)}
          </Text>
        </View>
        <Divider style={styles.divider} />

        {/* ************** PARCOUR ET CARROUSEL *********** */}
        {this.state.SelectedParcour === undefined ? (
          <ParcourCarousel
            updateParentState={this.updateChildState.bind(this)}
            parcours={ParcoursCulturePatrimoine(this.props.Locale)}
            region={Region}
            color={"rgb(128, 148, 251)"}
            color2={"#4457BC"}
            mode={"CulturePatrimoine"}
          />
        ) : (
          <Parcours
            updateParentState={this.updateChildState.bind(this)}
            sideMenu={this.state.sideMenu}
            parcours={
              ParcoursCulturePatrimoine(this.props.Locale)[
                this.state.SelectedParcour
              ]
            }
            color={"rgb(128, 148, 251)"}
            color2={"#4457BC"}
            mode={"CulturePatrimoine"}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    backgroundColor: "#FFFFFF",
  },
  image: {
    resizeMode: "cover",
    width: width,
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    backgroundColor: "#8094FB",
    paddingTop: 20,
  },
  containerScrollView: {
    flex: 1,
  },
  divider: {
    backgroundColor: "black",
    height: 1,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  iconSidePosition: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 1,
  },
  textHeader: {
    fontSize: width * 0.08,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: 0.1,
    shadowOpacity: 0.1,
    color: "white",
  },
  imgPres: {
    width: width,
    height: 250,
    marginTop: 0,
    paddingTop: 0,
  },

  containerListItem: {
    backgroundColor: "#ECDC4A",
    shadowColor: "black",
    width: width,
    height: height * 0.4,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.12,
    shadowRadius: 5.46,
    elevation: 20,
    padding: 0,
    marginBottom: 4,
  },
  textTitleListItem: {
    fontSize: 20,
    fontFamily: "Palanquin_700Bold",
  },
  textDescListItem: {
    fontSize: 13,
    marginTop: -10,
    // marginBottom:10,
    fontFamily: "Palanquin_500Medium",
  },
  rowCenter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#ECDC4A",
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    width: width,
    opacity: 0.8,
  },
  columnCenter: {
    flexDirection: "column",
    alignItems: "flex-start",
    marginTop: height * 0.3,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(CulturePatrimoine);
