import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Image,
  BackHandler,
} from "react-native";
import { Divider } from "react-native-elements";
import { Feather } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
import { ScrollView } from "react-native";
const { width, height } = Dimensions.get("window");
import { Entypo } from "@expo/vector-icons";
import GameParcour from "./Components/GameParcour";
import { Enigmes } from "../Assets/EscapeGame/Enigmes";

class EscapeGame extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      SelectedLevel: null,
      step: 1,
      goodAnswer: 0,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("Menu");
    return true;
  }

  updateChildState(data) {
    this.setState(data);
  }
  render() {
    var Region = {
      latitude: 44.429,
      longitude: 5.717,
      latitudeDelta: 0.01,
      longitudeDelta: 0.001,
    };

    var { SelectedLevel } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Menu")}
            style={{ marginHorizontal: 10 }}
          >
            <Feather
              name="menu"
              size={40}
              color="white"
              style={{
                textShadowColor: "black",
                textShadowOffset: { width: 0.1, height: 0.1 },
                textShadowRadius: 0.1,
              }}
            />
          </TouchableOpacity>

          <Text
            adjustsFontSizeToFit
            style={styles.textHeader}
            adjustsFontSizeToFit
          >
            {translate("Menu.Escape.Game", this.props.Locale)}
          </Text>
        </View>
        <Divider style={styles.divider} />
        {SelectedLevel === null ? (
          <ImageBackground
            source={require("../Assets/EscapeGame/image/texture.jpg")}
            style={styles.image}
          >
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{ paddingVertical: 20 }}
              contentContainerStyle={{ alignItems: "center" }}
            >
              <Text
                adjustsFontSizeToFit
                style={{
                  fontSize: 27,
                  fontFamily: "MedievalSharp_400Regular",
                  marginBottom: 20,
                  fontWeight: "900",
                }}
              >
                {translate("EscapeGame.Intro", this.props.Locale)}
              </Text>
              <Text
                adjustsFontSizeToFit
                style={{ fontSize: 14, marginBottom: 30 }}
              >
                {translate("EscapeGame.Intro.Desc", this.props.Locale)}
              </Text>
              <Text
                adjustsFontSizeToFit
                style={{ fontSize: 17, fontWeight: "700" }}
              >
                {translate("EscapeGame.Intro.Difficulty", this.props.Locale)}
              </Text>
              <TouchableOpacity
                style={{
                  backgroundColor: "#D83B3B",
                  borderRadius: 40,
                  width: width * 0.8,
                  height: 50,
                  marginVertical: 20,
                  paddingHorizontal: 20,
                  justifyContent: "space-between",
                  alignItems: "center",
                  flexDirection: "row",
                }}
                onPress={() => this.setState({ SelectedLevel: 0 })}
              >
                <Text
                  adjustsFontSizeToFit
                  style={{ fontSize: width * 0.05, fontWeight: "600" }}
                >
                  {translate("EscapeGame.Intro.Level.1", this.props.Locale)}
                </Text>
                <View
                  style={{ flexDirection: "row", justifyContent: "flex-end" }}
                >
                  <Image
                    style={{ width: 30, height: 30, marginLeft: 20 }}
                    source={require("../Assets/EscapeGame/image/icons8-adventure-100.png")}
                    resizeMode={"stretch"}
                  />
                  <Entypo name="chevron-right" size={25} color="black" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: "#D83B3B",
                  borderRadius: 40,
                  width: width * 0.8,
                  height: 50,
                  marginBottom: 40,
                  paddingHorizontal: 20,
                  justifyContent: "space-between",
                  alignItems: "center",
                  flexDirection: "row",
                }}
                onPress={() => this.setState({ SelectedLevel: 1 })}
              >
                <Text
                  adjustsFontSizeToFit
                  style={{ fontSize: width * 0.05, fontWeight: "600" }}
                >
                  {translate("EscapeGame.Intro.Level.2", this.props.Locale)}
                </Text>
                <View
                  style={{ flexDirection: "row", justifyContent: "flex-end" }}
                >
                  <Image
                    style={{ width: 30, height: 30, marginLeft: 20 }}
                    source={require("../Assets/EscapeGame/image/icons8-fairytale-100.png")}
                    resizeMode={"stretch"}
                  />
                  <Entypo name="chevron-right" size={25} color="black" />
                </View>
              </TouchableOpacity>
            </ScrollView>
          </ImageBackground>
        ) : (
          <GameParcour
            region={Region}
            step={this.state.step}
            difficulty={this.state.SelectedLevel}
            enigme={Enigmes[this.state.SelectedLevel]}
            goodAnswer={this.state.goodAnswer}
            updateParentState={this.updateChildState.bind(this)}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    backgroundColor: "#FFFFFF",
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    paddingTop: 20,
    backgroundColor: "#D83B3B",
  },
  divider: {
    backgroundColor: "black",
    height: 1,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  textHeader: {
    fontSize: 34,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: 0.1,
    shadowOpacity: 0.1,
    color: "white",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    paddingHorizontal: 20,
    alignItems: "center",
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(EscapeGame);
