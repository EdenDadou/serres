import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { ChangeLocale } from "../../Redux/locale/actions";
const { width, height } = Dimensions.get("window");
import { connect } from "react-redux";
import { Platform } from "react-native";

console.log(Platform)

class Language extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  changeLanguage = (langue) => {
    this.props.ChangeLocale(langue);
    this.props.navigation.navigate("Menu");
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerImgVille}>
          <Image
            style={styles.imgTown}
            source={require("../Assets/Langues/PhotoSerresVille.jpg")}
            resizeMode={"cover"}
            onLoadStart={() => this.setState({ loading: true })}
            onLoad={() => this.setState({ loading: false })}
          />
          <ActivityIndicator
            style={styles.activityIndicator}
            size="large"
            animating={this.state.loading}
          />
        </View>

        <View style={styles.containerBottom}>
          <View style={styles.containerHeader}>
            <Image
              style={styles.imgLogoLangues}
              source={require("../Assets/Langues/IconeChoixLang.png")}
              resizeMode={"cover"}
            />
            <Text adjustsFontSizeToFit style={styles.textBienvenue}>
              Bienvenue à Serres !
            </Text>
          </View>
          <View style={styles.containerFlag}>
            <TouchableOpacity
              style={styles.borderFlag}
              onPress={() => {
                this.changeLanguage("fr");
              }}
            >
              <Image
                style={styles.imgFlag}
                source={require("../Assets/Langues/IconeFrancais.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.borderFlag}
              onPress={() => {
                this.changeLanguage("en");
              }}
            >
              <Image
                style={styles.imgFlag}
                source={require("../Assets/Langues/IconeAnglais.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.borderFlag}
              onPress={() => {
                this.changeLanguage("it");
              }}
            >
              <Image
                style={styles.imgFlag}
                source={require("../Assets/Langues/IconeItalien.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.borderFlag}
              onPress={() => {
                this.changeLanguage("nl");
              }}
            >
              <Image
                style={styles.imgFlag}
                source={require("../Assets/Langues/IconeNLand.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    // justifyContent: "center",
  },
  containerImgVille: {
    // height: height * 0.8,
    flex: 0.8,
    width: width,
    borderWidth: 4,
    borderColor: "black",
  },
  containerBottom: {
    // height: height * 0.2,
    flex: 0.2,
    alignItems: "center",
    justifyContent: "center",
  },
  containerFlag: {
    width: width,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  containerHeader: {
    width: width,
    flexDirection: "row",
    marginTop: -7,
    marginLeft: -5,
    marginBottom: 15,
  },
  borderFlag: {
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "black",
    width: 46,
    height: 46,
  },
  imgTown: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  imgFlag: {
    width: 48,
    height: 48,
    alignSelf: "center",
    alignItems: "flex-start",
    marginTop: -3,
  },
  imgLogoLangues: {
    width: 48,
    height: 48,
  },
  textBienvenue: {
    fontSize: 20,
    fontWeight: Platform.OS === 'ios' ? "900" : "bold",
    marginTop: 10,
    marginHorizontal: "13%",
  },
  activityIndicator: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps, { ChangeLocale })(Language);
