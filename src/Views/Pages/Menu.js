import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Animated,
  Linking,
  BackHandler,
  PermissionsAndroid,
} from "react-native";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import { Entypo } from "@expo/vector-icons";
import { ChangeLocale } from "../../Redux/locale/actions";
import { Platform } from "react-native";
import store from "../../Redux/store";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      scaleAnimation: new Animated.ValueXY({ x: 0, y: 0 }),
      fadeAnim: new Animated.Value(1),
      AnimatedzIndex: new Animated.Value(2),
      animatedSettingHeight: new Animated.Value(height * 0.08),
      animatedSettingWidth: new Animated.Value(width * 0.16),
      settingZoomed: false,
      scaleTitle: new Animated.ValueXY({ x: 0, y: 0 }),
      errorOnPermission: false,
    };
  }

  componentDidMount() {
    this.requestLocationPermission();
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("Language");
    return true;
  }

  changeLanguage = (langue) => {
    this.props.ChangeLocale(langue);
    this.handleSettingClick();
  };

  requestLocationPermission = () => {
    if (Platform.OS === "android") {
      PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        ],
        {
          title: "We need your location Permission",
          message:
            "That App is a Tourism guide, in order to show you where to look, we need to know where you are... ",
          buttonNegative: "No thanks",
          buttonPositive: "Yes, you can use my location",
        }
      )
        .then((result) => {
          if (
            Object.values(result)[0] === "granted" &&
            Object.values(result)[1] === "granted"
          ) {
            console.log("PERMISSION LOCATION ACCORDÉ");
          } else {
            console.log("PERMISSION LOCATION REFUSÉ");
            this.setState({ errorOnPermission: true });
          }
        })
        .catch((err) => console.log(err));
    } else if (Platform.OS === "ios") {
      return true;
    }
  };

  handleSettingClick = () => {
    var { settingZoomed } = this.state;
    if (settingZoomed === false) {
      Animated.parallel([
        Animated.timing(this.state.fadeAnim, {
          useNativeDriver: true,
          toValue: 0,
          duration: 500,
        }),
        Animated.timing(this.state.AnimatedzIndex, {
          useNativeDriver: true,
          toValue: 0,
          duration: 500,
        }),
        Animated.timing(this.state.animatedSettingHeight, {
          useNativeDriver: false,
          toValue: 300,
          duration: 500,
        }),
        Animated.timing(this.state.animatedSettingWidth, {
          useNativeDriver: false,
          toValue: width * 0.9,
          duration: 500,
        }),
        Animated.spring(this.state.scaleAnimation, {
          useNativeDriver: true,
          toValue: { x: width / 2 - width * 0.075, y: -(height - 200) / 2 },
          velocity: 7,
          friction: 8,
        }),
        Animated.spring(this.state.scaleTitle, {
          useNativeDriver: true,
          toValue: { x: 0, y: 150 },
          velocity: 7,
          friction: 8,
        }),
      ]).start();
      setTimeout(() => {
        this.setState({ settingZoomed: true });
      }, 550);
    } else {
      Animated.parallel([
        Animated.timing(this.state.fadeAnim, {
          useNativeDriver: true,
          toValue: 1,
          duration: 500,
        }),
        Animated.timing(this.state.AnimatedzIndex, {
          useNativeDriver: true,
          toValue: 2,
          duration: 500,
        }),
        Animated.timing(this.state.animatedSettingHeight, {
          useNativeDriver: false,
          toValue: height * 0.08,
          duration: 500,
        }),
        Animated.timing(this.state.animatedSettingWidth, {
          useNativeDriver: false,
          toValue: width * 0.16,
          duration: 500,
        }),
        Animated.spring(this.state.scaleAnimation, {
          useNativeDriver: true,
          toValue: { x: 0, y: 0 },
          velocity: 7,
          friction: 8,
        }),
        Animated.spring(this.state.scaleTitle, {
          useNativeDriver: true,
          toValue: { x: 0, y: 0 },
          velocity: 8,
          friction: 8,
        }),
      ]).start();
      this.setState({ settingZoomed: false });
    }
  };

  render() {
    var { settingZoomed } = this.state;
    return (
      <ImageBackground
        source={require("../Assets/Menu/medieval.jpg")}
        style={styles.image}
      >
        <View style={styles.container}>
          <Animated.View
            style={[
              styles.containerHeaderMenu,
              {
                transform: this.state.scaleTitle.getTranslateTransform(),
              },
            ]}
          >
            <Image
              style={styles.imgBlason}
              source={require("../Assets/Menu/BlasonSerres.png")}
              resizeMode={"cover"}
            />
            <Text adjustsFontSizeToFit style={styles.textSerres}>
              Serres
            </Text>
            <Image
              style={styles.imgBlason}
              source={require("../Assets/Menu/BlasonSerres.png")}
              resizeMode={"cover"}
            />
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.state.fadeAnim,
              zIndex: this.state.AnimatedzIndex,
            }}
          >
            <TouchableOpacity
              style={[
                styles.dispoButtonMenu,
                {
                  backgroundColor: "rgba(128, 148, 251, 0.7)",
                },
              ]}
              onPress={() =>
                this.props.navigation.navigate("CulturePatrimoine")
              }
            >
              <View style={styles.dispoTextMenu}>
                <View style={styles.containerPastilleMenu}>
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Visite.Guide", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Culture.Patrimoine", this.props.Locale)}
                </Text>
              </View>
              <Image
                style={styles.imgPuceMenu}
                source={require("../Assets/Menu/CulturePat.jpg")}
                resizeMode={"cover"}
              />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.state.fadeAnim,
              zIndex: this.state.AnimatedzIndex,
            }}
          >
            <TouchableOpacity
              style={[
                styles.dispoButtonMenu,
                { backgroundColor: "rgba(88, 181, 87, 0.7)" },
              ]}
              onPress={() => this.props.navigation.navigate("NatureLoisirs")}
            >
              <View style={styles.dispoTextMenu}>
                <View style={styles.containerPastilleMenu}>
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Visite.Guide", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Nature.Loisir", this.props.Locale)}
                </Text>
              </View>
              <Image
                style={styles.imgPuceMenu}
                source={require("../Assets/Menu/NatureLoisirs.jpg")}
                resizeMode={"cover"}
              />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              flexDirection: "row",
              marginBottom: height * 0.03,
              opacity: this.state.fadeAnim,
            }}
          >
            <TouchableOpacity
              style={styles.dispoButtonArtisanat}
              onPress={() => this.props.navigation.navigate("Artisanat")}
            >
              <View style={styles.dispoTextMenu}>
                <View style={styles.containerPastilleMenu}>
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Carte.Interactive", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Artisanat", this.props.Locale)}
                </Text>
              </View>
              <Image
                style={styles.imgPuceMenu}
                source={require("../Assets/Menu/Artisanat.jpg")}
                resizeMode={"cover"}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.dispoButtonArt}
              onPress={() => this.props.navigation.navigate("Art")}
            >
              <Image
                style={[styles.imgPuceMenu, { marginRight: 0 }]}
                source={require("../Assets/Menu/Art.jpg")}
                resizeMode={"cover"}
              />
              <View style={styles.dispoTextMenuRigth}>
                <View
                  style={[
                    styles.containerPastilleMenu,
                    { alignSelf: "flex-end" },
                  ]}
                >
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Carte.Interactive", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Arts", this.props.Locale)}
                </Text>
              </View>
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.state.fadeAnim,
              zIndex: this.state.AnimatedzIndex,
            }}
          >
            <TouchableOpacity
              style={[
                styles.dispoButtonMenu,
                { backgroundColor: "rgba(249, 169, 16, 0.7)" },
              ]}
              onPress={() => this.props.navigation.navigate("ParcourLibre")}
            >
              <View style={styles.dispoTextMenu}>
                <View style={styles.containerPastilleMenu}>
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Promenade", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Parcours.Libre", this.props.Locale)}
                </Text>
              </View>
              <Image
                style={styles.imgPuceMenu}
                source={require("../Assets/Menu/ParcoursLibre.jpg")}
                resizeMode={"cover"}
              />
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              opacity: this.state.fadeAnim,
              zIndex: this.state.AnimatedzIndex,
            }}
          >
            <TouchableOpacity
              style={[
                styles.dispoButtonMenu,
                { backgroundColor: "rgba(216, 59, 59, 0.7)" },
              ]}
              onPress={() => this.props.navigation.navigate("EscapeGame")}
            >
              <View style={styles.dispoTextMenu}>
                <View style={styles.containerPastilleMenu}>
                  <Text adjustsFontSizeToFit style={styles.textPastilleMenu}>
                    {translate("Challenge", this.props.Locale)}
                  </Text>
                </View>
                <Text
                  adjustsFontSizeToFit
                  style={[styles.textWhite, styles.textTitreMenu]}
                >
                  {translate("Menu.Escape.Game", this.props.Locale)}
                </Text>
              </View>
              <Image
                style={styles.imgPuceMenu}
                source={require("../Assets/Menu/EscapeGame.jpg")}
                resizeMode={"cover"}
              />
            </TouchableOpacity>
          </Animated.View>

          <View
            style={{
              flexDirection: "row",
              marginBottom: height * 0.03,
            }}
          >
            <TouchableOpacity
              style={[
                styles.dispoButtonSetting,
                {
                  transform: this.state.scaleAnimation.getTranslateTransform(),
                },
              ]}
              onPress={() => this.handleSettingClick()}
              activeOpacity={settingZoomed === false ? 0 : 1}
            >
              <Animated.View
                style={{
                  height: this.state.animatedSettingHeight,
                  width: this.state.animatedSettingWidth,
                  justifyContent: "center",
                  paddingLeft: -10,
                  marginLeft: -10,
                }}
              >
                {settingZoomed === false ? (
                  <Image
                    style={[styles.imgIconMenu, { marginLeft: 10 }]}
                    source={require("../Assets/Menu/Reglages.png")}
                    resizeMode={"cover"}
                  />
                ) : (
                  <View style={{ padding: 20 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Image
                        style={[styles.imgIconMenu, { marginLeft: 7 }]}
                        source={require("../Assets/Menu/Reglages.png")}
                        resizeMode={"cover"}
                      />
                      <TouchableOpacity
                        onPress={() => this.handleSettingClick()}
                      >
                        <Entypo name="chevron-down" size={30} color="black" />
                      </TouchableOpacity>
                    </View>
                    <Text
                      style={{ fontSize: 22, fontWeight: "700", marginTop: 20 }}
                    >
                      {translate("Language.Select", this.props.Locale)}
                    </Text>
                    <View style={styles.containerFlag}>
                      <TouchableOpacity
                        style={styles.borderFlag}
                        onPress={() => {
                          this.changeLanguage("fr");
                        }}
                      >
                        <Image
                          style={styles.imgFlag}
                          source={require("../Assets/Langues/IconeFrancais.png")}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.borderFlag}
                        onPress={() => {
                          this.changeLanguage("en");
                        }}
                      >
                        <Image
                          style={styles.imgFlag}
                          source={require("../Assets/Langues/IconeAnglais.png")}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.borderFlag}>
                        <Image
                          style={styles.imgFlag}
                          source={require("../Assets/Langues/IconeAllemand.png")}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.borderFlag}
                        onPress={() => {
                          this.changeLanguage("it");
                        }}
                      >
                        <Image
                          style={styles.imgFlag}
                          source={require("../Assets/Langues/IconeItalien.png")}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.borderFlag}
                        onPress={() => {
                          this.changeLanguage("nl");
                        }}
                      >
                        <Image
                          style={styles.imgFlag}
                          source={require("../Assets/Langues/IconeNLand.png")}
                        />
                      </TouchableOpacity>
                    </View>
                    <Text
                      style={{ fontSize: 22, fontWeight: "700", marginTop: 20 }}
                    >
                      {translate("Credit", this.props.Locale)}
                    </Text>
                    <View style={{ flexDirection: "column", marginLeft: 7 }}>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL("https://icones8.fr/").catch((err) =>
                            console.error("Couldn't load page", err)
                          )
                        }
                      >
                        <Text
                          style={{
                            fontSize: width * 0.04,
                            fontWeight: "700",
                            color: "white",
                            marginBottom: 5,
                          }}
                        >
                          Icon8
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL("https://pixabay.com/fr/").catch(
                            (err) => console.error("Couldn't load page", err)
                          )
                        }
                      >
                        <Text
                          style={{
                            fontSize: width * 0.04,
                            fontWeight: "700",
                            color: "white",
                            marginBottom: 5,
                          }}
                        >
                          Pixabay
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL(
                            "http://www.mairie-serres05.fr/"
                          ).catch((err) =>
                            console.error("Couldn't load page", err)
                          )
                        }
                      >
                        <Text
                          style={{
                            fontSize: width * 0.04,
                            fontWeight: "700",
                            color: "white",
                            marginBottom: 5,
                          }}
                        >
                          Mairie de Serres
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          Linking.openURL(
                            "https://www.sisteron-buech.fr/fr"
                          ).catch((err) =>
                            console.error("Couldn't load page", err)
                          )
                        }
                      >
                        <Text
                          adjustsFontSizeToFit={settingZoomed}
                          style={{
                            fontSize: width * 0.038,
                            fontWeight: "700",
                            color: "white",
                            marginBottom: 5,
                          }}
                        >
                          Office de tourisme Sisteron Buëch
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </Animated.View>
            </TouchableOpacity>
            <Animated.View
              style={{
                opacity: this.state.fadeAnim,
                zIndex: this.state.AnimatedzIndex,
              }}
            >
              <TouchableOpacity
                style={styles.dispoButtonSerresPratique}
                onPress={() => this.props.navigation.navigate("Pratique")}
              >
                <View style={styles.dispoTextMenu}>
                  <Text
                    adjustsFontSizeToFit
                    style={[styles.textWhite, styles.textTitreMenu]}
                  >
                    {translate("Menu.Pratique", this.props.Locale)}
                  </Text>
                </View>
                <Image
                  style={styles.imgIconMenu}
                  source={require("../Assets/Menu/SerresPratique.png")}
                  resizeMode={"cover"}
                />
              </TouchableOpacity>
            </Animated.View>
            <Animated.View
              style={{
                opacity: this.state.fadeAnim,
                zIndex: this.state.AnimatedzIndex,
              }}
            >
              <TouchableOpacity
                style={[styles.dispoButtonAgenda]}
                onPress={() => this.props.navigation.navigate("Agenda")}
              >
                <Image
                  style={styles.imgIconMenu}
                  source={require("../Assets/Menu/Agenda.png")}
                  resizeMode={"cover"}
                />
                <View style={styles.dispoTextMenuRigth}>
                  <Text
                    adjustsFontSizeToFit
                    style={[styles.textWhite, styles.textTitreMenu]}
                  >
                    {translate("Menu.Agenda", this.props.Locale)}
                  </Text>
                </View>
              </TouchableOpacity>
            </Animated.View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    flex: 1,
    resizeMode: "contain",
  },
  containerHeaderMenu: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: height * 0.23,
    width: width,
    paddingHorizontal: 20,
  },
  imgBlason: {
    width: 61,
    height: 79.5,
  },
  imgIconSetting: {
    width: 50,
    height: 30,
  },
  textSerres: {
    fontSize: width * 0.17,
    fontWeight: "500",
    fontFamily: "MedievalSharp_400Regular",
    marginTop: 5,
    color: "white",
  },
  textWhite: {
    color: "white",
  },
  textTitreMenu: {
    fontSize: Platform.OS === "ios" ? 20 : 22,
    fontWeight: Platform.OS === "ios" ? "900" : "bold",
    // marginTop: Platform.OS === "ios" ? 10 : 5,
  },
  textCredit: {
    fontSize: 15,
    fontWeight: "900",
    textDecorationLine: "underline",
    marginHorizontal: width * 0.3,
    marginVertical: 10,
  },
  textDescMenu: {
    fontSize: 9,
    fontWeight: "900",
    marginTop: 3,
  },
  containerPastilleMenu: {
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    alignSelf: "flex-start",
    paddingHorizontal: 3,
    marginBottom: 5,
  },
  textPastilleMenu: {
    fontSize: 8,
    fontWeight: Platform.OS === "ios" ? "900" : "bold",
    paddingHorizontal: 2,
    paddingVertical: 0,
  },
  imgPuceMenu: {
    width: 50,
    height: 50,
    alignSelf: "center",
    alignItems: "flex-start",
    marginTop: -4,
    borderRadius: 25,
    borderColor: "white",
    borderWidth: 4,
    marginRight: 10,
    marginTop: 1,
  },

  imgIconMenu: {
    width: 31,
    height: 31,
    alignSelf: "center",
    alignItems: "flex-start",
  },
  dispoTextMenu: {
    padding: "3%",
  },
  dispoTextMenuRigth: {
    alignItems: "flex-end",
    justifyContent: "center",
    padding: "3%",
  },
  dispoButtonMenu: {
    width: width * 0.9,
    height: height * 0.1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: height * 0.03,
    borderRadius: 20,

    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  dispoButtonArtisanat: {
    width: width * 0.55,
    height: height * 0.1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    backgroundColor: "rgba(208, 212, 50, 0.7)",
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginRight: width * 0.05,
    padding: "2%",
  },
  dispoButtonArt: {
    width: width * 0.4,
    height: height * 0.1,
    flexDirection: "row",
    justifyContent: "space-between",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    backgroundColor: "rgba(214, 157, 157, 0.7)",
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    padding: "2%",
  },
  dispoButtonSerresPratique: {
    backgroundColor: "rgba(122, 133, 241, 0.7)",
    width: width * 0.4,
    height: height * 0.09,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    borderRadius: 20,
    // padding: "7%",
    marginRight: width * 0.05,
  },
  dispoButtonAgenda: {
    width: width * 0.35,
    height: height * 0.09,
    flexDirection: "row",
    justifyContent: "space-between",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    backgroundColor: "rgba(122, 191, 241, 0.7)",
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    padding: "8%",
  },
  dispoButtonSetting: {
    // width: width * 0.15,
    // height: height * 0.09,
    flexDirection: "row",
    justifyContent: "space-between",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    padding: "2%",
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    backgroundColor: "rgba(101, 101, 106, 0.7)",
    borderRadius: 20,
    padding: "1%",
    marginRight: width * 0.05,
  },
  containerFlag: {
    width: width * 0.8,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
  },
  borderFlag: {
    borderRadius: 30,
    borderWidth: 4,
    borderColor: "black",
    width: 48,
    height: 48,
  },
  imgFlag: {
    width: 48,
    height: 48,
    alignSelf: "center",
    alignItems: "flex-start",
    marginTop: -4,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps, { ChangeLocale })(Menu);
