import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Animated,
  BackHandler,
} from "react-native";
import { Divider } from "react-native-elements";
import { Feather } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import MapView, { PROVIDER_GOOGLE, Callout } from "react-native-maps";
import { MarkerParcourLibre } from "../Assets/ParcourLibre/Marker";
import AudioSlider from "./Components/utils/AudioSlider";
import MarkerPL from "./Components/utils/MarkerParcourLibre";

class ParcourLibre extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this._mapView = null;

    this.state = {
      eventSound: undefined,
      event: false,
      loadingSound: undefined,
      markers: undefined,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("Menu");
    return true;
  }

  updateChildState(data) {
    this.setState(data);
  }

  async handleMarkerPress(index, region, markers) {
    var { eventSound } = this.state;
    this.setState({ markers: markers });
    if (markers.audio !== undefined && eventSound === undefined) {
      this.setState({ eventSound: markers.audio, event: true });
    } else if (markers.audio !== undefined && eventSound !== undefined) {
      this.setState({ eventSound: undefined });
      setTimeout(() => {
        this.setState({ eventSound: markers.audio });
      }, 3000);
    }

    this._mapView.animateToRegion(region, 350);
  }

  soundLoader = () => {
    if (
      this.state.eventSound !== undefined &&
      this.state.markers !== undefined
    ) {
      this.setState({ eventSound: undefined, markers: undefined });
    }
  };

  render() {
    var { eventSound, markers } = this.state;
    var Marker = MarkerParcourLibre(this.props.Locale).map((markers, index) => {
      var coordinate = {
        latitude: markers.latitude,
        longitude: markers.longitude,
      };

      var region = {
        latitude: markers.latitude + 0.0007,
        longitude: markers.longitude,
        latitudeDelta: 0.002,
        longitudeDelta: 0.002,
      };

      return (
        <MarkerPL
          key={index}
          markers={markers}
          index={index}
          coordinate={coordinate}
          region={region}
          handleMarkerPress={this.handleMarkerPress.bind(this)}
          updateParentState={this.updateChildState.bind(this)}
        />
      );
    });
    return (
      <Animated.View style={styles.container}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Menu")}
            style={{ marginHorizontal: 10 }}
          >
            <Feather
              name="menu"
              size={40}
              color="white"
              style={{
                textShadowColor: "black",
                textShadowOffset: { width: 0.1, height: 0.1 },
                textShadowRadius: 0.1,
              }}
            />
          </TouchableOpacity>

          <Text adjustsFontSizeToFit style={styles.textHeader}>
            {translate("Menu.Parcours.Libre", this.props.Locale)}
          </Text>
        </View>
        <Divider style={styles.divider} />
        <View style={styles.container}>
          <MapView
            ref={(map) => (this._mapView = map)}
            initialRegion={{
              latitude: 44.429,
              longitude: 5.7155,
              latitudeDelta: 0.004,
              longitudeDelta: 0.004,
            }}
            style={[styles.container, styles.map]}
            showsUserLocation={true}
            showsMyLocationButton={true}
            followsUserLocation={true}
            showsPointsOfInterest={false}
            toolbarEnabled={true}
            onPress={() => {
              this.soundLoader();
            }}
            onMarkerDeselect={() => {
              this.soundLoader();
            }}
          >
            {Marker}
          </MapView>

          {markers !== undefined && (
            <View
              style={{
                justifyContent: "space-between",
                position: "absolute",
                width: height * 0.45,
                top: 20,
                backgroundColor: "rgba(252, 193, 79,0.8)",
                borderRadius: 20,
                borderColor: "black",
                borderWidth: 0.5,
              }}
            >
              <Text
                adjustsFontSizeToFit
                style={{
                  color: "black",
                  fontSize: 18,
                  fontWeight: "700",
                  textAlign: "center",
                  marginVertical: 12,
                }}
              >
                {translate(markers.name, this.props.Locale)}
              </Text>
              <Image
                source={markers.image}
                style={{
                  height: height * 0.35,
                  width: height * 0.45,
                  // backgroundColor: "red",
                }}
                resizeMode={"contain"}
              />
              <Text
                adjustsFontSizeToFit
                style={{
                  color: "black",
                  fontSize: 12,
                  fontWeight: "500",
                  textAlign: "center",
                  marginVertical: 15,
                  marginHorizontal: "3%",
                }}
              >
                {" "}
                {translate(markers.desc, this.props.Locale)}
              </Text>
            </View>
          )}

          <View
            style={{
              position: "absolute",
              bottom: 0,
              width: width * 0.9,
            }}
          >
            {eventSound !== undefined && (
              <AudioSlider
                backgroundColor={"rgba(252, 193, 79,0.8)"}
                playing={this.state.playing}
                soundObject={eventSound}
                event={this.state.event}
                height={60}
              />
            )}
          </View>
        </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    flex: 1,
  },
  image: {
    resizeMode: "cover",
    width: width,
  },
  map: {
    width: width,
    // height: height * 0.85,
    justifyContent: "flex-end",
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    paddingTop: 20,
    backgroundColor: "#FCC14F",
  },
  divider: {
    backgroundColor: "black",
    height: 0.6,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 1,
    elevation: 3,
  },
  iconSidePosition: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 1,
  },
  textHeader: {
    fontSize: 34,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: 0.1,
    shadowOpacity: 0.1,
    color: "white",
  },
  imgPres: {
    width: width,
    height: 250,
    marginTop: 0,
    paddingTop: 0,
  },

  containerListItem: {
    backgroundColor: "#ECDC4A",
    shadowColor: "black",
    width: width,
    height: height * 0.4,
    // marginTop: 10,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.12,
    shadowRadius: 5.46,
    elevation: 20,
    padding: 0,
    marginBottom: 4,
  },
  textTitleListItem: {
    fontSize: 20,
    fontFamily: "Palanquin_700Bold",
  },
  textDescListItem: {
    fontSize: 13,
    marginTop: -10,
    fontFamily: "Palanquin_500Medium",
  },
  rowCenter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#ECDC4A",
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    width: width,
    opacity: 0.8,
  },
  columnCenter: {
    flexDirection: "column",
    alignItems: "flex-start",
    marginTop: height * 0.3,
  },
  markerCallout: {
    width: height * 0.45,
    // height: height * 0.6,
    backgroundColor: "rgba(252, 193, 79,0.8)",
    borderRadius: 20,
    borderColor: "black",
    borderWidth: 0.5,
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(ParcourLibre);
