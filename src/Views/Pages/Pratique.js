import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Linking,
  Platform,
  BackHandler,
} from "react-native";
import { ListItem, Icon, Divider } from "react-native-elements";
import { Feather, Entypo } from "@expo/vector-icons";
import { translate } from "../../Langues/locale";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");

class Pratique extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      currentMonth: undefined,
      events: undefined,
      toggleEvent: false,
      presenceEvent: false,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("Menu");
    return true;
  }

  toggleEvent = (item) => {
    if (this.state.toggleEvent === false || this.state.toggleEvent !== item) {
      this.setState({ toggleEvent: item });
    } else {
      this.setState({ toggleEvent: false });
    }
  };

  render() {
    return (
      <ImageBackground
        source={require("../Assets/Langues/PhotoSerresVille.jpg")}
        style={styles.image}
        imageStyle={{ opacity: 0.5 }}
      >
        <View style={styles.container}>
          <View style={styles.containerHeader}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Menu")}
              style={{ marginHorizontal: 10 }}
            >
              <Feather name="menu" size={40} color="white" />
            </TouchableOpacity>

            <Text adjustsFontSizeToFit style={styles.textHeader}>
              {translate("Menu.Pratique", this.props.Locale)}
            </Text>
          </View>
          <Divider style={styles.divider} />
          <ScrollView
            style={{
              width: width,
              // marginBottom: 0,
              // flex: 1,
            }}
            contentContainerStyle={{
              // height: height * 0.85,
              paddingVertical: 10,
            }}
          >
            {/******************** SE RESTAURER ******************** */}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Food")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Restaurant.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Food", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Food" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Food" && (
              <View>
                <ListItem
                  // bottomDivider
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Brasserie+L'estaminet/@44.429286,5.7146873,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae2971e858019:0xb23466b3c41dd9c6!8m2!3d44.429286!4d5.716876"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Brasserie.Lestaminet", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Restaurant+La+Germanette/@44.4069934,5.7232403,17z/data=!3m1!4b1!4m5!3m4!1s0x12cafdeb3a4391dd:0x8f57e19cc015b982!8m2!3d44.4069934!4d5.725429"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Germanelle", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Vanille+Caf%C3%A9/@44.4287691,5.7138525,17z/data=!4m12!1m6!3m5!1s0x12cae296f70f6ff7:0xd5ca1605567ddef5!2sVanille+Caf%C3%A9!8m2!3d44.4287691!4d5.7160412!3m4!1s0x12cae296f70f6ff7:0xd5ca1605567ddef5!8m2!3d44.4287691!4d5.7160412"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Vanille.Cafe", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.fr/maps/place/Clara/@44.4251853,5.7167235,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae392d0fe672b:0xac25d51948c4ca84!8m2!3d44.4251853!4d5.7188752"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Clara", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Sud+Vietnam/@44.4281207,5.7126979,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae2967b282fad:0xc0c6500f31b0fbc1!8m2!3d44.4281207!4d5.7148866"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Sud.Vietnam", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Caf%C3%A9+du+commerce/@44.4293854,5.715154,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae29717393a59:0x7fc2c61d7c2c8eac!8m2!3d44.4293854!4d5.7173427"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Cafe.Commerce", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/La+pizza+serroise/@44.4290602,5.7149512,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae362f502215b:0xbfc3cee6a8615cfe!8m2!3d44.4290602!4d5.7171399"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Pizza.Serroise", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/La+buvette+de+Serres/@44.4289594,5.7142677,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae3bb4066bdd1:0xe69b59a75a41bc33!8m2!3d44.4289594!4d5.7164564"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Buvette.Serres", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/La+Marmotte+Gourmande/@44.4277365,5.7105308,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae295c352f66b:0xc2e1e1c2ad429ad6!8m2!3d44.4277365!4d5.7127195"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Restaurant.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Marmotte.Gourmande", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** VOYAGEUR *********************/}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Travel")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Voiture.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Travel", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Travel" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Travel" && (
              <View>
                <ListItem
                  // bottomDivider
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Gare+de+Serres/@44.424647,5.7162693,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae2bd3c01b477:0x32f984f912973a6b!8m2!3d44.424647!4d5.718458"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Metro.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Gare", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  // bottomDivider
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL("https://zou.maregionsud.fr/").catch(
                      (err) => console.error("Couldn't load page", err)
                    )
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Metro.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Reseau.Bus", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/KiWhi+Pass+Charging+Station/@44.4285273,5.7180235,19.96z/data=!4m12!1m6!3m5!1s0x12cae2971e858019:0xb23466b3c41dd9c6!2sBrasserie+L'estaminet!8m2!3d44.429286!4d5.716876!3m4!1s0x12cae29774b4069f:0xc2e0a8a92b25940b!8m2!3d44.4287291!4d5.7181551"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Borne.Electrique", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Station+Service+Les+Chambons/@44.4364795,5.710993,17z/data=!3m1!4b1!4m5!3m4!1s0x12afdb17eebb3c57:0x6f8e806ad3666218!8m2!3d44.4364795!4d5.7131817"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Station.Chambon", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Station+U/@44.427819,5.7146732,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae297ca18fa7b:0xb079ab62733dfbe9!8m2!3d44.427819!4d5.7168619"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Station.U", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/GARAGE+DES+BARILLONS/@44.4243588,5.71074,16.66z/data=!4m5!3m4!1s0x12cae381a54f5327:0xfdf61d68003ff561!8m2!3d44.4250514!4d5.7103318"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Garage.Barillons", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Sarl+de+L'europe/@44.4276066,5.7181541,17z/data=!4m12!1m6!3m5!1s0x12cae297bd4c4e23:0x93dc0439abab5903!2sSarl+de+L'europe!8m2!3d44.4275833!4d5.7179304!3m4!1s0x12cae297bd4c4e23:0x93dc0439abab5903!8m2!3d44.4275833!4d5.7179304"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Garage.Europe", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/GARAGE+DU+BUECH+-+Citro%C3%ABn/@44.4248642,5.7103639,17.83z/data=!4m5!3m4!1s0x12cae297a147cd6d:0xa59058f08473060a!8m2!3d44.4237665!4d5.7104706"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Voiture.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Garage.Buech", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** SEJOURNER *********************/}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Sleep")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Sejourner.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Sleep", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Sleep" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Sleep" && (
              <View>
                <ListItem
                  // bottomDivider
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/H%C3%B4tel+Fifi+Moulin/@44.4281632,5.7127475,17z/data=!3m1!4b1!4m8!3m7!1s0x12cae2967007b977:0xc505d7d413f13845!5m2!4m1!1i2!8m2!3d44.4281632!4d5.7149362"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Hotel.Fifi.Moulin", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/H%C3%B4tel+des+Alpes/@44.429773,5.7152447,17z/data=!3m1!4b1!4m8!3m7!1s0x12cae2971566ba0f:0xf421b5b36217a8a7!5m2!4m1!1i2!8m2!3d44.429773!4d5.7174334"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Hotel.Alpes", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Restaurant+H%C3%B4tel+Du+Nord/@44.4295414,5.7154814,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae359bfcd2057:0x1314c021c2e6c160!8m2!3d44.4295414!4d5.7176701"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Hotel.Nord", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  // bottomDivider
                  onPress={() =>
                    Linking.openURL(
                      "https://www.sisteron-buech.fr/fr/hebergement-a-louer/gite-rocher"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Gite.Rocher", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/R%C3%AAve+d'une+nuit+d'%C3%A9t%C3%A9/@44.4287139,5.7148699,18z/data=!4m12!1m6!3m5!1s0x12cae359bfcd2057:0x1314c021c2e6c160!2sRestaurant+H%C3%B4tel+Du+Nord!8m2!3d44.4295414!4d5.7176701!3m4!1s0x12cae309a48eba9d:0x74045c234f4f0f9a!8m2!3d44.4284379!4d5.7138958"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Reve.Nuit.Ete", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/R%C3%AAve+d'une+nuit+d'%C3%A9t%C3%A9/@44.4287139,5.7148699,18z/data=!4m12!1m6!3m5!1s0x12cae359bfcd2057:0x1314c021c2e6c160!2sRestaurant+H%C3%B4tel+Du+Nord!8m2!3d44.4295414!4d5.7176701!3m4!1s0x12cae309a48eba9d:0x74045c234f4f0f9a!8m2!3d44.4284379!4d5.7138958"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Soustet.Auches", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Les+Hirondelles+Chambres+d'H%C3%B4tes/@44.4274052,5.709323,17z/data=!3m1!4b1!4m8!3m7!1s0x12cae2959dfa2619:0x9a419c65b385533!5m2!4m1!1i2!8m2!3d44.4274052!4d5.7115117"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Hirondelle", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Le+Relais+du+Claret/@44.4304086,5.7124236,17z/data=!4m15!1m9!3m8!1s0x12cae2959dfa2619:0x9a419c65b385533!2sLes+Hirondelles+Chambres+d'H%C3%B4tes!5m2!4m1!1i2!8m2!3d44.4274052!4d5.7115117!3m4!1s0x12cafd3fffffffff:0x65c546135fb191c!8m2!3d44.4324339!4d5.7158685"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Sejourner.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Relais.Claret", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Flower+Camping+Le+Domaine+des+2+Soleils/@44.421109,5.7204623,17z/data=!3m1!4b1!4m8!3m7!1s0x12cae2a0a7b3b2fd:0x50153b9a3eb41085!5m2!4m1!1i2!8m2!3d44.421109!4d5.722651"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Tente.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Flower.Camping", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Camping+des+Barillons/@44.4171207,5.7111362,15.96z/data=!4m18!1m9!3m8!1s0x12cae2959dfa2619:0x9a419c65b385533!2sLes+Hirondelles+Chambres+d'H%C3%B4tes!5m2!4m1!1i2!8m2!3d44.4274052!4d5.7115117!3m7!1s0x0:0xe8b62c1cbefc7488!5m2!4m1!1i2!8m2!3d44.4193614!4d5.718386"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Tente.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Camping.Barillons", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** PROVISION *********************/}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Shopping")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Shopping.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Shopping", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Shopping" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Shopping" && (
              <View>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Livache+Roger/@44.4286276,5.7157345,20.04z/data=!4m5!3m4!1s0x12cae29664086469:0x62ac4bff33cb6149!8m2!3d44.4286424!4d5.7156634"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Viande.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Boucherie.Livache", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                {/* <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Boucherie+Bastiaise/@44.4297854,5.7168922,19.78z/data=!4m5!3m4!1s0x12cae29722f8aed1:0x8672e10d7c13a71b!8m2!3d44.4298515!4d5.7173578"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Viande.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Boucherie.Bastiaise", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem> */}
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Boulangerie+l'Epi+d'Or/@44.4278598,5.7173809,19z/data=!4m5!3m4!1s0x12cae297a2686c27:0xe66aa59ccdab76f6!8m2!3d44.4279682!4d5.7179513"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Boulangerie.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Boulangerie.Epi.Or", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Petit+Fournil+du+Pain+d'Aujour+-+Le+D%C3%A9p%C3%B4t/@44.4285213,5.7155645,19.78z/data=!4m5!3m4!1s0x0:0x7ea4fb9f5b9fb22d!8m2!3d44.4286895!4d5.7159187"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Boulangerie.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Petit.Fournil", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/3+Place+de+la+Libert%C3%A9,+05700+Serres/@44.4292877,5.7169244,21z/data=!4m5!3m4!1s0x12cae296e20b8571:0x629e04bb136f907b!8m2!3d44.4293694!4d5.71683"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Boulangerie.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Boulangerie.Patisserie", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/U+Express/@44.4274908,5.7168723,19.7z/data=!4m5!3m4!1s0x12cae296357bb917:0xe94a9785212ed46c!8m2!3d44.4272796!4d5.7166048"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Shopping.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("U.Express", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/L'Epicerie+Bio/@44.4279455,5.7177185,19.18z/data=!4m5!3m4!1s0x0:0x2df3ba1b9291d0dc!8m2!3d44.4282541!4d5.7181441"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Shopping.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Epicerie.Bio", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.fr/maps/place/Le+Panieier+%C3%80+Salade/@44.4287978,5.716279,19z/data=!4m8!1m2!2m1!1sle+panier+%C3%A0+salade+serres!3m4!1s0x12cae296fc7c07a1:0xa9c388d01c19b775!8m2!3d44.4290035!4d5.7165059"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Shopping.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Panier.Salade", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.fr/maps/place/Le+Depanneur/@44.4286273,5.713588,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae296f5f6c9f3:0xa7c7d4a9e78273ec!8m2!3d44.4286303!4d5.7157745"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Shopping.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Depaneur", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/TABAC+LOTO+PRESSE+DE+SERRES/@44.4292014,5.7166814,19.18z/data=!4m5!3m4!1s0x12cae2971d3d2765:0x68c1eb18960bcda!8m2!3d44.4294086!4d5.7169059"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Shopping.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Tabac.Presse", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** LOISIR *********************/}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Loisir")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Piscine.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Loisirs", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Loisir" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Loisir" && (
              <View>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Base+de+loisirs+de+la+Germanette/@44.4080315,5.7202097,16.31z/data=!4m5!3m4!1s0x12cafd535f042c1f:0x10aab0adbda26025!8m2!3d44.4069792!4d5.7256714"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Piscine.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Base.Germanette", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** SANTE *********************/}
            <ListItem
              underlayColor="transparent"
              onPress={() => this.toggleEvent("Sante")}
              containerStyle={styles.containerListItem}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Hopital.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    {translate("Health", this.props.Locale)}
                  </ListItem.Title>
                </View>
              </ListItem.Content>
              {this.state.toggleEvent !== "Sante" ? (
                <ListItem.Chevron size={25} color={"black"} />
              ) : (
                <Entypo name="chevron-down" size={25} color="black" />
              )}
            </ListItem>

            {this.state.toggleEvent === "Sante" && (
              <View>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Pharmacie+Garavagno+St%C3%A9phane/@44.4289613,5.7164386,19.7z/data=!4m5!3m4!1s0x12cae2971d763f9b:0xdcf8e900b877e3b1!8m2!3d44.4291687!4d5.7167711"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Hopital.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Pharmacie.Garavagno", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Caruso+Alain/@44.4276915,5.7161175,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae2961c96aecd:0xec2f5e58eb85752a!8m2!3d44.4276915!4d5.7183062"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Hopital.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Medecin.Caruso", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
                <ListItem
                  underlayColor="transparent"
                  onPress={() =>
                    Linking.openURL(
                      "https://www.google.com/maps/place/Draganescu+Bogdan/@44.4267823,5.7112529,17z/data=!3m1!4b1!4m5!3m4!1s0x12cae295f1aea913:0xa8eaa941f1ba8cb0!8m2!3d44.4267823!4d5.7134416"
                    ).catch((err) => console.error("Couldn't load page", err))
                  }
                  containerStyle={styles.containerListItems}
                >
                  <ListItem.Content style={styles.columnCenter}>
                    <View style={styles.rowCenter}>
                      <Image
                        style={{ width: 20, height: 20 }}
                        source={require("../Assets/Pratique/Hopital.png")}
                        resizeMode={"stretch"}
                      />

                      <ListItem.Title
                        adjustsFontSizeToFit
                        style={styles.textTitleListItems}
                      >
                        {translate("Medecin.Bogdan", this.props.Locale)}
                      </ListItem.Title>
                    </View>
                  </ListItem.Content>

                  <ListItem.Chevron size={15} color={"black"} />
                </ListItem>
              </View>
            )}
            {/******************** INTRAMUROS *********************/}
            <ListItem
              underlayColor="transparent"
              containerStyle={styles.containerListItem}
              onPress={() => {
                Platform.OS === "ios"
                  ? Linking.openURL(
                      "https://apps.apple.com/fr/app/intramuros-info-du-territoire/id1312850573"
                    ).catch((err) => console.error("Couldn't load page", err))
                  : Linking.openURL(
                      "https://play.google.com/store/apps/details?id=com.intramuros.Intramuros.production"
                    ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Intramuros.png")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    Intramuros
                  </ListItem.Title>
                </View>
              </ListItem.Content>

              <ListItem.Chevron size={25} color={"black"} />
            </ListItem>
            {/******************** Patrimoine Haute Alpes *********************/}
            <ListItem
              underlayColor="transparent"
              containerStyle={styles.containerListItem}
              onPress={() => {
                Platform.OS === "ios"
                  ? Linking.openURL(
                      "https://apps.apple.com/us/app/itinerance-alpine/id1144433544"
                    ).catch((err) => console.error("Couldn't load page", err))
                  : Linking.openURL(
                      "https://play.google.com/store/apps/details?id=com.cg05.itinerancealpine"
                    ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <ListItem.Content style={styles.columnCenter}>
                <View style={styles.rowCenter}>
                  <Image
                    style={{ width: 35, height: 35 }}
                    source={require("../Assets/Pratique/Patrimoine.jpg")}
                    resizeMode={"stretch"}
                  />

                  <ListItem.Title
                    adjustsFontSizeToFit
                    style={styles.textTitleListItem}
                  >
                    Patrimoine Hautes-Alpes
                  </ListItem.Title>
                </View>
              </ListItem.Content>

              <ListItem.Chevron size={25} color={"black"} />
            </ListItem>
          </ScrollView>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: height,
    alignItems: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  containerHeader: {
    height: height * 0.15,
    width: width,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.22,
    shadowRadius: 5.46,
    elevation: 20,
    paddingTop: 20,
    backgroundColor: "#7A85F1",
  },
  containerHeaderMenu: {
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    height: height * 0.1,
    width: width,
    backgroundColor: "#588AAD",
  },
  containerListItem: {
    backgroundColor: "rgba(122, 133, 241, 0.8)",
    // shadowColor: "black",
    width: width * 0.96,
    marginHorizontal: width * 0.02,
    borderRadius: 20,
    height: height * 0.1,
    marginTop: 10,

    // shadowOffset: {
    //   width: 0,
    //   height: 10,
    // },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 20,
  },
  containerListItems: {
    backgroundColor: "rgba(200, 204, 247, 0.8)",
    shadowColor: "black",
    height: height * 0.06,
    width: width * 0.9,
    marginLeft: width * 0.05,
    marginTop: 10,
    padding: 10,
    borderRadius: 50,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 20,
  },
  textEventTitle: {
    fontSize: 20,
    marginHorizontal: 20,
    marginVertical: 13,
  },
  textDescEvent: {
    fontSize: 16,
    marginHorizontal: 10,
    marginBottom: 10,
  },
  textTitleListItem: {
    marginLeft: 10,
    fontSize: width * 0.05,
    fontWeight: "700",
  },
  textTitleListItems: {
    marginLeft: width * 0.02,
    fontSize: 15,
    fontWeight: "700",
  },
  textSerrois: {
    textAlign: "center",
    marginTop: 20,
    fontSize: 15,
    fontWeight: "800",
    textDecorationLine: "underline",
  },
  divider: {
    backgroundColor: "black",
    height: 0.6,
    width: width,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 2,
    shadowRadius: 10,
    elevation: 20,
  },
  containerNoEvents: {
    width: width,
    height: height * 0.77,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  textHeader: {
    fontSize: 34,
    fontWeight: "800",
    textShadowColor: "black",
    textShadowOffset: { width: 0.1, height: 0.1 },
    textShadowRadius: 0.1,
    shadowOpacity: 0.1,
    color: "white",
  },
  textMonth: {
    fontSize: 30,
    fontWeight: "700",
    color: "white",
    marginBottom: 10,
  },
  imgHeader: {
    width: "100%",
    height: 150,
  },
  imgEvent: {
    height: 150,
    width: width * 0.7,
    marginVertical: 20,
    marginHorizontal: width * 0.08,
  },
  containerAgenda: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  columnCenter: { flexDirection: "column", justifyContent: "center" },
  rowCenter: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = ({ locale }) => {
  const { Locale } = locale;
  return { Locale };
};

export default connect(mapStateToProps)(Pratique);
