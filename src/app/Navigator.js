import { createSwitchNavigator, createAppContainer } from "react-navigation";
import Language from "../Views/Pages/Language";
import Menu from "../Views/Pages/Menu";
import Agenda from "../Views/Pages/Agenda";
import Pratique from "../Views/Pages/Pratique";
import Artisanat from "../Views/Pages/Artisanat";
import Art from "../Views/Pages/Art";
import NatureLoisirs from "../Views/Pages/NatureLoisirs";
import ParcourLibre from "../Views/Pages/ParcourLibre";
import CulturePatrimoine from "../Views/Pages/CulturePatrimoine";
import EscapeGame from "../Views/Pages/EscapeGame";

const Navigator = createSwitchNavigator({
  Language: Language,
  Menu: Menu,
  Agenda: Agenda,
  Pratique: Pratique,
  Artisanat: Artisanat,
  Art: Art,
  NatureLoisirs: NatureLoisirs,
  ParcourLibre: ParcourLibre,
  CulturePatrimoine: CulturePatrimoine,
  EscapeGame: EscapeGame,
});

export default createAppContainer(Navigator);
